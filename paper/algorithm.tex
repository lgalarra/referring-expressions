%In this section we elaborate on our approach to mine REs on KBs. 
Given an RDF KB $\mathcal{K}$ and a set of target entities
$T$, REMI returns an intuitive RE---a conjunction of subgraph expressions---that describes unambiguously 
 the input entities $T$ in $\mathcal{K}$. 
%We define intuitiveness for REs as a trade-off between compactness and informativeness, thus intuitive REs should be 
Intuitive REs are concise and resort to concepts that users are likely to understand. 
We first show how to quantify intuitiveness in Section~\ref{subsec:complexityfunction}.
We then elaborate on REMI's algorithm in Section \ref{subsec:algorithm}.
%This corresponds to the expression with minimal $\hat{C}(e)$, where $\hat{C}$ is our approximation 
%Kolmogorov complexity. 
% We start by describing REMI's language bias. We then elaborate on our quantification of complexity and the REMI algorithm in
% Sections~\ref{subsec:complexityfunction} and~\ref{subsec:algorithm} respectively.

\subsection{Quantifying intuitiveness}
\label{subsec:complexityfunction}
There may be multiple ways to describe a set of entities uniquely. For example, 
$\textit{capitalOf}(x, \textit{France})$ and $\textit{birthPlaceOf}(x, \textit{Voltaire})$ are both REs for Paris.
Our goal is therefore to quantify the \emph{intuitiveness} of such expressions without human intervention.
We say that an RE $e$ is more intuitive than an RE $e'$, if $C(e) < C(e')$, where $C$ 
denotes the Kolmogorov complexity. %; so we define intuitiveness as the inverse of complexity. 
The Kolmogorov complexity $C(e)$ of a string $e$ (e.g., an expression) is a measure of the absolute 
amount of information conveyed by $e$ and is defined
as the length in bits of $e$'s shortest effective binary description~\cite{zellner2001simplicity}.
If $e_b$ denotes such binary description and $M$ is the program that can \emph{decode} $e_b$ into $e$,  
$C(e) = l(e_b) + l(M)$ where $l(\cdot)$ denotes length in bits.
% The exact calculation of $C(e)$
% requires us to know the shortest way to compress $e$ as well as the most compact program $M$ for decompression.
Due to $C$'s intractability, applications can only \emph{approximate} it via 
suboptimal encodings and programs ($\hat{e_b}$, $\hat{M}$), hence $C(e) \approx \hat{C}(e) = l(\hat{e_b}) + l(\hat{M})$ 
with $C(e) \le \hat{C}(e)$. 

% Once we have fixed a compression/decompression scheme $\hat{M}$, applications only need to worry 
% about the term $l(\hat{e_b})$. %In our setting, this encoding should use fewer bits to encode intuitive expressions. 
Our proposed encoding builds upon the observation that intuitive expressions resort to prominent concepts. 
For example, it is natural and informative 
to describe Paris as the capital of France, because the concept of capital is well understood and France is a very salient entity. 
In contrast, it would be more complex to describe Paris in terms of less prominent concepts, let us say,  
its twin cities. %, because this concept is less prominent than the concept of capital city. %In the same spirit, the expression $\textit{placeOfBirthOf}(x, \textit{Voltaire})$ is also less intuitive, because Voltaire is likely irrelevant outside France.
% In other words, prominent predicates and entities are informative because they 
% relate to concepts that users often recall. 
In this spirit, we devise a code for concepts as follows:
% We thus devise a code for predicates and entities by constructing
% a \emph{ranking by prominence}. 
The code for a predicate $p$ (entity $I$) is the binary representation of its position $k$
in a \emph{ranking by prominence}. This way, prominent concepts can be rewarded 
with shorter codes.
We can now define the estimated Kolmogorov complexity $\hat{C}$ of a single-atom subgraph expression $p(x, I)$ as:
%\begin{small}
\[\hat{C}(p(x, I)) = l(k(p)) + l(k(I\;|\;p)) \]
%\end{small}
In the formula, $l(\cdot) = \textit{log}_2(\cdot) + 1$,
$k(p)$ is $p$'s position in the ranking of predicates of the KB, and
$k(I\;|\;p)$ is $I$'s conditional rank given $p$, i.e., $I$'s rank among all objects of $p$. The latter term follows
from the chain rule of the Kolmogorov complexity.
% It is calculated as the logarithm of $I$'s rank in the ranking of 
% objects of predicate $p$. 
For instance, if $p$ is the predicate \emph{city mayor}, the chain rule models 
the fact that once the concept of mayor has been conveyed, the context becomes narrower and the user needs
to rank fewer concepts, in this example, only city mayors. The chain rule also applies to 
subgraph expressions with multiple atoms. For instance, the complexity of  
$\rho = \textit{mayor}(x, y) \land \textit{party(y, \textit{Socialist})}$ is:
%\begin{small}
\[
\begin{aligned}
\hat{C}(\rho) ={} & l(k(\textit{mayor})) + l(k(\textit{party}(y, z) \: | \: \textit{mayor(x, y)})) \: + \\ 
& l(k(\textit{Socialist} \: |\: \textit{mayor}(x, y) \land \textit{party}(y, z))) 
\end{aligned}
\]
%  \[
%  \hat{C}(\rho) = l(\textit{mayor}_b) + l(\textit{party}(y, z)_b \: | \: \textit{mayor(x, y)}) + l(\textit{Socialist}_b \: |\: \textit{mayor}(x, y) \land \textit{party}(y, z)) 
%  \]
%\end{small}
% We highlight that the code for \emph{party} must account for the fact that this predicate appears in 
% a first-to-second-argument join with the predicate \emph{mayor}. 
% Hence, the second term in the sum amounts to 
The second term in the sum amounts to the code length of the rank of predicate \emph{party} among those predicates that allow for 
subject-to-object joins with \emph{mayor} in the KB. Likewise, the complexity of the Socialist party in the third term depends
on the ranking of parties with mayors among their members, i.e., the bindings for $z$ 
in $\textit{mayor}(x, y) \land \textit{party}(y, z)$.
% The chain rule models 
% the fact that once the concept of \emph{mayor} has been conveyed in the description, the context becomes narrower and the user needs
% to discriminate among fewer other concepts, in this example the predicates and entities that can describe politicians.
%As hinted in Section~\ref{subsec:languagebias}, the complexity of subgraph expressions does not always increase with the number of atoms. 
If a city can be unambiguously described as $\mathit{mayor}(x, I)$ for a non-prominent mayor $I$,
we may achieve a shorter code length if we replace $I$ by a variable $y$, an additional predicate, and 
a well-known party. 
%This could be the case when $l(I_b \;|\;\textit{mayor}) < l(\textit{party}(y, z)_b\;|\;\textit{mayor}(x, y)) + l(\textit{Socialist}_b\;|\;\textit{mayor}(x, y)\land \textit{party}(y, z))$.
%Nonetheless, longer descriptions tend to be more complex than shorter ones.
 %, namely as $\hat{C}(e) = \sum_{1\le i \le m}{\hat{C}(\rho_i)}$. %, however, 
%That is, we prefer REs with fewer subgraph expressions. 
% Consider as an example the following RE:
% $$e = \mathit{in}(x, \mathit{S. America})\land \mathit{officialLang}(x, y) \land \mathit{langFamily}(y, \mathit{Germanic})$$
% It follows that we can calculate $\hat{C}(e)$ as: 
% \[
% \begin{aligned}
% \hat{C}(e) ={} & \hat{C}(\mathit{in}(x, \mathit{S. America})) \;+ \\ 
% & \hat{C}(\mathit{officialLang}(x, y) \land \mathit{langFamily}(y, \mathit{Germanic})) 
% \end{aligned}
% \]
%\begin{small}
%\[\hat{C}(e) = \hat{C}(\mathit{in}(x, \mathit{S. America})) + \hat{C}(\mathit{officialLang}(x, y) \land \mathit{langFamily}(y, \mathit{Germanic}))$ \] 
%\end{small}
% It is vital to remark, however, that this formula makes a simplification. Consider the RE 
% $\textit{officialLang}(x, IT) \land \textit{officialLang}(x, DE)$ for Switzeland. 
% While $\hat{C}$ adds the complexity of the predicate \emph{officialLang} twice, an optimal code would count the predicate
% once and encode its multiplicity. In fact, this optimal code 
% would be applied to every common sub-path with multiplicity in the list of subgraph expressions. 
% This fact worsens the quality of $\hat{C}$ as an approximation of $C$ for such kind of expressions, however 
% it is not a problem in our setting as long as we use $\hat{C}$ for comparison purposes.

% Lastly, we discuss how to rank concepts by prominence. Wikipedia-based KBs
% provide information about the hyperlink structure of the entity pages, thus
% one alternative is to use the Wikipedia page rank (PR). The downside of this metric is that it is undefined for predicates.
% A second alternative is frequency, i.e., number of mentions of a concept.
% Frequency could be measured in the KB or extracted from exogenous sources
% such as a crawl of the Web or a search engine. Even though search engines may quantify prominence 
% more accurately (by providing real-time feedback and 
% leveraging circumstantiality), 
% we show that endogenous sources are good enough for this goal.
%Lastly, we discuss how to rank concepts by prominence. 
In line with other works that quantify prominence for concepts in KBs~\cite{linksum}, we 
rank concepts by frequency (\emph{fr}), and Wikipedia's page rank (\emph{pr}).
We denote the resulting complexity measures using these prominence metrics 
by $\hat{C}_{\mathit{fr}}$ and $\hat{C}_{\mathit{pr}}$ respectively. %We use \emph{fr} whenever \emph{pr} is undefined.

Finally, we can estimate the Kolgomorov complexity of an RE $e = \bigwedge_{1\le i \le m}{\rho_i}$ as the 
sum of the complexities of its individual subgraph expressions, i.e., $\hat{C}(e) = \sum_{1\le i \le m}{\hat{C}(\rho_i)}$.
% \subsection{Language Bias}
% \label{subsec:languagebias}
% Most approaches for RE mining define REs as conjunctions of atoms with bound objects,
% thus we call this language bias, \emph{the state-of-the-art language bias}.
% %The state-of-the-art language bias for REs consists of conjunctions of atoms with bound objects.
% REMI extends this language by allowing atoms with
% additional existentially quantified variables. 
% This design decision allows us to replace tail entities with high Kolmogorov complexity 
% with entities that are more prominent and hence more intuitive. 
% For instance, consider the RE $\textit{supervisorOf}(x, \textit{Alfred Kleiner})$ for
% Johann J. Müller. Saying that he was the supervisor of A. Kleiner may not
% say much to an arbitrary user. By allowing an additional variable, we can consider the expression
% ``he was the supervisor of the supervisor of Albert Einstein'', namely
% $\textit{supervisorOf}(x, y) \land \textit{supervisorOf}(y, \textit{A. Einstein})$. %, i.e., he was the supervisor of the supervisor of Albert Einstein. 
% We highlight that Einstein is \emph{simpler} to describe than Kleiner, 
% which makes the second expression, albeit longer, more informative and overall more intuitive than the first one. %, despite the additional atom.
% This shows that in the presence of irrelevant object entities, further atoms may help increase intuitiveness. %by making the expression more informative. 
% Nevertheless, in the general case longer expressions tend to be more complex. 
% This phenomenon becomes more palpable when the additional atoms 
% do not describe the root variable as in 
% $\textit{speaks}(x, y) \land \textit{family}(y, z) \land \textit{superfamily}(z, \textit{Italic})$ (``she 
% speaks a language in a subfamily of the Italic languages''). 
% This expression introduces two additional variables that 
% % turn comprehension and translation to natural language more effortful.
% % To see this phenomenon consider the REs 
% % $\textit{speaks}(x, \textit{ES}) \land \textit{speaks}(x, \textit{DE}) \land \textit{speaks}(x, \textit{DA})$ 
% % (``she speaks Spanish, German, and Danish'')
% % vs. $\textit{speaks}(x, y) \land \textit{langFamily}(y, z) \land \textit{superfamily}(z, \textit{Italic})$ (``she 
% % speaks a language of a subfamily of the Italic languages'').
% % Even if both REs are of the same length, the latter resorts to two additional variables that 
% % turn comprehension and translation to natural language more effortful. 
% turn comprehension and translation to natural language more effortful. 
% Besides, further atoms and specially additional variables can dramatically increase the size of the search space of REs, 
% which is exponential in the number of possible subgraph expressions.
% Our observations reveal, e.g., that a second additional variable increases by more than 270\% the number
% of subgraph expressions that REMI must handle in DBpedia. % (for Wikidata the increase is around 65\%). 
% Conversely, increasing the number 
% of atoms from 2 to 3 while keeping only one additional variable, leads to an increase of 40\%. %in DBpedia and 5\% in Wikidata.
% Based on all these observations, we restrict REMI's language
% bias to subgraph expressions with at most one additional variable and 3 atoms. % (in line with rule mining approaches on large KBs~\cite{amieplus} ). 
% The 3-atom constraint
% goes in line with rule mining approaches on large KBs~\cite{amieplus}. 
% This decision disqualifies our
% last example, but still allows expressions such as $\textit{bornIn}(x, y) \land \textit{livedIn}(x, y) \land 
% \textit{diedIn}(x, y)$ (she was born, lived and died in the same place).
% %Besides, this language bias keeps the search space manageable. 
% Table~\ref{tab:language} summarizes REMI's language
% of subgraph expressions.

%\begin{figure}
%\begin{minipage}[!b]{0.48\textwidth}
%  \begin{table}
% % %\resizebox{\columnwidth}{!}{
%    %\centering
%    \begin{tabular}{ l  l  }
%      \toprule
%      1 atom & $p_0(x, I_0)$ \\
%      Path & $p_0(x, y) \land p_1(y, I_1)$ \\
%      Path + star & $p_0(x, y) \land p_1(y, I_1) \land p_2(y, I_2) $ \\
%      2 closed atoms\; & $p_0(x, y) \land p_1(x, y)$ \\ 
%      3 closed atoms\;  & $p_0(x, y) \land p_1(x, y) \land p_2(x, y)$ \\ \bottomrule
%    \end{tabular}
% % %}
%    \caption{REMI's subgraph expressions.}
%    \label{tab:language}
%  \end{table}
%\end{minipage}
%\hfill
%\begin{minipage}[!b]{0.48\textwidth}
\begin{figure}[h]
\includegraphics[width=0.9\linewidth]{figures/tree.pdf}
%\includegraphics[width=0.45\textwidth]{figures/tree.pdf}
%\vspace{-25pt}
\caption{Search space example.}
\label{fig:tree}
%\end{figure}
%\end{minipage}
\end{figure}


\subsection{Algorithm}
\label{subsec:algorithm}
% REMI is inspired on algorithms to mine itemset generators~\cite{defme}.  
% Recall from Section~\ref{subsubsec:parallels} that if we model entities as transactions 
% and subgraph expressions as items, mining REs of minimal size in a KB is equivalent to find generators in 
% a smaller search space consisting of the items (subgraph expressions) 
% that describe the target transactions (entities). 
% Mining \emph{intuitive} REs differs from mining generators because (1) 
% intuitive REs have minimal complexity, which does not always correlate with minimal size, 
% and (2) the set of REs of minimal complexity for a given set of entities tends to be---if not empty---small.
%Inspired on methods to mine itemset generators, 
REMI implements a depth-first search (DFS) %\footnote{DFS approaches are preferred over BFS (breadth-first search) due to their smaller
%memory footprint})
on conjunctions of the subgraph expressions common
to \textbf{all} the target entities.
%The idea is illustrated with the example in Figure~\ref{fig:tree}.
Let us assume the KB knows only three common subgraph expressions $\rho_1$, $\rho_2$, and $\rho_3$ for the entities 
\emph{Rennes} and \emph{Nantes}, such that $\hat{C}(\rho_1) \leq \hat{C}(\rho_2) \leq \hat{C}(\rho_3)$ as illustrated
in Figure~\ref{fig:tree}.
% The tree in illustrates the search space for our example. 
Each node in the tree is an expression,
i.e., a conjunction of subgraph expressions and its complexity $\hat{C}$ is in parentheses.
When visiting a node, DFS must test whether the corresponding expression is an RE, i.e., 
whether the expression describes exclusively the target entities. If the test fails, the strategy should move to the node's first child.
If the test succeeds, DFS must verify whether the expression is less complex
than the least complex RE seen so far. If it is the case, this RE should be remembered, and DFS can prune
the search space by backtracking. 
%We observe, however, that DFS does not need to traverse the entire search space. 
To see why, imagine that $\rho_1 \land \rho_2$ in Figure~\ref{fig:tree} is an RE.
In this case, all REs prefixed with this expression (the node's descendants) 
will also be REs. However, all these REs are more complex.
This means that we can stop descending in the tree and prune the node 
$\rho_1 \land \rho_2 \land \rho_3$ in Figure~\ref{fig:tree}. We call this step a \emph{pruning by depth}.
We can do further pruning if we leverage the order of the entities. In our example, if 
$\rho_1 \land \rho_2$ is an RE, any expression prefixed with $\rho_1 \land \rho_{i}$ 
for $i>2$ must be more complex and can be therefore skipped. 
We call this a \emph{side pruning}. All these ideas are formalized by Algorithm~\ref{alg:remi} that takes as input a
KB $\mathcal{K}$ as well as the entities to describe, and returns an RE of minimal complexity according to
$\hat{C}$.
For each of the target entities, line 1 calculates (in a BFS fashion) its matching subgraph expressions, and takes 
those common to all the target entities. 
The expressions are then sorted by increasing complexity in a priority queue (line 2), which is 
processed as follows: % The depth-first exploration is 
%performed in lines 4-8. 
At each iteration, the least complex subgraph expression $\rho$ is dequeued (line 5) 
and sent to the subroutine \emph{DFS-REMI} (line 6) with the rest of the queue.
This subroutine explores the subtree rooted at $\rho$ and returns the most intuitive RE $e'$ prefixed with $\rho$. 
If $e'$ is less complex than the best
solution found so far (line 7), we remember it\footnote{We define $\hat{C}(\top) = \infty$}.
If \emph{DFS-REMI} returns an empty expression, we can conclude that there is no RE for the target entities $T$ (line 8).
To see why, recall that DFS will, in the worst case, combine $\rho$ 
with all remaining expressions $\rho'$ that are more complex.
If none of such combinations is an RE, there is no solution for $T$ in $\mathcal{K}$.%\vspace{-0.4cm}
\begin{algorithm}
%\begin{small}
\caption{REMI}
\label{alg:remi}
\KwIn{a KB: $\mathcal{K}$, 
the target entities: $T$}
\KwOut{an RE of minimal complexity: $e$}
    $G := \bigcap_{t \in T}{\textit{subgraphs-expressions}(t)}$ \\
    create priority queue from $G$ in ascending order by $\hat{C}$ \\    
    $\textit{e} := \top$ \\
    
    \While{$|G| > 0$}{
%    	\If{$g(\mathcal{K}) = T \land \hat{C}(g) < \hat{C}(R) $}{
%	    \Return $g$ \\
%    	}
%	$G' := G - \{ g \}$ \\
	$\rho := G.\textit{dequeue}()$ \\
	$e' := $\emph{DFS-REMI}($\rho$, $G$, $T$, $\mathcal{K}$) \\
%	$R := $ \emph{DFS-REMI}($g$, $G', T, \mathcal{K}$) \\	
	\IIf{$\hat{C}(e') < \hat{C}(e)$}{ $e := e'$ \\} 
	\IIf{$e = \top$}{ \Return $\top$ \\} 

    }
    \Return $e$
%\end{small}
\end{algorithm}%\vspace{-0.4cm}

We implemented Algorithm~\ref{alg:remi} in Java 8, including a parallel version called
P-REMI (detailed in our technical report~\cite{galrraga2019remi}).

% \noindent We now sketch how to calculate the subgraph expressions of an entity $t$ (line 1). 
% Contrary to \emph{DFS-REMI}, the routine $\textit{subgraphs-expressions}$ carries out a 
% breadth-first search. Starting with 
% atomic expressions of the form $p_0(x, I_0)$ (where $x$ binds to $t$), the routine derives all two-atom expressions, 
% namely paths of the form $p_0(x, y) \land p_1(y, I_1)$ and closed conjunctions 
% $p_0(x, y) \land p_1(x, y)$. %The two-atom paths are extended to produce the path+star combinations
% The two-atom paths are extended with atoms of the form $p_2(y, I_2)$ to produce the path+star combinations,
% whereas the closed conjunctions are used to derive the closed expressions of three atoms (see Table~\ref{tab:language}).

% The subroutine \emph{DFS-REMI} is detailed in Algorithm~\ref{alg:dfs-remi}, which 
% takes as input a subgraph expression $\rho$, the priority queue of subgraph expressions 
% $G$ (without $\rho$), the target entities $T$ and a KB $\mathcal{K}$.
% We use a stack initialized
% with the empty subgraph expression $\top$ in order to traverse the search space in a depth-first manner (line 1). 
% Each iteration pushes a subgraph expression to the stack, starting with $\rho$ (line 3).
% The conjunction of the elements of the stack defines an expression $e'$ that 
% is evaluated on the KB to test if it is an RE for the target entities (lines 4-5). If $e'$  
% is the least complex RE seen so far, the algorithm remembers it (line 6).
% Adding more expressions to $e'$ can only increase
% its complexity, hence line 7 performs a pruning by depth, so that all descendants of $e'$ are abandoned. 
% %If we resort to the example in Figure~\ref{fig:tree} with $e'=\rho_1 \land \rho_2$, this is tantamount to backtracking to $\rho_1$. 
% Line 8 backtracks anew to achieve a side pruning. 
% %This amounts to backtracking from $\rho_1$ to the root of the tree so that we prune all the subtrees rooted at nodes $\rho_1\;\land\;\rho_i$ ($i > 2$), as all expressions in those subtrees must be more complex than $\rho_1\land \rho_2$. 
% If backtracking leads
% to an empty stack, \emph{DFS-REMI} cannot do better, and can thus return $e'$ (line 9). %\vspace{-0.6cm}
% \begin{algorithm}
% %\begin{small}
% \caption{DFS-REMI}
% \label{alg:dfs-remi}
% \KwIn{a subgraph expression: $\rho$, priority queue: $G$, target entities: $T$, a KB: $\mathcal{K}$}
% \KwOut{an RE of minimal complexity prefixed in: $e$}
%      $\textit{S} := \{\top \}; \; e := \top; \; G' := \{\rho \} \cup G$ \\
%      \ForEach{$\rho' \in G'$}{
% 	$\textit{S} := S \cup \{ \rho' \} $ \\
% 	$e' := \bigwedge_{\hat{\rho} \in S}{\hat{\rho}}$ \\
% 	\If{ $e'(\mathcal{K}) = T$}{
% 	  \IIf{$\hat{C}(e') < \hat{C}(e)$}{ $e := e'$ \\ }
% 	  $S.\mathit{pop}()$ \\
% 	  $S.\mathit{pop}()$ \\
% 	  \IIf {$S = \emptyset$}{ \Return $e$ \\ }
% 	}
%     }
%     \Return $e$
% %   \end{small} 
% \end{algorithm}%\vspace{-0.9cm}
% \subsection{Parallel REMI} %\vspace{-0.2cm}
% We can parallelize Algorithm~\ref{alg:remi} if we allow multiple threads to concurrently dequeue elements from
% the priority queue of subgraph expressions and explore the subtrees rooted at those elements independently. 
% This implies to execute the loop in lines 4-8 in parallel.
% %Besides, we can parallelize line 2 by applying a parallel sort method to construct the priority queue.
% This new strategy, called P-REMI, preserves the logic of REMI with three differences.
% First, the least complex solution $e$ can be read and written by all threads.
% Second, if a thread found no solution from its exploration rooted at subgraph expression $\rho_i$, 
% it must signal all the other threads rooted at subgraph expressions
% $\rho_j$ ($j > i$) to stop. For instance, if a thread finished its exploration rooted at $\rho_1$ in Fig.~\ref{fig:tree},
% any exploration rooted at $\rho_2$ or $\rho_3$ is superfluous as it covers expressions
% that are less specific than those rooted at $\rho_1$.
% % The rationale behind this decision is that those threads explore regions of the space 
% % with expressions that are less specific than those expressions prefixed with $\rho_i$. Hence, 
% % those threads will not be able to find a solution either.
% Third, before
% testing if an expression is an RE, each thread should verify whether there is already a solution $e$
% of lower complexity. If so, the thread can backtrack until reaching
% a node of even lower complexity than $e$. 
% Since these differences mostly affect
% the logic of \emph{DFS-REMI}, we detail a new procedure called \emph{P-DFS-REMI} in Algorithm~\ref{alg:p-dfs-remi}.
% The new routine has the same signature as \emph{DFS-REMI} plus a reference to the best solution $e$. 
% Lines 1 and 2 initialize the stack and create a new priority queue $G'$ from 
% the original one. The DFS exploration starts in line 3. The first task of \emph{P-DFS-REMI} is to
% backtrack iteratively while the expression
% represented by the stack is less complex than the best solution $e$ (line 6). 
% If \emph{P-DFS-REMI} backtracked to the root node, it means the algorithm cannot find a better solution from now
% on, and can return $e$ (line 7). If backtracking did not remove any expression from the stack (check in line 8), 
% \emph{P-DFS-REMI} proceeds exactly as its sequential counterpart \emph{DFS-REMI} (lines 9-14). 
% Conversely, if the stack was pruned by the loop in line 6, P-DFS-REMI starts a new iteration since the
% corresponding expression in the resulting stack must have been tested in a previous iteration. 
% For proper implementation the access to $e$ must be synchronized among the different threads. 
% \begin{algorithm}
% %\begin{small}
% \caption{P-DFS-REMI}
% \label{alg:p-dfs-remi}
% \KwIn{a subgraph expression: $\rho$, priority queue of subgraph expressions: $G$,
% the target entities: $T$, $e$: best solution found so far, 
% a KB: $\mathcal{K}$}
% \KwOut{an RE of minimal complexity: $e$}
%      $S := \{\top\} $ \\
%      $G' := \{ \rho \} \cup  G$ \\
%      \While{$|G'| > 0$}{
% 	$\rho' := G'.\textit{dequeue}()$ \\
% 	$\textit{S} := S \cup \{ \rho' \} $ \\
% 	\WWhile{$|S| > 1 \land \hat{C}(\bigwedge_{\hat{\rho} \in S}{\hat{\rho}}) \ge \hat{C}(e)$}{
% 	  $S.\textit{pop}()$
% 	}
% 	
% 	\IIf{$S = \{ \top \}$}{ \Return $e$ }
% 	
% 	%$\rho' := G'.\textit{dequeue}()$ \\
% 	%$\textit{S} := S \cup \{ \rho' \} $ \\
% 	\If{$S.\mathit{peek()} = \rho' $}{
% 	  $e' := \bigwedge_{\hat{\rho} \in S}{\hat{\rho}}$ \\	
% 	  \If{ $e'(\mathcal{K}) = T$}{
% 	    \IIf{$\hat{C}(e') < \hat{C}(e)$}{ $e := e'$ \\ }
% 	    
% 	    $S.\mathit{pop}()$ \\
% 	    $S.\mathit{pop}()$ \\
% 	    \IIf {$S = \emptyset$}{ \Return $e$ \\ }
% 	  }
% 	}
%     }
%     \Return $e$
% %\end{small}
% \label{subsec:premi}
% \end{algorithm}%\vspace{-0.6cm}
% \subsection{Implementation}\label{subsec:implementation}
% \subsubsection{Data storage.} We store the KB in a single file using the HDT~\cite{hdt} format. 
% HDT is a binary compressed format, conceived for fast data transfer, that offers 
% reasonable performance for search and browse operations without prior decompression.
% HDT libraries support only the retrieval of bindings for atoms $p(X, Y)$, leaving 
% the execution of additional query operators to upper layers. We used the Apache Jena framework\footnote{\url{https://jena.apache.org/}} (version 3.7)
% as access layer. %\vspace{-0.4cm}
% %We chose HDT with Jena because it offers a good trade-off between por:ility and performance.
% \subsubsection{Algorithms.}
% \label{subsubsec:algorithms}
% REMI and P-REMI are implemented in Java 8. Their runtime is dominated
% by two phases: (1) the construction of the priority queue of subgraph expressions (line 2 in Alg.~\ref{alg:remi}), and (2)
% the DFS exploration (lines 4-8). The first phase is computationally expensive
% because it requires the calculation of $\hat{C}$ on large sets 
% of subgraph expressions, leading to the execution of expensive queries on the KB.
% To alleviate this fact, we parallelized the construction and sorting of the queue
% and applied a series of pruning heuristics.
% %First, we materialize the facts using inverse predicates only for 
% %simple entities, i.e., those among the 5\% most prominent entities. 
% % If we denote this set by $S \subset I$, we add a fact 
% % $p^{-1}(o, s)$ to $\mathcal{K}$ for each $p(s, o) \in \mathcal{K}$ only if $o \in S$. 
% First, the routine $\textit{subgraphs-expressions}$ ignores expressions
% of the form $p(x, B)$ with $B \in \mathcal{B}$, since blank nodes are by conception irrelevant entities.
% However, the routine always derives paths that ``hide'' blank nodes, that is, 
% $p(x, y) \land p'(y, I)$ (such that $y$ binds to $B$ and $I \in \mathcal{I}$) is always considered.
% %In addition, we avoid expressions with consecutive occurrences of pairs of inverse predicates (e.g., $p(x, y) \land p^{-1}(y, I)$). 
% Conversely, we do not derive multi-atom subgraph expressions
% from atoms with object entities among the 5\% most prominent entities. 
% For example, we do not explore extensions of $\textit{capitalOf}(x, \mathit{Germany})$
% such as $\textit{capitalOf}(x, y) \land \textit{locatedIn}(y, \emph{Europe})$, because 
% the complexity of the additional atom will likely be higher than the complexity of a simple entity such as \emph{Germany}. 
% %Moreover, the construction of the priority queue is also highly parallelized.
% % Moreover, the complexity $\hat{C}$ for large queues of subgraph expressions is calculated in parallel by splitting 
% % the queue into as many fragments as available processors. Then, the priority queue is sorted via bucket sort. 
% Finally, REMI requires the execution of the same queries multiple times, thus query results are cached
% in a least-recently-used fashion. %\vspace{-0.4cm}
% \subsubsection{Complexity function.}
% The calculation of $\hat{C}$ requires the construction of multiple rankings on prominence
% for concepts in the KB. For example,
% to calculate   
% $\hat{C}(\textit{capital}(x, \textit{Paris}))$, we need the rank $k(\textit{capital})$
% among all predicates, as well as the rank $k(\textit{Paris} \;|\; \textit{capital})$ 
% among all capital cities. 
% Even though predicates are always evaluated against the same ranking,
% an entity may rank differently depending on the context.  
% % This also true for predicates that do not contain the target variable, e.g., \emph{country} in
% % $g' = \textit{residence}(x, y) \land \textit{country}(y, \mathit{UK})$. In this example, 
% % the position of \emph{country}
% % is calculated only among those predicates with subject-object joins with the predicate \emph{residence}. 
% We could precompute $k(I \;|\; p)$ for every $I$, $p$ in the KB, however we can leverage the
% correlation between prominence and rank to reduce the amount of stored information. 
% It has been empirically shown that the frequency of 
% terms in textual corpora follows a power-law distribution~\cite{IR}. 
% If $\mathit{fr}(k)$ is the frequency of the k$^\text{th}$
% most frequent term in a corpus, $\mathit{fr}(k) \approx \hat{\beta} k^{-\hat{\alpha}}$ for some constants $\hat{\alpha}, \hat{\beta} > 0$. 
% If we treat all the facts $p(s, o) \in \mathcal{K}$ with the same predicate $p$ as a corpus, we can 
% estimate the number of bits of an entity given $p$ from its conditional frequency $\mathit{fr}(I \;|\;p) = |I: \exists s : p(s, I) \in \mathcal{K}| $ as follows:
% \begin{small}
% \begin{equation}\label{eq:powerlaw}
% \mathit{fr}(I \;|\; p) \approx \hat{\beta} \times k(I \;|\; p)^{-\hat{\alpha}} \therefore \; \mathit{log}_2(k(I \;|\; p)) \approx -\alpha \mathit{log}_2(\mathit{fr}(I \;|\; p)) + \beta
% \end{equation}
% \end{small}
% \noindent We can thus learn the 
% coefficients $\alpha$ and $\beta$ that map frequency in the KB to complexity in bits.  
% While this still requires us to precompute the conditional rankings, Equation~\ref{eq:powerlaw} allows us to ``compress'' them
% as a collection of pairs of coefficients (one per predicate).
% Our results on two KBs confirm the linear correlation between the logarithms of rank and frequency,
% since the fitted functions exhibit an average $R^2$ measure of 0.85 in DBpedia
% and 0.88 in Wikidata\footnote{Values closer to 1 denote a good fit.}.
% Likewise, this power-law correlation extrapolates to the Wikipedia page rank, which reveals an 
% average $R^2$ of 0.91 in DBpedia. 
% 
% % \noindent If we are interested in the conditional rank of a concept w.r.t. a conjunctive expression,
% % we can adjust Equation~\ref{eq:powerlaw} via a correction term denoted by $\mathit{log}_2(\gamma)$.
% % For example, we can
% % approximate $\mathit{log}_2(k(\mathit{Socialist} | \mathit{mayor}(x, y) \land \mathit{party}(y, z))$ 
% % by $\mathit{log}_2(k(\mathit{Socialist} | \mathit{party})) - \mathit{log}_2(\gamma)$ with:
% % \begin{small}
% % \[1 \le \gamma = \frac{|\text{approximative ranking}|}{|\text{real ranking}|} = \frac{\#o: \mathit{party}(s, o)}{\#o: \mathit{mayor}(s', s) \land \mathit{party}(s, o)   } \]
% % \end{small}
% % In other words, $\mathit{log}_2(\gamma)$ accounts for the reduction in the length of descriptions 
% % imposed by the fact that the approximative ranking is usually larger than the real ranking. 
% % Indeed, the ranking of all parties used for the approximation is at least as large as 
% % the ranking of parties with mayors among their adherents.
% %This very same principle can be applied to estimate the conditional complexity of predicates. 
% % For instance, we
% % can approximate $\mathit{log}_2(k(\mathit{childCompany} \;|\; \mathit{inCity}))$ as 
% % $\mathit{log}_2(k(\mathit{inCity})) + \mathit{log}_2(\gamma')$ with% $\gamma'$ is calculated as follows:
% % \[\gamma' = \frac{\#p: \mathit{childCompany}(s', s) \land \mathit{p}(s, o) }{ |\mathcal{P}| } \]
% 
% % g' = \textit{residence}(x, y) \land \textit{country}(y, \mathit{UK})
% 

\subsection{RDF Knowledge Bases}
\label{subsec:kbs}
This work focuses on mining REs on 
RDF knowledge bases (KBs). A KB $\mathcal{K}$ is a set of 
assertions in the form of facts $p(s, o)$ with predicate $p\in\mathcal{P}$, 
subject $s\in\mathcal{I} \cup \mathcal{B}$, and object 
$o \in \mathcal{I} \cup \mathcal{L} \cup \mathcal{B}$. In this formulation, 
$\mathcal{I}$ is a set of entities such as London, 
$\mathcal{P}$ is a set of predicates, e.g., \emph{cityIn}, $\mathcal{L}$ is a set of
literal values such as strings or numbers,  
and $\mathcal{B}$ is a set of blank nodes, i.e., anonymous entities. %~\cite{rdf}. 
An example of an RDF triple is \fact{London}{cityIn}{UK}. KBs often include assertions 
that state the class of an entity, e.g., \fact{UK}{is}{Country}. 
%Furthermore, for each predicate $p \in \mathcal{P}$ we define its
%inverse predicate $p^{-1}$ as the relation consisting of all facts  
%$p^{-1}(o, s)$ such that $p(s, o) \in \mathcal{K}$\footnote{To be RDF-compliant,
%$p^{-1}$ is defined only for triples with $o \in \mathcal{I} \cup \mathcal{B}$}.  
% There are plenty of publicly available KBs in the Web that model knowledge from different domains
% such as life sciences, governmental information, media, etc. Some KBs such as YAGO~\cite{yago}, DBpedia~\cite{dbpedia}
% or Wikidata~\cite{wikidata} consist of large collections of facts extracted from Wikipedia. KBs are also important for industry:
% all major search engines nowadays resort to KBs in order to ``understand'' queries better and 
% deliver more semantic results. KBs are also one of the building blocks of smart assistants such as Siri or Cortana.

\subsection{Referring Expressions}
\label{subsec:res}
\subsubsection{Atoms.}
An \emph{atom} $p(X, Y)$ is an expression such that $p$ is a predicate and 
$X$, $Y$ are either variables or constants. 
We say an atom has matches in 
a KB $\mathcal{K}$ 
if there exists a function $\sigma \subset \mathcal{V} \times (\mathcal{I} \cup \mathcal{L} \cup \mathcal{B})$ 
from the variables $\mathcal{V}$ of the atom to constants in the KB 
such that $\mu_{\sigma}(p(X, Y)) \in \mathcal{K}$. The operator $\mu_{\sigma}$ returns a new atom such that the constants
in the input atom are untouched, and 
variables are replaced by their corresponding mappings according to $\sigma$.
We call $\mu_{\sigma}(p(X, Y))$ a bound atom and $\sigma$ a matching assignment.
We extend the notion of matching assignment to conjunctions of atoms, i.e., $\sigma$
is a matching assignment for $\bigwedge_{1 \le i \le n}{p_i(X_i, Y_i)}$ iff 
$\mu_{\sigma}(p_i(X_i, Y_i)) \in \mathcal{K}$ for $1 \le i \le n$.

\subsubsection{Expressions \& Language Bias}
\label{subsubsec:expressions}
Atoms are traditionally the building blocks of referring expressions.
We say that two atoms are connected if they share at least one variable argument.
Most approaches for RE mining define REs as conjunctions of connected atoms with bound objects.
We call this language bias, \emph{the state-of-the-art language bias}.
%The state-of-the-art language bias for REs consists of conjunctions of atoms with bound objects.
We extend this language by allowing atoms with
additional existentially quantified variables. For this purpose, we propose subgraph expressions
as the new building blocks for REs.
%We say that two atoms are connected if they share at least one variable argument. 
A \emph{subgraph expression} $\rho = p_1(x, Y_1) \land \bigwedge_{1 < i \le n}{p_i(X_i, Y_i)}$, rooted at variable $x$,
is a conjunction of connected atoms such that %(1) there is at least an atom that contains $x$ as first
% argument\footnote{If $x$ is the second argument as in $p(Y, x)$, we can rewrite the atom as $p^{-1}(x, Y)$} 
%and (2) 
for $i>1$, atoms are transitively connected to $p_1(x, Y_1)$ via at least another variable besides $x$. Examples are: 
(i) $\mathit{cityIn}(x, \mathit{France})$, 
and (ii) $\mathit{cityIn}(x, y) \land \mathit{officialLang}(y, z) \land \mathit{langFamily}(z, \mathit{Romance})$.
An \emph{expression} $e = \bigwedge_{1 \leq j \leq m} \rho_j$ is a conjunction of 
subgraph expressions rooted at the same variable $x$
such that they have only $x$---the root variable---as common variable. 
% 
% %\begin{small}
% \begin{enumerate}[leftmargin=*]
%  \item $\mathit{cityIn}(x, \mathit{France})$
%  \item $\mathit{cityIn}(x, y) \land \mathit{officialLanguage}(y, z) \land \mathit{langFamily}(z, \mathit{Romance})$
%  \item $\mathit{cityIn}(x, y) \land \mathit{largestCity}(x, y)$
% \end{enumerate}
% %\end{small}
% % In contrast, the following expressions are not considered subgraph expressions rooted at $x$:
% % \begin{enumerate}
% %  \item $\mathit{cityIn}(x, \mathit{France}) \land \mathit{largestCityIn}(x, \mathit{France})$
% %  \item $\mathit{cityIn}(x, y) \land \mathit{largestCity}(x, z)$
% % \end{enumerate}
% % This happens because the atoms should share at least one variable besides the root variable.
% A subgraph expression $\rho$ has matches in a KB $\mathcal{K}$ 
% if there is an assigment $\sigma$ from the variables in the expression to constants in the KB 
% such that $\mu_{\sigma}({p_i(X_i, Y_i)}) \in \mathcal{K}$ for $1\leq i \leq n$ in the subgraph expression.
% Subgraph expressions are the building blocks of referring expressions, thus 
% we define an \emph{expression} $e = \bigwedge_{1 \leq j \leq m} \rho_j$ as a conjunction of 
% subgraph expressions rooted at the same variable $x$
% such that the expressions have only $x$---called the root variable---as common variable. 
% %We call $x$ the root variable of the expression.
% An expression $e$ has matches in a KB if there is an assigment $\sigma$ that yields matches for every subgraph expression 
% of $e$.
Finally, we say $e$ is a \emph{referring expression} (RE) 
for a set of target entities $T \subseteq \mathcal{I}$ in a KB 
$\mathcal{K}$ iff:
 \begin{enumerate}[leftmargin=*]
  \item $\forall t \in T : \exists \sigma : (x \mapsto t) \in \sigma$, i.e., 
  for every target entity $t$, there exists a matching assignment $\sigma$ in $\mathcal{K}$ 
  that binds the root variable $x$ to $t$.
  \item $\nexists\sigma', t' : (x \mapsto t') \in \sigma' \land \;t' \not\in T$, 
  in other words, no matching assignment binds the root variable to entities outside the set $T$ of target entities.
 \end{enumerate}
 \noindent For example, consider a complete and accurate KB $\mathcal{K}$ as well as 
 the following conjunction of two subgraph expressions:  %$e$ rooted at $x$ and consisting of two subgraph expressions:
 %\begin{small}
 \[e = \textit{in}(x, \textit{S. America}) \land \textit{officialLang}(x, y) \land \textit{langFamily}(y, \textit{Germanic}) \]
 %\end{small}
\noindent We say that $e$ is an RE for $T = \{\mathit{Guyana},\mathit{Suriname}\}$ in $\mathcal{K}$ 
because matching assignments can only bind $x$ to these two countries.

While we do not limit the number of subgraph expressions in REs, 
we do not allow more than one variable and three atoms in individual subgraph expressions, 
leading to the expressions in Table~\ref{tab:language}. 
This design decision aims at keeping both the search space and the complexity of the REs under control.
Indeed, expressions with multiple non-root variables 
%are orders of magnitude more numerous, and 
make 
comprehension and translation to natural language more effortful. 
% as in 
% $\mathit{cityIn}(x, y) \land \mathit{officialLang}(y, z) \land \mathit{langFamily}(z, \mathit{Latin})$.
%Table~\ref{tab:language} summarizes the language of subgraph expressions supported by REMI.

% A subgraph expression 
% Such 
% We do not allow for subgraph expressions with more than one variable for the sake of clarity. In our previous  
% and three atoms the number of variables and atoms in subgraph expressions to 1 an form of subgraph expressions to the
% expressions in Table~\ref{tab:language} for the sake of efficiency and clarity. 
% We highlight, however, that referring expressions with
% We limit the language of REs to subgraph expressions with one 





% We highlight that our definition of expression does not consider arbitrarily complex queries because we aim 
% at simple descriptions for entities. In that regard, constraints on the other variables are less interesting
% to us because they add complexity to the expression and boost the size of the search space. 
% Nevertheless, these additional variables provide further exhaustivity in the search that may be useful 
% to find REs in cases where the KB does not know many facts about the target entities.
% We elaborate on this observation when describing REMI's language bias in Section~\ref{subsec:languagebias}.

% \subsubsection{Parallels to Databases, Inductive Logic Programming and Pattern Mining}
% \label{subsubsec:parallels}
% The techniques used in REMI were inspired on views of the problem from three different domains, namely databases, 
% inductive logic programming (ILP), and pattern mining. From a database perspective, 
% an RE is a query with bag semantics such that its
% results are only the entities we aim to describe. This means that mining REs is a query induction task. Query
% induction has been intensively studied in the database literature~\cite{XX, YY} and is usually tackled by 
% systematically exploring the search space of queries until a solution that fits the results is found.
% This very same principle is usually followed by ILP and rule mining approaches. Given a set of positive and 
% negative examples for a predicate, ILP
% aims at finding hypotheses (expressions) that explain all positive examples and none of the negative examples. In 
% this line of thought, an REs is rules and the target entities 
% are its positive examples. Indeed, in the
% experimental section we compare REMI against a state-of-the-art ILP system~\cite{amie}. Finally REMI's search strategy, 
% described in Section~\ref{subsec:algorithm}
% borrows techniques from frequent itemset mining, specifically from the task of mining generators. 
% In pattern mining a database is modeled as a set of transactions, each consisting of a set of items.
% A pattern is a motif, i.e., a set of items that occur frequently in transactions. Patterns describe sets of transactions
% in a compact way. A given set of transactions can be described by a large number of patterns; these patterns
% constitute an equivalence class. 
% Among those patterns, the \emph{generators} are the patterns of minimal size. 
% We observe that mining REs is equivalent to find generators if
% we model graph expressions as items, and entities as transactions.
% A transaction (entity) ``contains'' an item (subgraph expression) if the expression 
% has the entity among its matches for the root variable. Nevertheless, we show in Section~\ref{sec:algorithm} that
% mining intuitive REs differs from generator mining because minimality is not anymore defined in terms of 
% number of items (subgraph expressions) but in terms of the \emph{description complexity} 
% of the composite expression.  


% Finally we define a path expression $p$ for a variable $x$ as a transitively connected conjunction of atoms such t
% The techniques used for REMI are inspired on views from two additional fields:
% 
% \paragraph{Database perspective.} An RE for a set of entities $T$ and a variable $x$ is a query with set semantics 
% such that result set for $x$ is the set the target entities.

% \paragraph{Pattern mining perspective.} If each entity is a transaction, and each subexpression start
% An RE for a set of entities $T$ and a variable $x$ is an itemset with transaction

% \subsubsection{Kolmogorov Complexity}
% \label{subsubsec:kolmogorov}
% The Kolmogorov complexity $C(s)$ of a string $s$ is a measure of the absolute 
% amount of information conveyed by $s$ and is defined
% as the length in bits of its shortest effective binary description~\cite{zellner2001simplicity}.
% For example, consider a string $s_1 = aaa\dots$, where $a$ is repeated 100000 times 
% versus a string $s_2$ consisting of 100000 randomly generated
% characters. $s_1$ has a smaller Kolmogorov complexity than $s_2$ because
% its regularities make it more \emph{compressible}, i.e., its absolute information content is small. 
% Indeed, if $b(s)$ and $l(s)$ denote 
% the binary representation of a string $s$ and its length, 
% we can compactly describe $s_1$ as 
% $\hat{s}_1 = l(a)b(a)l(100000)b(100000)$ (``a'' followed by 100000), plus a program $M$ 
% that takes the first argument and outputs it as many times as specified by the second argument. 
% We say that $C(s_1) = l(\hat{s_1}) + l(M)$. Put differently, the Kolmogorov complexity of $s_1$ is the size
% of the compressed string plus the program to decompress it. In contrast, the lack of regularities in
% $s_2$ do not allow for such a compact representation, thus $s_2$ has higher Kolmogorov complexity. 
% % A string $s$ is said to be random if the string is not compressible ($C(s) = l(s)$), i.e.,
% % its minimal representation is itself. We observe that $l(s)$ defines an 
% % upper bound for $C(s)$.
% The Kolmogorov complexity of a string $s$ cannot be computed because it requires us to 
% know 
% the program of minimal size that can 
% decompress $\hat{s}$ among all programs. Despite this negative result, it is shown~\cite{zellner2001simplicity} that the length of
% all computable programs is a constant factor larger than this optimal program. This means that 
% the selection of an arbitrary program $\hat{M}$ will provide an approximation of the Kolmogorov
% complexity $\hat{C}(s)$ that is $O(1)$ worse than the real complexity, i.e., $\hat{C}(s) = C(s) + O(1)$.
 \begin{table}
% %\resizebox{\columnwidth}{!}{
   %\centering
   \begin{tabular}{ l  l  }
     \toprule
     1 atom & $p_0(x, I_0)$ \\
     Path & $p_0(x, y) \land p_1(y, I_1)$ \\
     Path + star & $p_0(x, y) \land p_1(y, I_1) \land p_2(y, I_2) $ \\
     2 closed atoms\; & $p_0(x, y) \land p_1(x, y)$ \\ 
     3 closed atoms\;  & $p_0(x, y) \land p_1(x, y) \land p_2(x, y)$ \\ \bottomrule
   \end{tabular}
% %}
   \caption{REMI's subgraph expressions.}
   \label{tab:language} \vspace{-0.4cm}
 \end{table}
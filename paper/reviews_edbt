----------------------- REVIEW 1 ---------------------
SUBMISSION: 238
TITLE: REMI: Mining Intuitive Referring Expressions on Knowledge Bases
AUTHORS: Luis Galárraga, Julien Delaunay and Jean-Louis Dessalles
>>
----------- Overall evaluation -----------
SCORE: 1 (weak accept)
----------- Relevance -----------
SELECTION: yes
----------- Summary of the paper -----------
The authors propose a new parallelizable method for the mining of Referring
Expressions. The paper formalizes the problem together with additional concepts
for reader understanding. The authors contextualize their work in the first
section to demonstrate effective applicability. The experimental sections
include both qualitative analysis employing human evaluation and timing
performance analysis. The paper reads well and, depending on the reader
background could lead to several ideas.
----------- Three (or more) strong points about the paper -----------
1. The concepts are clearly presented.
2. The code and data are available open-source.
3. The experimental section includes detailed timing performance analysis.
----------- Three (or more) weak points about the paper -----------
1. The qualitative evaluation by humans could lead to biased results (not
statistical sampling).
2. Some concepts can be detailed early in the text to help reader understanding.
3. The full experimental comparison in the technical report, if not reported,
should be referenced as existing in the paper.
----------- Novelty -----------
SCORE: 4 (Novelty unclear)
----------- Significance -----------
SCORE: 2 (Improvement over existing work)
----------- Technical Depth and Quality of Content -----------
SCORE: 4 (Solid work)
----------- Experiments -----------
SCORE: 3 (OK, but certain claims are not covered by the experiments)
----------- Presentation -----------
SCORE: 2 (Reasonable: improvements needed)
----------- Detailed Evaluation (Contribution, Pros/Cons, Errors) -----------
The paper presents an interesting method to mine referring expressions using
knowledge bases. The paper is well written but certain aspects could be better
analyzed/clarified:
>>
- The explanation for "ranking by prominence" should be anticipated to help
readers understanding.
- Does not exists any competitive systems performing similar tasks (apart from
AMIE)?
- It is unclear why the authors set a timeout limit in the experimental
comparison.
- Can the method be seen as an extension of AMIE?
>>
>>
>>
----------------------- REVIEW 2 ---------------------
SUBMISSION: 238
TITLE: REMI: Mining Intuitive Referring Expressions on Knowledge Bases
AUTHORS: Luis Galárraga, Julien Delaunay and Jean-Louis Dessalles
>>
----------- Overall evaluation -----------
SCORE: 2 (accept)
----------- Relevance -----------
SELECTION: yes
----------- Summary of the paper -----------
The paper proposes a method to mine intuitive referring expression (RE) of large
knowledge bases.
----------- Three (or more) strong points about the paper -----------
S1: The paper handles a  real problem
S2: The paper is well written, easy to understand
S3: A  theoretical contribution and validation through a user study.
S4: Code and the experimental data are publicly available
----------- Three (or more) weak points about the paper -----------
W1: The paper defines more expressive language but finally the expressivity is
limited for complexity reasons.
"While we do not limit the number of subgraph expressions in
REs, we do not allow more than one variable and three atoms
in individual subgraph expressions, leading to the subgraph expressions
in Table 1."
>>
W2:  The number of participants in the study is unknown, the gender, age ..etc
----------- Novelty -----------
SCORE: 5 (With some new ideas)
----------- Significance -----------
SCORE: 2 (Improvement over existing work)
----------- Technical Depth and Quality of Content -----------
SCORE: 4 (Solid work)
----------- Experiments -----------
SCORE: 4 (Very nicely support the claims made in the paper)
----------- Presentation -----------
SCORE: 3 (Excellent: careful, logical, elegant, easy to understand)
----------- Detailed Evaluation (Contribution, Pros/Cons, Errors) -----------
The paper proposes three contributions for mining intuitive RE:  1) A scheme
based on information theory to quantify the intuitiveness f entity descriptions
extracted from a KB. 2) REMI, an algorithm to mine intuitive REs on large KBs.
3) A user study to assess the intuitiveness of REMI’s descriptions.
>>
The paper is well written, easy to understand even for a non-expert in the
domain.
Ideas, approaches, and algorithms are illustrated with examples.
>>
Questions and remarks:
>>
1) Section 2.1: that state the class  -> that states the class
2) Section 2.2: the evaluation of atoms and expressions are similar to the
evaluation of a SPARQL triple pattern and a basic graph pattern. Could you rely
on the SPARQL evaluation as defined in:
>>
Jorge Pérez, Marcelo Arenas, and Claudio Gutierrez. 2009. Semantics and
complexity of SPARQL. ACM Trans. Database Syst. 34, 3, Article 16 (September
2009), 45 pages. DOI: https://doi.org/10.1145/1567274.1567278
>>
instead of defining your own semantics?
>>
>>
In summary, the proposed mining algorithm is accompanied by an evaluation. The
work is mature enough to be published as a short research paper in EDBT 2020
conference.
>>
>>
>>
----------------------- REVIEW 3 ---------------------
SUBMISSION: 238
TITLE: REMI: Mining Intuitive Referring Expressions on Knowledge Bases
AUTHORS: Luis Galárraga, Julien Delaunay and Jean-Louis Dessalles
>>
----------- Overall evaluation -----------
SCORE: 0 (borderline paper)
----------- Relevance -----------
SELECTION: no
----------- Summary of the paper -----------
The paper discusses the problem of creating referring expressions in knowledge
bases. It uses the Kolmogorov complexity to quantify the value of an
expression. It presents an algorithm that performs that task and the results of
the evaluation of the specific algorithm. The paper unfortunately, fails to
provide enough technical details on the algorithms, a description of them
technical challenges of the problem and what advanced technical solutions the
algorithmic solution brings.
----------- Three (or more) strong points about the paper -----------
S1. It is interesting and very useful as usual to be able to infer new RE from a
knowledge base
>>
S2. The usefulness of the RE  generated with the specific method looks high
according to the performed experiments
>>
S3.  Easy to read text
----------- Three (or more) weak points about the paper -----------
W1. The algorithm presented in the paper is straight forward. It could be as a
base line as well. The paper does not describe any advanced solution or any
technical optimizations made to improve performance. It implements the straight
forwards approach and then simply evaluates it. In that sense, the clear
technical contribution of the paper is questionable,
>>
W2. I understand that this is a short paper but given the fact that experiments
are presented, I would have expected to see what the authors have learned from
these experiments. The experiments are mainly presenting results without any
insights.
>>
W3. The related work at the beginning states some limitations in terms of
semantics. But from the data management perspective, it provided very few
information. Where is the database technology issues that make it related to
EDBT..?
----------- Novelty -----------
SCORE: 4 (Novelty unclear)
----------- Significance -----------
SCORE: 2 (Improvement over existing work)
----------- Technical Depth and Quality of Content -----------
SCORE: 3 (Syntactically complete but with limited contribution)
----------- Experiments -----------
SCORE: 3 (OK, but certain claims are not covered by the experiments)
----------- Presentation -----------
SCORE: 3 (Excellent: careful, logical, elegant, easy to understand)
----------- Detailed Evaluation (Contribution, Pros/Cons, Errors) -----------
<See weak points>

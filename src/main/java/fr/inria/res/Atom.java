package fr.inria.res;

public class Atom implements Cloneable {
	private String subject;
	private String predicat;
	private String object;
	private String objectHidden;
	
	public Atom(){}
		
	public Atom(String subject, String predicat, String object, String objectHidden) {
		this.subject = subject;
		this.predicat = predicat;
		this.object = object;
		this.objectHidden = objectHidden;
	}
		
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public void setPredicat(String predicat) {
		this.predicat = predicat;
	}
	
	public void setObject(String object) {
		this.object = object;
	}

	public void setObjectHidden(String objectHidden) {
		this.objectHidden = objectHidden;
	}
	
	public String getSubject() {
		return this.subject;
	}
	
	public String getPredicat() {
		return this.predicat;
	}
	
	public String getObject() {
		return this.object;
	}
	
	public String getObjectHidden() {
		return this.objectHidden;
	}

	@Override
	public String toString() {
		if(object.equals("")) {
			return "Atom [subject=" + subject + ", predicat=" + predicat + ", hiddenObject=" + objectHidden+ "]";
		}
		else {
			return "Atom [subject=" + subject + ", predicat=" + predicat + ", object=" + object+ "]";
		}
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((object == null) ? 0 : object.hashCode());
		result = prime * result + ((predicat == null) ? 0 : predicat.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
	//	result = prime * result + ((objectHidden == null) ? 0 : objectHidden.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atom other = (Atom) obj;
		if (object == null) {
			if (other.object != null)
				return false;
		} else if (!object.equals(other.object))
			return false;
/*		if (objectHidden == null) {
			if (other.objectHidden != null)
				return false;
		} else if (!objectHidden.equals(other.objectHidden))
			return false;
	*/	if (predicat == null) {
			if (other.predicat != null)
				return false;
		} else if (!predicat.equals(other.predicat))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		return true;
	}
	
	public String[] toArray() {
		return new String[] {subject, predicat, object};
	}

	@Override
	public Atom clone() {
		return new Atom(subject, predicat, object, objectHidden);
	}

}

package fr.inria.res;


import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;

import fr.inria.res.data.KB;

public class REMiningJob implements Runnable {

	private Condition[] masterQueue;
	
	private List<String> entities2Describe;
	
	private REMiner parentJob;
	
	private KB kb;
	
	private AtomicInteger cursor;
	
	private ReadWriteLock rwlock;
	
	private Lock wlock;
	
	private Lock rlock;
	
	private ReadWriteLock cursorRWLock;
	
	private AtomicInteger finishedCursor;
	
	private Solution bestSolution;
			
	public REMiningJob(Condition[] sharedMasterQueue, 
			List<String> entities2Describe, REMiner parentJob, AtomicInteger cursor, 
			ReadWriteLock rwlock, ReadWriteLock cursorRWLock, AtomicInteger finishedCursor,
			Solution solution) {
		this.masterQueue = sharedMasterQueue;
		this.entities2Describe = entities2Describe;
		this.parentJob = parentJob;
		this.kb = parentJob.getKB();
		this.cursor = cursor;
		this.rwlock = rwlock;
		this.cursorRWLock = cursorRWLock;
		this.finishedCursor = finishedCursor;
		this.rlock = this.rwlock.readLock();
		this.wlock = this.rwlock.writeLock();
		this.bestSolution = solution;
	}

	@Override
	public void run() {
		List<String> coveredEntities = new ArrayList<>();
		Stack<Condition> stack = new Stack<Condition>();
		Stack<Integer> indexesStack = new Stack<Integer>();
		Set<String> setEntities2Describe = new LinkedHashSet<>(entities2Describe);
		int masterIndex = cursor.getAndDecrement();		
		// first loop of condition
		while (masterIndex >= 0) {
			this.rlock.lock();
			double bestComplexity = bestSolution.getComplexity();
			this.rlock.unlock();
	
			if (masterQueue[masterIndex].getComplexity() > bestComplexity)
				break;
			
			int workIndex = masterIndex;
			coveredEntities.clear();
			while (workIndex >= 0) {	
								
				Lock cursorRLock = this.cursorRWLock.readLock();
				boolean finishedRightAway = false;
				cursorRLock.lock();
				// This means we should abort the mission.
				if (masterIndex < finishedCursor.get())
					finishedRightAway = true;				
				cursorRLock.unlock();
				if (finishedRightAway)
					return;
				
				// put the first condition of the sorted queue in the stack
				//System.out.println("T" + Thread.currentThread().getId() + " Stack : " + indexesStack);
				// Check if another thread has already found a solution with lower complexity
				this.rlock.lock();
				bestComplexity = bestSolution.getComplexity();
				this.rlock.unlock();
				double latestComplexity = masterQueue[workIndex].getComplexity();
				
				do {
					if (REMiner.getComplexity(stack) + latestComplexity > bestComplexity) {
						if (!stack.isEmpty()) {
							stack.pop();
							indexesStack.pop();
						} else {
							break;
						}
					} else {
						stack.push(masterQueue[workIndex]);
						indexesStack.push(workIndex);
						break;
					}
					
					this.rlock.lock();
					bestComplexity = bestSolution.getComplexity();
					this.rlock.unlock();					
				} while (!stack.isEmpty());
				
				if (stack.isEmpty()) { 
					workIndex = -1;
					break;
				}
				
				// Impose the max depth argument
				if (stack.size() > parentJob.getMaxDepth()) {
					stack.pop();
					indexesStack.pop();
					stack.pop();
					indexesStack.pop();
					--workIndex;
					continue;
				}
				
				// check that the last condition add is not redundancy and modify the
				// entityCovered by the stack
				coveredEntities = parentJob.getCoveredEntities(this.kb, stack);
				//System.out.println("T" + Thread.currentThread().getId() + " Covered entities : " + coveredEntities);
				SetRelationCase relationCase = parentJob.compareCollectionEntitiesExhaustively(coveredEntities,
						setEntities2Describe);
				if (relationCase == SetRelationCase.EQUALITY) {
					// Update the solution
					this.wlock.lock();
					if (bestSolution.update(stack, indexesStack)) {						
						if (parentJob.isEnableDebugging()) {
							System.out.println("T" + Thread.currentThread().getId() + " Best solution until now: " + 
								bestSolution.getElements());
						}
					}
					
					this.wlock.unlock();
										
					// if we find a solution of size 1 we should not go further in the master queue
					if (stack.size() == 1) {
						cursor.set(-1);
						workIndex = -1;
					} else {
						stack.pop();					
						indexesStack.pop();
					}
					
					stack.pop();
					indexesStack.pop();
					//System.out.println("T" + Thread.currentThread().getId() + " Stack after pruning: " + indexesStack);
					if (stack.isEmpty()) {
						break;
					}
					
				} else {
					--workIndex;
				}

			}
			
			int oldMasterIndex = masterIndex;
			masterIndex = cursor.getAndDecrement();
			boolean isTrivialSolution = false;
			rlock.lock();
			isTrivialSolution = bestSolution.isTrivialSolution();
			rlock.unlock();
			
			if (isTrivialSolution) {
				cursor.set(-1);
				Lock cursorWLock = this.cursorRWLock.writeLock();
				cursorWLock.lock();
				if (oldMasterIndex < finishedCursor.get())
					finishedCursor.set(masterIndex);				
				cursorWLock.unlock();
				break;
			}
		}
	}

}

package fr.inria.res.tools;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import fr.inria.res.data.HDTKB;
import fr.inria.res.data.KB;

public class RelationCardinalityCacheBuilder {

	KB kb;
	
	public static void main(String[] args) throws IOException {
		KB kb;
		String fileName = "src/data/dbpedia/dbpedia.hdt";
		kb = new HDTKB(fileName, false);

		RelationCardinalityCacheBuilder cc = new RelationCardinalityCacheBuilder(kb);
		cc.run();
	}
	
	public RelationCardinalityCacheBuilder(KB kb) {
		this.kb = kb;
	}

	
	public void run() throws FileNotFoundException {
		// SELECT (sum(?co) as ?cc) WHERE { SELECT ?p (count(distinct ?o) as ?co) WHERE { ?s0 ?p ?o  . ?s1 <http://dbpedia.org/ontology/capital> ?s0  . } GROUP BY ?p}		
		PrintWriter cached_queries = new PrintWriter(new DataOutputStream(new BufferedOutputStream(new FileOutputStream(
				new File("src/data/dbpedia/cached_relation_queries")))));
		List<String[]> atoms = new ArrayList<>();
		String[] atom = new String[3];
		atom[0] = "";
		atom[1] = "";
		atom[2] = "";
//		Set<String> predicates = kb.selectDistinct("?p", atoms);
//		for (String predicate : predicates) {
//			System.out.println(predicate);
/**			Set<String> resultOfKBSelectDistinct
			Set<String> resultOfKBSelectDistinct = null;
			
			int length = resultOfKBSelectDistinct.size();
			for (String str : resultOfKBSelectDistinct) {
				atom[0] = "?x";
				atom[1] = "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>";
				atom[2] = str;
				List<String[]> atoms = new ArrayList<>();
				atoms.add(atom);
				long numberOfObject = kb.countDistinct("?x", atoms);
				cached_queries.println(str + " " + numberOfObject);
				System.out.println(str + " " + numberOfObject);
	
			}
			
			if (cached_queries != null)
				cached_queries.close();

		} **/
	}

}

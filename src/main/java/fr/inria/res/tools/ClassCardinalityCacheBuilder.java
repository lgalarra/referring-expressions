package fr.inria.res.tools;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import fr.inria.res.data.HDTKB;
import fr.inria.res.data.KB;

public class ClassCardinalityCacheBuilder {

	
	public ClassCardinalityCacheBuilder(KB kb) throws IOException {
		PrintWriter cached_queries = new PrintWriter(new DataOutputStream(new BufferedOutputStream(new FileOutputStream(
				new File("src/data/dbpedia/cached_queries")))));
		String[] atom = new String[3];
		atom[0] = "";
		atom[1] = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
		atom[2] = "";
		Set<String> resultOfKBSelectDistinct = kb.selectDistinctO(atom);
		int length = resultOfKBSelectDistinct.size();
		for (String str : resultOfKBSelectDistinct) {
			atom[0] = "?x";
			atom[1] = "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>";
			atom[2] = str;
			List<String[]> atoms = new ArrayList<>();
			atoms.add(atom);
			long numberOfObject = kb.countDistinct("?x", atoms);
			cached_queries.println(str + " " + numberOfObject);
			System.out.println(str + " " + numberOfObject);

		}
		
		if (cached_queries != null)
			cached_queries.close();
	}

	public static void main(String[] args) throws IOException {
		KB kb;
		String fileName = "src/data/dbpedia/dbpedia.hdt";
		kb = new HDTKB(fileName, false);

		ClassCardinalityCacheBuilder cc = new ClassCardinalityCacheBuilder(kb);
	}

}

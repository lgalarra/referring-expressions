package fr.inria.res.tools;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import fr.inria.res.data.HDTKB;
import fr.inria.res.data.KB;

/**
 * Class to output in the text "samplerFile" the list of random entities pick in the different files 
 * like the top 5% entities for example !
 * 
 * You choose number of entities you want per lines with the first loop "j"
 * and then you choose number of lines you want with the second loop "k"
 * 
 * @author jdelauna
 *
 */

public class REMinerInputSampler {

	public REMinerInputSampler(KB kb) throws IOException {
		PrintWriter samplerFile = new PrintWriter(new DataOutputStream(
				new BufferedOutputStream(new FileOutputStream(new File("src/data/dbpedia/extraEntities")))));
		BufferedReader topEntitiesFilmsAlbum = null;
		BufferedReader topEntitiesPersonn = null;
		BufferedReader topEntitiesCompany = null;
		BufferedReader topEntitiesEvent = null;
		BufferedReader topEntitiesSettlement = null;

		/*
		 * choose the different file with the name of the different entities
		 */
		try {
			topEntitiesFilmsAlbum = new BufferedReader(new FileReader(
					"src/data/dbpedia/topentities-for-classes-0_05-httpdbpediaorgontologyAlbum-httpdbpediaorgontologyFilm"));
			topEntitiesCompany = new BufferedReader(new FileReader(
					"src/data/dbpedia/topentities-for-classes-0_05-httpdbpediaorgontologyCompany-httpdbpediaorgontologyOrganisation-httpdbpediaorgontologyUniversity"));
			topEntitiesEvent = new BufferedReader(
					new FileReader("src/data/dbpedia/topentities-for-classes-0_05-httpdbpediaorgontologyEvent"));
			topEntitiesPersonn = new BufferedReader(
					new FileReader("src/data/dbpedia/topentities-for-classes-0_05-httpdbpediaorgontologyPerson"));
			topEntitiesSettlement = new BufferedReader(
					new FileReader("src/data/dbpedia/topentities-for-classes-0_05-httpdbpediaorgontologySettlement")); 
			
			/**topEntitiesFilmsAlbum = new BufferedReader(new FileReader(
					"src/data/wikidata/topentities-for-classes-0_05-httpwikidataorgresourcealbumQ"));
			topEntitiesCompany = new BufferedReader(new FileReader(
					"src/data/wikidata/topentities-for-classes-0_05-httpwikidataorgresourcecityQ"));
			topEntitiesEvent = new BufferedReader(
					new FileReader("src/data/wikidata/topentities-for-classes-0_05-httpwikidataorgresourcecompanyQ"));
			topEntitiesPersonn = new BufferedReader(
					new FileReader("src/data/wikidata/topentities-for-classes-0_05-httpwikidataorgresourcefilmQ"));
			topEntitiesSettlement = new BufferedReader(
					new FileReader("src/data/wikidata/topentities-for-classes-0_05-httpwikidataorgresourcehumanQ"));**/


		} catch (FileNotFoundException exc) {
			System.out.println("Erreur d'ouverture");
		}

		/*
		 * add to a string random entities from the target files
		 */
		String output = outputTopEntities(topEntitiesCompany);
		samplerFile.print(output);

		output = outputTopEntities(topEntitiesPersonn);
		samplerFile.print(output);

		output = outputTopEntities(topEntitiesEvent);
		samplerFile.print(output);

		output = outputTopEntities(topEntitiesSettlement);
		samplerFile.print(output);

		output = outputTopEntities(topEntitiesFilmsAlbum);
		samplerFile.print(output);

		if (samplerFile != null) {
			samplerFile.close();
			topEntitiesFilmsAlbum.close();
			topEntitiesCompany.close();
			topEntitiesSettlement.close();
			topEntitiesPersonn.close();
			topEntitiesEvent.close();
		}
	}

	/**
	 * Take a buffer reader and choose random entities and return string
	 * @param topEntities
	 * @return
	 * @throws IOException
	 */
	public String outputTopEntities(BufferedReader topEntities) throws IOException {
		String output = "";
		int Max = 0;
		topEntities.mark((int) (Math.random() * 1000000000));
		// Discover number of lines in the file
		while (topEntities.readLine() != null) {
			Max++;
		}
		int i = 0;
		topEntities.reset();
		int nombreAleatoire = (int) (Math.random() * Max);
		//Choose number of entities
		for (int j = 2; j >= 1; j--) {
			topEntities.reset();
			nombreAleatoire = (int) (Math.random() * Max);
			while (nombreAleatoire + 5 > Max) {
				nombreAleatoire = (int) (Math.random() * Max);
			}
			// Go to the random lines in the file
			while (i < nombreAleatoire) {
				topEntities.readLine();
				i++;
			}

			// How many lines print in the file
			for (int k = 0; k < 3; k++) {
				for (i = nombreAleatoire; i < nombreAleatoire + j; i++) {
					if (topEntities.readLine() != null) {
						output += topEntities.readLine() + " ";
						topEntities.readLine();
					} else {
						topEntities.reset();
						output += topEntities.readLine() + " ";
					}
				}
				output += "\n";

			}
			topEntities.mark((int) (Math.random() * 1000000000));
		}
		topEntities.close();
		return output;

	}

	public static void main(String[] args) throws IOException {
	
		String fileName = "src/data/dbpedia/dbpedia.hdt";
		KB kb = new HDTKB(fileName, false);
		REMinerInputSampler cc = new REMinerInputSampler(kb);
	}

}

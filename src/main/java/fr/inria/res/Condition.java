package fr.inria.res;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.rdfhdt.hdt.exceptions.NotImplementedException;

import fr.inria.res.data.HDTKB;

public class Condition implements Cloneable {

	private Map<Atom, List<Condition>> atoms;
	private double complexity;
	
	/**
	 * Transform 2 String to a condition
	 * 
	 * @param predicate
	 * @param object
	 * @result Condition
	 */
	public static Condition makeCondition(String predicate, String object) {
		return new Condition(predicate, object);
	}

	public Condition() {
		this.atoms = new LinkedHashMap<>();
		this.complexity = -1.0;
	}

	/**
	 * Create a new Condition
	 * 
	 * @param predicat
	 * @param object
	 */
	protected Condition(String predicat, String object) {
		this.atoms = new LinkedHashMap<>();
		Atom atom = new Atom("", predicat, object, null);
		this.atoms.put(atom, new ArrayList<Condition>());
		this.complexity = -1.0;
	}

	public double getComplexity() {
		return complexity;
	}

	public void setComplexity(double complexity) {
		this.complexity = complexity;
	}

	public Map<Atom, List<Condition>> atoms() {
		return this.atoms;
	}

	/**
	 * return the KeySet of the current Condition
	 * 
	 * @return
	 */
	public Set<Atom> getSetAtoms() {
		Set<Atom> setAtom = new LinkedHashSet<>();
		setAtom.addAll(this.atoms.keySet());
		return setAtom;
	}
	
	/**
	 * return all the predicates of all atoms in the KeySet of the current Condition 
	 * @return
	 */
	public Set<String> getPredicatesInAtoms() {
		Set<String> predicates = new LinkedHashSet<>();
		for (Atom p : this.atoms.keySet()) {
			predicates.add(p.getPredicat());
		}
			
		return predicates;
	}
	
	
	public Condition(Atom... atoms) {
		this();
		for (Atom a : atoms) {
			this.atoms.put(a, new ArrayList<>(1));
		}
	}

	/**
	 * Search in the current Condition where is the Atom gives in param and return a
	 * new Condition with this Atom as key
	 * 
	 * @param atom
	 * @return
	 */
	public Condition search(Atom atom) {
		if (this.atoms.containsKey(atom)) {
			return this;
		} else {
			for (List<Condition> listCond : this.atoms.values()) {
				for (Condition cond : listCond) {
					Condition found = cond.search(atom);
					if (found != null) {
						return found;
					}
				}
			}
		}

		return null;
	}

	/**
	 * Adds a new atom to the current Condition if the oldAtom is a root create a
	 * new list of Condition else search the place of the oldAtom and adds the
	 * newAtom
	 * 
	 * @param newAtom
	 * @param referenceAtom
	 * @return
	 */
	public Condition chainAtomSO(Atom newAtom, Atom referenceAtom) {
		if (referenceAtom.getObject().equals(newAtom.getSubject())) {
			Condition clonedCond = this.clone();
			Condition cond = clonedCond.search(referenceAtom);
			Atom referenceAtomModified = new Atom("", referenceAtom.getPredicat(), "", referenceAtom.getObject());
			Condition condNewAtom = new Condition(newAtom.getPredicat(), newAtom.getObject());
			List<Condition> listCond = cond.atoms.get(referenceAtom);
			listCond.add(condNewAtom);
			cond.atoms.remove(referenceAtom);
			cond.atoms.put(referenceAtomModified, listCond);
			return clonedCond;
		} else
			return new Condition();
	}
	
	// NOTE: The variable closingAtom is directly added to the condition
	// so, do not modify it!
	public Condition chainAtomSSOO(Atom closingAtom, Atom referenceAtom) {
		Condition clonedCond = this.clone();
		Condition targetCondition = clonedCond.search(referenceAtom);
		Atom referenceAtomModified = new Atom("", referenceAtom.getPredicat(), "", closingAtom.getObjectHidden());
		List<Condition> listCond = targetCondition.atoms.get(referenceAtom);
		targetCondition.atoms.remove(referenceAtom);
		targetCondition.atoms.put(referenceAtomModified, listCond);
		targetCondition.atoms.put(closingAtom, new ArrayList<>(1));
		return clonedCond;
	}
	
	
	/**
	 * Adds a new atom to the current Condition if the oldAtom is a root create a
	 * new list of Condition else search the place of the oldAtom and adds the
	 * newAtom
	 * 
	 * @param newAtom
	 * @param referenceAtom
	 * @return
	 */
	public Condition addAtom(Atom newAtom, Atom newAtom2, Atom referenceAtom) {

		if (referenceAtom.getObject().equals(newAtom.getSubject()) 
				&& referenceAtom.getObject().equals(newAtom2.getSubject())) {
			Condition clonedCond = this.clone();
			Condition cond = clonedCond.search(referenceAtom);
			Atom referenceAtomModified = new Atom("", referenceAtom.getPredicat(), "", referenceAtom.getObject());
			List<Condition> listCond = cond.atoms.get(referenceAtom);
			Condition cond2Atom = new Condition(new Atom("", newAtom.getPredicat(), newAtom.getObject(), null), 
					new Atom("", newAtom2.getPredicat(), newAtom2.getObject(), null));
			listCond.add(cond2Atom);
			cond.atoms.remove(referenceAtom);
			cond.atoms.put(referenceAtomModified, listCond);
			return clonedCond;
		} else
			return new Condition();
	}
	
	
	/**
	 * Adds a list of new atom to the current Condition if the oldAtom is a root create a
	 * new list of Condition else search the place of the oldAtom and adds the
	 * newAtom
	 * 
	 * @param newAtom
	 * @param oldAtom
	 * @return
	 */
	public Condition addAtoms(List<Atom> listNewAtom, Atom referenceAtom) {
		Condition clonedCond = this.clone();
		List<Condition> listCondNewAtom = new ArrayList<>();
		for (Atom newAtom : listNewAtom) {
			if (!referenceAtom.getObject().equals(newAtom.getSubject())) {
				return new Condition();
			}
			Condition condNewAtom = new Condition(newAtom.getPredicat(), newAtom.getObject());
			listCondNewAtom.add(condNewAtom);
		}

		Condition cond = clonedCond.search(referenceAtom);
		Atom referenceAtomModified = new Atom("", referenceAtom.getPredicat(), "", referenceAtom.getObject());

		List<Condition> listCond = cond.atoms.get(referenceAtom);
		listCond.addAll(listCondNewAtom);
		if(!listCond.isEmpty()) {
			cond.atoms.remove(referenceAtom);
			cond.atoms.put(referenceAtomModified, listCond);
		}
		return clonedCond;
	}

	/**
	 * make the merge of the current Condition and the condition gives in param
	 * 
	 * @param cond
	 */
	public Condition mergeCondition(List<Condition> listCond) {
		Condition cond2Return = new Condition();

		for (int i = 0; i < listCond.size(); i++) {
			Condition cond = listCond.get(i);
			for (Atom atom : cond.atoms.keySet()) {
				List<Condition> listCondValues = cond.atoms.get(atom);
				cond2Return.atoms.putIfAbsent(atom, listCondValues);

			}

		}
		for (Atom currentAtom : this.atoms.keySet()) {
			List<Condition> CurrentListCond = this.atoms.get(currentAtom);
			cond2Return.atoms.put(currentAtom, CurrentListCond);
		}
		return cond2Return;
	}

	/**
	 * Return the last object of the current Atoms
	 * 
	 * @return Object
	 */
	public String getLastObject() {
		Set<Atom> setAtom = this.atoms.keySet();
		Iterator<Atom> itSetAtom = setAtom.iterator();
		Atom atom = new Atom();
		while (itSetAtom.hasNext()) {
			atom = itSetAtom.next();
		}
		return atom.getObject();
		// This should give you the object of the deepest leave in the tree
		// Note: this method makes sense only if we assume a maximum number
		// of atoms of 3. Otherwise we should actually return a list of the
		// objects of the deepest leaves. We can discuss
	}

	/**
	 * Return the last predicate of the current Atoms
	 * 
	 * @return Object
	 */
	public String getLastPredicate() {
		Set<Atom> setAtom = this.atoms.keySet();
		Iterator<Atom> itSetAtom = setAtom.iterator();
		Atom atom = new Atom();
		while (itSetAtom.hasNext()) {
			atom = itSetAtom.next();
		}
		return atom.getPredicat();
		// This should give you the predicate of the deepest leave in the tree
		// Note: this method makes sense only if we assume a maximum number
		// of atoms of 3. Otherwise we should actually return a list of the
		// objects of the deepest leaves. We can discuss
	}

	/**
	 * Return the number of Atom (keySet) in the Condition
	 * 
	 * @return int
	 */
	public int numberAtoms() {
		return this.atoms.keySet().size();
	}

	/**
	 * Return the number of Condition in the Condition
	 * 
	 * @return int
	 */
	public int size() {
		return this.atoms.size();
	}

	/**
	 * Convert a list of Condition to a list of ByteString
	 * 
	 * @param List
	 *            of Condition
	 * @result List of ByteString
	 */
	public List<String[]> convertList(List<Condition> listCond) {
		throw new NotImplementedException("We have to reimplement this method");
	}

	@Override
	public String toString() {
		String variable = "?x0";
		List<String[]> queryResult = HDTKB.condition2Atoms(this, variable);
		String result = "";
		for(int i = 0; i < queryResult.size(); i++) {
			String [] tab = queryResult.get(i);
			for(int j = 0; j < tab.length; j = j+3) {
				result += tab[j + 1] + "(" + tab[j] + ", " + tab[j+2] + ") ";
			}
		}
		return result;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atoms == null) ? 0 : atoms.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Condition other = (Condition) obj;
		if (other.atoms.keySet().size() != this.atoms.keySet().size()) {
			return false;
		}
		for (Atom atom : this.atoms.keySet()) {
			if (other.atoms.keySet().contains(atom)) {
				if (other.atoms.get(atom).size() != this.atoms.get(atom).size()) {
					return false;
				}
				for (Condition childCondition : this.atoms.get(atom)) {
					if (!other.atoms.get(atom).contains(childCondition)) {
						return false;
					}
				}
			} else {
				return false;
			}
		}
		return true;
	}

	private Integer getDepth(Atom a) {
		// If it is a leaf
		if (atoms.get(a).isEmpty())
			return 1;

		Integer maxDepth = 0;
		for (Condition childCondition : atoms.get(a)) {
			for (Atom childAtom : childCondition.atoms.keySet()) {
				Integer currentDepth = getDepth(childAtom);
				if (currentDepth > maxDepth) {
					maxDepth = currentDepth;
				}
			}
		}

		return maxDepth + 1;
	}

	class AtomComparator implements Comparator<Atom> {

		@Override
		public int compare(Atom o1, Atom o2) {
			int cmp = getDepth(o1).compareTo(getDepth(o2));
			if (cmp == 0) {
				return o1.toString().compareTo(o2.toString());
			}

			return cmp;

		}

	}

	/**
	 * It returns a map where each atom is associated to all its ancestors.
	 * 
	 * @return
	 */
	public Map<Atom, List<Atom>> ancestorWalk() {
		Map<Atom, Atom> atom2Parent = new LinkedHashMap<>();
		Queue<Atom> queue = new LinkedList<>();
		Map<Atom, Condition> atom2Condition = new LinkedHashMap<>();
		for (Atom atom : atoms.keySet()) {
			atom2Parent.put(atom, null);
			atom2Condition.put(atom, this);
			queue.add(atom);
		}

		while (!queue.isEmpty()) {
			Atom currentAtom = queue.poll();
			Condition currentCondition = atom2Condition.get(currentAtom);
			for (Condition childCondition : currentCondition.getChildren(currentAtom)) {
				for (Atom childAtom : childCondition.atoms.keySet()) {
					queue.add(childAtom);
					atom2Parent.put(childAtom, currentAtom);
					atom2Condition.put(childAtom, childCondition);
				}
			}
		}

		// Until now we have a map node -> parent
		// Now build a map node -> [list of ancestors] (or null if it is an atom in the root)
		Map<Atom, List<Atom>> atom2Ancestors = new LinkedHashMap<>();
		for (Atom atom : atom2Parent.keySet()) {
			if (atom2Parent.get(atom) == null) {
				atom2Ancestors.put(atom, Collections.emptyList());
			} else {
				Atom parent = atom2Parent.get(atom);
				List<Atom> ancestors = new ArrayList<>();
				while (parent != null) {
					ancestors.add(parent);
					parent = atom2Parent.get(parent);
				}
				atom2Ancestors.put(atom, ancestors);
			}
		}

		return atom2Ancestors;
	}

	@Override
	public Condition clone() {
		// First clone the map
		Condition newCondition = new Condition();
		for (Atom atom : atoms.keySet()) {
			Atom atomClone = atom.clone();
			List<Condition> clonedConditions = new ArrayList<>();
			for (Condition c : atoms.get(atom)) {
				Condition conditionClone = c.clone();
				clonedConditions.add(conditionClone);
			}
			newCondition.atoms.put(atomClone, clonedConditions);
		}

		return newCondition;
	}
	
	public List<Condition> getChildren(Atom a) {
		return this.atoms.get(a);
	}

	public static void main(String[] args) {
		
		Map<String, Long> cachedTypesCardinalities = new HashMap<>();
		List<String> queryCacheLines = new ArrayList<>();
		String stri = new String("http://dbpedia.org/ontology/Band 34594");
		queryCacheLines.add(stri);
		stri = "http://dbpedia.org/ontology/Album 139078"; 
		queryCacheLines.add(stri);
		stri = "http://dbpedia.org/ontology/Language 8140"; 
		queryCacheLines.add(stri);
		stri = "http://dbpedia.org/ontology/TelevisionShow 39209";
		
		queryCacheLines.add(stri);
		for (String str : queryCacheLines) {
			String[] list = str.split(" ");
			cachedTypesCardinalities.put(list[0], Long.parseLong(list[1].trim()));
		}
    	
		System.out.println(cachedTypesCardinalities);
	
		
	}

}

package fr.inria.res;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.MutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import fr.inria.res.complexity.ComplexityCalculator;
import fr.inria.res.complexity.ComplexityFunction;
import fr.inria.res.complexity.KBBasedImprovedComplexityCalculator;
import fr.inria.res.complexity.KBDegreeBasedComplexityCalculator;
import fr.inria.res.complexity.KBDegreeBasedExactComplexityCalculator;
import fr.inria.res.complexity.KBEntropyBasedComplexityCalculator;
import fr.inria.res.complexity.ShortestLengthComplexityCalculator;
import fr.inria.res.data.HDTKB;
import fr.inria.res.data.InMemoryKB;
import fr.inria.res.data.KB;
import fr.inria.res.data.KBUtils;
import fr.inria.res.experimental.REMIRuntimeInfo;

@SuppressWarnings("deprecation")
public class REMiner {

	protected KB kb;
	protected Set<String> topEntities;
	private int nbProcessors;
	private boolean enableExtension;
	private boolean enableExtensionFinal;
	private boolean enableDebugging;
	private boolean exhaustiveSearch;
	private boolean experimentalSearch;
	private boolean deepBreadthSearch;
	private boolean scalabilityMode;
	private int maxDepth;
	private ComplexityFunction cFunction;

	// For smaller sets of conditions, do the complexity calculation in a sequential
	// fashion.
	private static int minThresholdForParallelComplexityCalc = 20;

	public REMiner(KB kb) {
		this.kb = kb;
		this.enableExtension = true;
		this.setEnableDebugging(false);
		this.exhaustiveSearch = false;
		this.scalabilityMode = false;
		this.nbProcessors = 1;
		this.cFunction = ComplexityFunction.DEGREE_BASED;
		this.maxDepth = 100;
		this.topEntities = new LinkedHashSet<>();
		/*
		 * these 2 parameter must be commented because when the test are launched there are
		 * no top entities or predicate types so it doesn't work
		 */
		// this.loadTopEntities();
	}

	public REMiner(KB kb, boolean debug) {
		this.kb = kb;
		this.enableExtension = true;
		this.setEnableDebugging(debug);
		this.exhaustiveSearch = false;
		this.nbProcessors = 1;
		this.cFunction = ComplexityFunction.DEGREE_BASED;
		this.topEntities = new LinkedHashSet<>();
		this.scalabilityMode = false;
		this.maxDepth = 100;		
		this.loadTopEntities();
	}

	public REMiner(KB kb, boolean debug, boolean enableExtension) {
		this.kb = kb;
		this.enableExtension = enableExtension;
		this.setEnableDebugging(debug);
		this.exhaustiveSearch = false;
		this.nbProcessors = 1;
		this.cFunction = ComplexityFunction.DEGREE_BASED;
		this.topEntities = new LinkedHashSet<>();
		this.scalabilityMode = false;
		this.maxDepth = 100;		
		this.loadTopEntities();
	}

	public REMiner(KB kb, boolean debug, boolean enableExtension, boolean enableExtensionFinal) {
		this.kb = kb;
		this.enableExtension = enableExtension;
		this.enableExtensionFinal = enableExtensionFinal;
		this.setEnableDebugging(debug);
		this.exhaustiveSearch = false;
		this.nbProcessors = 1;
		this.cFunction = ComplexityFunction.DEGREE_BASED;
		this.topEntities = new LinkedHashSet<>();
		this.scalabilityMode = false;
		this.maxDepth = 100;		
		this.loadTopEntities();
	}

	public REMiner(KB kb, ComplexityFunction cFunction) {
		this.kb = kb;
		this.enableExtension = true;
		this.setEnableDebugging(false);
		this.exhaustiveSearch = false;
		this.scalabilityMode = false;
		this.nbProcessors = 1;
		this.cFunction = cFunction;
		this.topEntities = new LinkedHashSet<>();
		this.maxDepth = 100;		
		this.loadTopEntities();
	}

	public REMiner(KB kb, boolean debug, boolean enableExtension, ComplexityFunction cFunction) {
		this.kb = kb;
		this.enableExtension = enableExtension;
		this.setEnableDebugging(debug);
		this.exhaustiveSearch = false;
		this.nbProcessors = 1;
		this.cFunction = cFunction;
		this.topEntities = new LinkedHashSet<>();
		this.maxDepth = 100;		
		this.scalabilityMode = false;
		this.loadTopEntities();
	}

	public REMiner(KB kb, boolean debug, boolean enableExtension, boolean enableExtensionFinal,
			boolean exhaustiveSearch, ComplexityFunction cFunction) {
		this.kb = kb;
		this.enableExtension = enableExtension;
		this.enableExtensionFinal = enableExtensionFinal;
		this.setEnableDebugging(debug);
		this.exhaustiveSearch = exhaustiveSearch;
		this.nbProcessors = 1;
		this.cFunction = cFunction;
		this.topEntities = new LinkedHashSet<>();
		this.maxDepth = 100;
		this.scalabilityMode = false;
		this.loadTopEntities();
	}

	public REMiner(KB kb, boolean debug, boolean enableExtension, boolean enableExtensionFinal,
			boolean exhaustiveSearch, ComplexityFunction cFunction, int nbProcessors, boolean deepBreadthSearch) {
		this.kb = kb;
		this.enableExtension = enableExtension;
		this.enableExtensionFinal = enableExtensionFinal;
		this.setEnableDebugging(debug);
		this.exhaustiveSearch = exhaustiveSearch;
		this.nbProcessors = nbProcessors;
		this.cFunction = cFunction;
		this.deepBreadthSearch = deepBreadthSearch;
		this.topEntities = new LinkedHashSet<>();
		this.maxDepth = 100;		
		this.scalabilityMode = false;
		this.loadTopEntities();
	}

	public REMiner(KB kb, Boolean debug, Boolean enableExtension, Boolean enableExtensionFinal,
			boolean exhaustive, ComplexityFunction cf, int nbProcessors, Boolean breadthSearch, boolean experimental) {
		this.kb = kb;
		this.enableExtension = enableExtension;
		this.enableExtensionFinal = enableExtensionFinal;
		this.setEnableDebugging(debug);
		this.exhaustiveSearch = exhaustive;
		this.nbProcessors = nbProcessors;
		this.cFunction = cf;
		this.deepBreadthSearch = breadthSearch;
		this.topEntities = new LinkedHashSet<>();
		this.loadTopEntities();
		this.experimentalSearch = experimental;
		this.maxDepth = 100;		
		this.scalabilityMode = false;
	}

	public KB getKB() {
		return this.kb;
	}

	public boolean isEnableDebugging() {
		return enableDebugging;
	}

	public void setEnableDebugging(boolean enableDebugging) {
		this.enableDebugging = enableDebugging;
	}
	
	public int getNumberOfProcessors() {
		return this.nbProcessors;
	}
	
	public void setNumberOfProcessors(int nProcessors) {
		this.nbProcessors = nProcessors;
	}

	public int getMaxDepth() {
		return maxDepth;
	}

	public void setMaxDepth(int maxDepth) {
		if (maxDepth < 3) {
			throw new IllegalArgumentException("maxDepth supports only values greater or equal than 3");
		}
		this.maxDepth = maxDepth;
	}

	public boolean isScalabilityMode() {
		return scalabilityMode;
	}

	public void setScalabilityMode(boolean scalabilityMode) {
		this.scalabilityMode = scalabilityMode;
	}

	/**
	 * Load the top entities of the database and modify the set topEntities by put
	 * in the top entities
	 */
	private void loadTopEntities() {
		System.err.println("Loading file " + GlobalConfig.getTopEntitiesPath());
		try (Stream<String> lines = Files.lines(Paths.get(GlobalConfig.getTopEntitiesPath()),
				Charset.forName("utf-8"))) {
			lines.forEachOrdered(line -> {
				topEntities.add(line);
			});
		} catch (Exception e) {
			System.err.println("The top entities could not be loaded: " + e);
		}
	}

	/**
	 * change the complexity Calculator of the kb
	 * 
	 * @param kb
	 * @param complexity
	 * @return
	 */
	public static ComplexityCalculator getComplexityCalculator(KB kb, ComplexityFunction complexity) {
		if (complexity == ComplexityFunction.SHORTEST) {
			return new ShortestLengthComplexityCalculator(kb);
		} else if (complexity == ComplexityFunction.STANDARD) {
			return new KBBasedImprovedComplexityCalculator(kb);
		} else if (complexity == ComplexityFunction.ENTROPY_BASED) {
			return new KBEntropyBasedComplexityCalculator(kb);
		} else if (complexity == ComplexityFunction.DEGREE_BASED) {
			File coefficientsFile = new File(GlobalConfig.getCoefficientsPath());
			if (coefficientsFile.exists()) {
				try {
					Map<String, Pair<Double, Double>> coefficients = 
							loadCoefficients(GlobalConfig.getCoefficientsPath());
					Pair<Double, Double> predicateCoeffs = 
							coefficients.get("predicates");
					Pair<Double, Double> constantCoeffs = 
							coefficients.get("constants");
					return new KBDegreeBasedComplexityCalculator(kb,
							predicateCoeffs.getLeft(), predicateCoeffs.getRight(),
							constantCoeffs.getLeft(), constantCoeffs.getRight(),
							coefficients);
				} catch (IOException e) {
					System.err.println(e);
					return new KBDegreeBasedComplexityCalculator(kb);
				}				
			} 
			
			return new KBDegreeBasedComplexityCalculator(kb);
		} else if (complexity == ComplexityFunction.DEGREE_BASED_EXACT) {
			long a = System.currentTimeMillis();
			System.out.println("Load the rankings");
			ComplexityCalculator cc =  new KBDegreeBasedExactComplexityCalculator(kb);	
			System.out.println("Loading the rankings takes " + (System.currentTimeMillis() - a) + " ms");
			return cc;
		}
		
		return new KBDegreeBasedComplexityCalculator(kb);

	}

	public static Map<String, Pair<Double, Double>> loadCoefficients(String iFileName) throws IOException {
		try (Stream<String> lines = Files.lines(Paths.get(iFileName),
				Charset.defaultCharset())) {
			Map<String, Pair<Double, Double>> coefficientsMap = new HashMap<>();
			
			lines.forEachOrdered(line -> {
				if (!line.startsWith("Predicate")) {
					String parts[] = line.split("\t");
					if (parts.length >= 4) {
						coefficientsMap.put(parts[0].replace(">", "").replace("<", ""), new MutablePair<>(
								Double.parseDouble(parts[1]), 
								Double.parseDouble(parts[2])));
					}
				}
			});
			return coefficientsMap;
		} catch (Exception e) {
			throw new IOException("The top entities could not be loaded: " + e.getMessage());
		}
	}

	/**
	 * Execute the query for the entity
	 * 
	 * @param entity
	 * @return List<ByteString[]>
	 */
	public static String[] makeQueryForEntity(String entity) {
		return new String[] { entity, "", "" };
	}

	/**
	 * Conversion of a map to a collection of Conditions
	 * 
	 * @param Map
	 *            of predicate and object results of the select
	 * @result Collection of Conditions
	 */
	private static List<Condition> convertMap2Conditions(Map<String, Set<String>> queryResults) {
		List<Condition> result = new ArrayList<>();
		for (String predicate : queryResults.keySet()) {
			for (String object : queryResults.get(predicate)) {
				result.add(Condition.makeCondition(predicate, object));
			}
		}

		return result;
	}
	
	private static List<Atom> convertMap2Atoms(Map<String, Set<String>> queryResults) {
		List<Atom> result = new ArrayList<>();
		for (String predicate : queryResults.keySet()) {
			for (String object : queryResults.get(predicate)) {
				result.add(new Atom("", predicate, object, ""));
			}
		}

		return result;
	}

	/**
	 * Return all the Conditions for a single entity
	 * 
	 * @param entity
	 * @return
	 */
	Collection<Condition> getConditionsForEntity(String entity) {
		String[] query = makeQueryForEntity(entity);
		return convertMap2Conditions(kb.selectDistinctPO(query));
	}
	
	Collection<Atom> getAtomsForEntity(String entity) {
		String[] query = makeQueryForEntity(entity);
		return convertMap2Atoms(kb.selectDistinctPO(query));
	}

	/**
	 * results of the intersection between the database and the target entities
	 * returns all the condition for the target entities
	 * 
	 * @param entities2describe
	 * @return Set<Condition>
	 * @see makeQueryForEntity
	 * @see convertMap2Conditions
	 */
	public Set<Condition> intersectEntities(List<String> entities2Describe) {
		Set<String> targetEntities = new HashSet<String>(entities2Describe);
		Set<Condition> intersection = new LinkedHashSet<Condition>();
		/*
		 * Initialize the intersection list with the conditions of the first entity
		 */
		intersection.addAll(getConditionsForEntity(entities2Describe.get(0)));
		if (this.isEnableDebugging()) {
			if (this.enableExtension || this.enableExtensionFinal) {
				long a = System.currentTimeMillis();
				intersection = finalExtendListOfConditions(intersection, targetEntities);
				for (int i = 1; i < entities2Describe.size(); ++i) {
					intersection.retainAll(finalExtendListOfConditions(
							transformCollections2Set(getConditionsForEntity(entities2Describe.get(i))),
							targetEntities));
				}
				long b = System.currentTimeMillis();
				System.out.print("temps pour faire l'extension du premier element ");
				System.out.print(b - a);
				System.out.println(" en millisecondes");
			} else {
				long a = System.currentTimeMillis();
				for (int i = 1; i < entities2Describe.size(); ++i) {
					intersection.retainAll(transformCollections2Set(getConditionsForEntity(entities2Describe.get(i))));					
				}
				
				removeBlankNodes(intersection);
				
				
				
				long b = System.currentTimeMillis();
				System.out.print("temps pour faire l'extension du premier element ");
				System.out.print(b - a);
				System.out.println(" en millisecondes");

			}
			System.out.println("nombre de conditions apres l'extension :" + intersection.size());
			return intersection;
		} else if (!this.enableExtension && !this.enableExtensionFinal) {
			for (int i = 1; i < entities2Describe.size(); ++i) {
				intersection.retainAll(getConditionsForEntity(entities2Describe.get(i)));
			}
			removeBlankNodes(intersection);
			return intersection;
		} else {
			intersection = finalExtendListOfConditions(intersection, targetEntities);
			for (int i = 1; i < entities2Describe.size(); ++i) {
				intersection.retainAll(finalExtendListOfConditions(
						transformCollections2Set(getConditionsForEntity(entities2Describe.get(i))), targetEntities));
			}
			return intersection;
		}
	}

	private void removeBlankNodes(Set<Condition> intersection) {
		// Removal of conditions with blank nodes
		List<Condition> conditionsToRemove = new ArrayList<>();
		for (Condition c : intersection) {
			if (KBUtils.isBlankNode(c.getLastObject())) {
				conditionsToRemove.add(c);
			}
		}
		intersection.removeAll(conditionsToRemove);
	}

	/**
	 * Execute the selection on the knowledgeBases for the entities give and the
	 * conditions recover
	 * 
	 * @param kb
	 * @param setCond
	 * @param entities2Describe
	 * @result Map<ByteString, IntHashMap<ByteString>>
	 * 
	 */
	public Map<String, Set<String>> resultOfKb(KB kb, Set<Condition> setCond, List<String> entities2Describe) {
		Iterator<Condition> it = setCond.iterator();
		Map<String, Set<String>> map = new LinkedHashMap<String, Set<String>>();
		int i = 0;
		while (i < entities2Describe.size() && it.hasNext()) {
			String[] e = new String[3];
			e[0] = entities2Describe.get(0);
			Condition cond = it.next();
			e[1] = cond.getLastPredicate();
			e[2] = cond.getLastObject();
			map.putAll(kb.selectDistinctPO(e));
			i++;
		}
		return map;
	}

	/**
	 * Transform a List of Condition to a ConcurrentLinkedQueue of Condition
	 * 
	 * @param List<Condition>
	 *            originalList
	 * @return Queue<Condition>
	 */
	public Queue<Condition> transformListToQueue(List<Condition> originalList) {
		Queue<Condition> queue = new LinkedList<>();
		for (int i = 0; i < originalList.size(); i++) {
			queue.add(originalList.get(i));
		}
		return queue;
	}

	/**
	 * 
	 * @param cond
	 * @return
	 */
	public List<Condition> refine(Condition cond) {
		String lastObject = cond.getLastObject();

		Map<String, Set<String>> resultOf = resultOfKb(kb, new LinkedHashSet<>(getConditionsForEntity(lastObject)),
				Arrays.asList(lastObject));

		return convertMap2Conditions(resultOf);
	}

	/**
	 * Method create a path with the list of Atom gives in parameter as sons and the
	 * referenceAtom as root
	 * 
	 * @param listAtom
	 * @param referenceAtom
	 * @param referenceCond
	 * @param targetEntities
	 * @return
	 */
	public List<Condition> method3a(List<Atom> listAtom, Atom referenceAtom, Condition referenceCond,
			Set<String> targetEntities) {
		List<Condition> listCond2Return = new ArrayList<>();
		
		for (int i = 0; i < listAtom.size() - 1; i++) {
			String predicateI = listAtom.get(i).getPredicat();
			String objectI = listAtom.get(i).getObject();
			if (predicateI.endsWith("-inv") || targetEntities.contains(objectI))
				continue;
			
			if (objectI.startsWith("https://"))
				continue;

			List<Atom> listAtom2Add = new ArrayList<>();
			listAtom2Add.add(listAtom.get(i));
			for (int j = i + 1; j < listAtom.size(); j++) {
				String predicateJ = listAtom.get(j).getPredicat();
				String objectJ = listAtom.get(j).getObject();

				if (predicateJ.endsWith("-inv") || targetEntities.contains(objectJ))
					continue;
				
				if (objectJ.startsWith("https://"))
					continue;

				listAtom2Add.add(listAtom.get(j));
				Condition cond2Add = referenceCond.addAtoms(listAtom2Add, referenceAtom);
				listCond2Return.add(cond2Add);
				listAtom2Add.remove(1);
			}
		}

		return listCond2Return;
	}

	/**
	 * Method create a star with the list of Atom gives in parameter as sons and the
	 * referenceAtom as root
	 * 
	 * @param listAtom
	 * @param referenceAtom
	 * @param referenceCond
	 * @param targetEntities
	 * @return
	 */
	public List<Condition> method3b(List<Atom> listAtom, Atom referenceAtom, Condition referenceCond,
			Set<String> targetEntities) {
		List<Condition> listCond2Return = new ArrayList<>();
		for (int i = 0; i < listAtom.size() - 1; i++) {
			String predicateI = listAtom.get(i).getPredicat();
			String objectI = listAtom.get(i).getObject();

			if (predicateI.endsWith("-inv") || targetEntities.contains(objectI))
				continue;
			
			if (objectI.startsWith("https://"))
				continue;
			
			
			if (KBUtils.isBlankNode(objectI))
				continue;

			for (int j = i + 1; j < listAtom.size(); j++) {
				Condition cond2Add = new Condition();
				String predicateJ = listAtom.get(j).getPredicat();
				String objectJ = listAtom.get(j).getObject();

				if (objectJ.startsWith("https://"))
					continue;
				
				if (!topEntities.contains(objectJ))
					continue;
				
				if (KBUtils.isBlankNode(objectJ))
					continue;
				
				if (predicateJ.endsWith("-inv") || targetEntities.contains(objectJ))
					continue;
								
				
				cond2Add = referenceCond.addAtom(listAtom.get(i), listAtom.get(j), referenceAtom);
				listCond2Return.add(cond2Add);
			}
		}

		return listCond2Return;
	}

	/**
	 * Method creates a son of the son with addAtom as the first son and addAtom1 as
	 * the son of addAtom
	 * 
	 * @param addAtom
	 * @param addAtom1
	 * @param referenceAtom
	 * @param referenceCond
	 * @return
	 */
	public Condition method3c(Atom addAtom, Atom addAtom1, Atom referenceAtom, Condition referenceCond) {
		Condition cond2Return = referenceCond.chainAtomSO(addAtom, referenceAtom);
		Atom atom1Modified = new Atom("", addAtom.getPredicat(), addAtom1.getSubject(), null);
		cond2Return = cond2Return.chainAtomSO(addAtom1, atom1Modified);
		return cond2Return;
	}

	protected Set<Condition> method3cAux(List<Condition> conditionsOfSize2, Atom referenceAtom, Condition referenceCondition) {
		Set<Condition> extendedConditions = new LinkedHashSet<>();
		for (int i = 0; i < conditionsOfSize2.size(); i++) {
			Condition conditionOfSize2 = conditionsOfSize2.get(i);
			if (conditionOfSize2.getLastObject() == "") {
				Set<Atom> atomsInConditionSize2 = conditionOfSize2.atoms().keySet();
				int j = 0;
				for (Atom potentialReferenceAtom : atomsInConditionSize2) {
					List<Condition> listChildren = conditionOfSize2.getChildren(potentialReferenceAtom);
					
					if (listChildren.isEmpty())
						continue;
						
					Atom atomAdd2 = new Atom(potentialReferenceAtom.getObjectHidden(), listChildren.get(j).getLastPredicate(),
							listChildren.get(j).getLastObject(), null);
					Atom atomAdd = new Atom(referenceAtom.getObject(), potentialReferenceAtom.getPredicat(), potentialReferenceAtom.getObjectHidden(), null);
					
					if (!KBUtils.areInversePairs(atomAdd2.getPredicat(), atomAdd.getPredicat())
							&& !KBUtils.areInversePairs(atomAdd2.getPredicat(), referenceAtom.getPredicat())
							&& !KBUtils.areInversePairs(atomAdd.getPredicat(), referenceAtom.getPredicat())) {
						extendedConditions.add(method3c(atomAdd, atomAdd2, referenceAtom, referenceCondition));
					}

				}

			}
		}
		return extendedConditions;
	}


	/**
	 * Transform a List of ByteString to a Set of ByteString
	 * 
	 * @param List<ByteString>
	 *            entities2Describe
	 * @return Set<ByteString>
	 */
	protected Set<String> transformListToSet(List<String> entities2Describe) {
		Set<String> setEntities = new LinkedHashSet<>();
		setEntities.addAll(entities2Describe);
		return setEntities;
	}

	/**
	 * Return the predicate classified by the PriorityQueue
	 * 
	 * @param Set<Condition>
	 *            setCond
	 * @result PriorityQueue<Condition>
	 */
	public PriorityQueue<Condition> sortConditions(Set<Condition> setCond) {
		Comparator<Condition> comparator = (Comparator<Condition>) REMiner.getComplexityCalculator(kb, cFunction);
		PriorityQueue<Condition> predicateClassified = new PriorityQueue<>(setCond.size() + 1, comparator);
		predicateClassified.addAll(setCond);
		return predicateClassified;
	}

	/**
	 * 
	 * @param setCond
	 * @return
	 * @throws InterruptedException 
	 */
	public Condition[] sortConditionsInParallel(Set<Condition> setCond) throws InterruptedException {
		Condition[] conditionsArr = new Condition[setCond.size()];
		int k = 0;
		ComplexityCalculator calculator = REMiner.getComplexityCalculator(kb, cFunction);

		for (Condition c : setCond) {
			conditionsArr[k] = c;
			++k;
		}

		if (setCond.size() >= minThresholdForParallelComplexityCalc
				&& Runtime.getRuntime().availableProcessors() > 1) {
			double[] maxMin = calculateComplexitiesInParallel(conditionsArr, calculator);
			bucketSort(conditionsArr, maxMin[1], maxMin[0], (Comparator<Condition>) calculator);
		} else {
			Arrays.parallelSort(conditionsArr, ((Comparator<Condition>) calculator).reversed());
		}

		return conditionsArr;
	}

	private void bucketSort(Condition[] conditionsArr, double min, double max, Comparator comparator) {
		int nThreads = this.nbProcessors;
		Thread[] threads = new Thread[nThreads];
		double range = max - min;
		double delta = range / nThreads;
		double[] splits = new double[nThreads];
		ArrayList<ArrayList<Condition>> buckets = new ArrayList<ArrayList<Condition>>();
		for (int i = 0; i < nThreads; ++i) {
			splits[i] = Math.min(min + delta * (i + 1), max);
			buckets.add(new ArrayList<>());
		}

		for (Condition c : conditionsArr) {
			for (int i = 0; i < nThreads; ++i) {
				double complexity = c.getComplexity();
				if (complexity <= splits[i]) {
					buckets.get(i).add(c);
					break;
				}
			}
		}

		for (int i = 0; i < nThreads; ++i) {
			final int m = i;
			threads[i] = new Thread(new Runnable() {

				@SuppressWarnings("unchecked")
				@Override
				public void run() {
					Collections.sort(buckets.get(m), comparator.reversed());
				}
			});

			threads[i].start();
		}

		for (int i = 0; i < nThreads; ++i) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		int k = 0;
		for (int i = nThreads - 1; i >= 0; --i) {
			List<Condition> bucket = buckets.get(i);
			for (int j = 0; j < bucket.size(); ++j) {
				conditionsArr[k] = bucket.get(j);
				++k;
			}
		}

	}

	/**
	 * Computes the complexity score of the conditions of the input array.
	 * 
	 * @param conditionsArr
	 * @param calculator
	 * @return An array of 2 elements with the lowest and highest complexity values.
	 * @throws InterruptedException 
	 */
	private double[] calculateComplexitiesInParallel(Condition[] conditionsArr, ComplexityCalculator calculator) 
			throws InterruptedException {
		int nThreads = Runtime.getRuntime().availableProcessors();
		double[] maxC = new double[nThreads];
		double[] minC = new double[nThreads];
		for (int i = 0; i < maxC.length; i += 2) {
			maxC[i] = Double.MIN_VALUE;
			minC[i + 1] = Double.MAX_VALUE;
		}

		Thread[] threads = new Thread[nThreads];
		int chunkSize = (int) Math.ceil(conditionsArr.length / nThreads);
		for (int i = 0; i < nThreads; ++i) {
			final int m = i;
			threads[i] = new Thread(new Runnable() {

				@Override
				public void run() {
					final int min = m * chunkSize;
					final int max = (m == nThreads - 1) ? conditionsArr.length : (int) (m + 1) * chunkSize;
					for (int j = min; j < max; ++j) {
						double complexity = calculator.getComplexity(conditionsArr[j]);
						if (complexity > maxC[m]) {
							maxC[m] = complexity;
						}

						if (complexity < minC[m]) {
							minC[m] = complexity;
						}

					}
				}
			});

			threads[i].start();
		}

		for (Thread t : threads) {
			t.join();
		}

		Arrays.sort(maxC);
		Arrays.sort(minC);
		return new double[] { maxC[maxC.length - 1], minC[0] };
	}

	/**
	 * Execute the selectDistict for the Condition and return a Set of the entities
	 * 
	 * @param Condition
	 *            cond
	 * @return Set of ByteString
	 */
	public Set<String> getEntitiesForSimpleCondition(KB theKB, Condition cond) {
		return theKB.selectDistinct(cond);

	}

	/**
	 * Execute the expression of the last condition from the stack and modify the
	 * stack if the last condition is redundancy and modify entitiesCoveredUntilNow
	 * to obtain the entities covered by all the conditions from the stack
	 * 
	 * @param Stack
	 *            of the condition which have to be done
	 * @param List
	 *            of the entities target
	 * @result a Set of the Condition
	 * @see resultOfKbExpression
	 */
	List<String> getCoveredEntities(KB theKB, Stack<Condition> stack) {
		Set<String> entitiesCoveredUntilNow = new LinkedHashSet<>(
				getEntitiesForSimpleCondition(theKB, stack.get(stack.size() - 1)));
		for (int i = stack.size() - 2; i >= 0; --i) {
			entitiesCoveredUntilNow.retainAll(getEntitiesForSimpleCondition(theKB, stack.get(i)));
		}
		return new ArrayList<>(entitiesCoveredUntilNow);
	}

	/**
	 * Compare 2 list of entities (ByteString), Equality if they are equals, Subset
	 * if coveredObject is a subset of entities2Describe (this case could never
	 * happen and superset if coveredObject is a superset of entities2Describe
	 * 
	 * @param coveredObject
	 *            Set of Conditions to be compared
	 * @param intersection
	 *            between the list of target entities and the knowledgeBases
	 * @return a number for the difference between the 2 Set of Condition
	 * @see compareTo
	 */
	public SetRelationCase compareCollectionEntities(Collection<String> coveredObject,
			Collection<String> entities2Describe) {
		if (!coveredObject.containsAll(entities2Describe)) {
			return SetRelationCase.SUBSET;
		}
		if (entities2Describe.size() < coveredObject.size()) {
			return SetRelationCase.SUPERSET;
		}
		/**
		 * if (entities2Describe.size() < coveredObject.size()) { return
		 * SetRelationCase.SUPERSET; }
		 **/

		return SetRelationCase.EQUALITY;
	}

	/**
	 * coveredEntities is a superset of entities2Describe so if entities2Describe
	 * contains all, they are equals
	 * 
	 * @param coveredEntities
	 * @param setEntities2Describe
	 * @return
	 */
	SetRelationCase compareCollectionEntitiesExhaustively(List<String> coveredEntities,
			Set<String> setEntities2Describe) {

		if (setEntities2Describe.containsAll(coveredEntities)) {
			return SetRelationCase.EQUALITY;
		}

		return SetRelationCase.SUBSET;
	}

	/**
	 * 
	 * @param setCond
	 * @return
	 */
	public Set<Condition> transformCollections2Set(Collection<Condition> collection) {
		Set<Condition> setCond = new LinkedHashSet<>();
		setCond.addAll(collection);
		return setCond;
	}

	/**
	 * 
	 * @param listCond
	 * @param referenceCond
	 * @return
	 */
	public List<Atom> transformListCondition2ListAtom(List<Condition> listCond, Condition referenceCond) {
		List<Atom> listAtom = new ArrayList<>();
		for (Condition cond : listCond) {
			Atom atom = new Atom(referenceCond.getLastObject(), cond.getLastPredicate(), cond.getLastObject(), null);
			listAtom.add(atom);
		}
		return listAtom;
	}

	/**
	 * transform a stack of Conditions to a list of Conditions
	 * 
	 * @param stack
	 * @return
	 */
	private List<Condition> transformStack2List(Stack<Condition> stack) {
		List<Condition> listCond = new ArrayList<>();
		for (int i = 0; i < stack.size(); i++) {
			listCond.add(stack.elementAt(i));
		}
		return listCond;
	}

	/**
	 * Execute the method2, method3a, method3b, method3c for the set of Condition
	 * gives in parameter and returns all the sons of each Condition
	 * 
	 * @param commonConditions
	 * @return
	 */
	public Set<Condition> finalExtendListOfConditions(Set<Condition> commonConditions, Set<String> targetEntities) {
		Set<Condition> extendedConditions = new LinkedHashSet<>();
		
		Map<String, List<Condition>> object2ConditionIndex = indexConditionsByConstant(commonConditions);
		Set<Pair<String, String>> seenClosingPairs = new LinkedHashSet<>();
		Set<Triple<String, String, String>> seenClosingTriples = new LinkedHashSet<>();


		for (Condition cond : commonConditions) {
			if (cond.getLastPredicate().startsWith("http://www.w3.org/2002/07/owl#differentFrom"))
				continue;
			
			
			if (cond.getLastObject().startsWith("https://"))
				continue;
			
			if (KBUtils.isBlankNode(cond.getLastObject()))
				continue;
			
			extendedConditions.add(cond);

			Atom atom = cond.getSetAtoms().iterator().next();
			// Do not specialize conditions of the form type(x, Country)
			if (atom.getPredicat().equals(GlobalConfig.typeRelation))
				continue;
			
			if (topEntities.contains(atom.getObject()))
				continue;

			// Do not specialize inverse conditions
			if (atom.getPredicat().endsWith("-inv"))
				continue;

			// Getting the extensions of size two r(x, y) ^ r'(y, C)
			List<Condition> listCond = (List<Condition>) getConditionsForEntity(atom.getObject());
			List<Atom> listNewAtom = transformListCondition2ListAtom(listCond, cond);
			List<Condition> conditionsOfSize2 = method2a(listNewAtom, atom, cond, targetEntities);
			extendedConditions.addAll(conditionsOfSize2);
			
			// Getting the closed extensions of size 2, r(x, y) ^ r'(x, y)
			List<Condition> conditionsWithSameObject = object2ConditionIndex.get(atom.getObject());
			List<Condition> closedConditionsOfSize2 = method2b(conditionsWithSameObject, atom, cond, seenClosingPairs);
			extendedConditions.addAll(closedConditionsOfSize2);

			//System.out.println("2a: " + extendedConditions);
			if (this.enableExtensionFinal) {
				// Now getting the extensions of size 3
				if (this.scalabilityMode) {
					extendedConditions.addAll(method3a(listNewAtom, atom, cond, targetEntities));
				}
				// System.out.println("3a: " + extendedConditions.size());
				List<Condition> list3b = method3b(listNewAtom, atom, cond, targetEntities);
				extendedConditions.addAll(list3b);
				//System.out.println(cond + " 3b: " + list3b);
				// We have decided to remove extension 3c
				if (this.scalabilityMode) {
					List<Condition> listCond3C = extendListOfConditions(new HashSet<>(listCond), targetEntities);
					extendedConditions.addAll(this.method3cAux(listCond3C, atom, cond));
				}
				method2bP2a(closedConditionsOfSize2, atom, targetEntities, extendedConditions);				
				method3d(closedConditionsOfSize2, conditionsWithSameObject, seenClosingTriples, extendedConditions);
			}

		}

		return extendedConditions;
	}
	
	
	private List<Condition> extendListOfConditions(Set<Condition> commonConditions, Set<String> targetEntities) {
		Set<Condition> extendedConditions = new LinkedHashSet<>();
		
		Map<String, List<Condition>> object2ConditionIndex = indexConditionsByConstant(commonConditions);
		Set<Pair<String, String>> seenClosingPairs = new LinkedHashSet<>();

		for (Condition cond : commonConditions) {
			if (cond.getLastPredicate().startsWith("http://www.w3.org/2002/07/owl#differentFrom"))
				continue;
			
			
			if (cond.getLastObject().startsWith("https://"))
				continue;
			
			if (KBUtils.isBlankNode(cond.getLastObject()))
				continue;
			
			extendedConditions.add(cond);

			Atom atom = cond.getSetAtoms().iterator().next();
			// Do not specialize conditions of the form type(x, Country)
			if (atom.getPredicat().equals(GlobalConfig.typeRelation))
				continue;
			
			if (topEntities.contains(atom.getObject()))
				continue;

			// Do not specialize inverse conditions
			if (atom.getPredicat().endsWith("-inv"))
				continue;

			// Getting the extensions of size two r(x, y) ^ r'(y, C)
			List<Condition> listCond = (List<Condition>) getConditionsForEntity(atom.getObject());
			List<Atom> listNewAtom = transformListCondition2ListAtom(listCond, cond);
			List<Condition> conditionsOfSize2 = method2a(listNewAtom, atom, cond, targetEntities);
			extendedConditions.addAll(conditionsOfSize2);
			
			// Getting the closed extensions of size 2, r(x, y) ^ r'(x, y)
			List<Condition> conditionsWithSameObject = object2ConditionIndex.get(atom.getObject());
			List<Condition> closedConditionsOfSize2 = method2b(conditionsWithSameObject, atom, cond, seenClosingPairs);
			extendedConditions.addAll(closedConditionsOfSize2);
		}

		return new ArrayList<>(extendedConditions);
	}
	
	
	/**
	 * Change the list of condition to get the 3d extension
	 * ie : livesIn(?x, ?y) biggestCity(?y, ?z) capital(?y, ?z)
	 * @param closedConditionsOfSize2
	 * @param conditionsWithSameObject
	 * @param seenClosingTriples
	 * @param extendedConditions
	 */
	private void method3d(List<Condition> closedConditionsOfSize2,
			List<Condition> conditionsWithSameObject, Set<Triple<String, String, String>> seenClosingTriples,
			Set<Condition> extendedConditions) {
		
		for (Condition condition : closedConditionsOfSize2) {
			Set<Atom> atomsCondition = condition.getSetAtoms();			
			Atom referenceAtom = atomsCondition.iterator().next();
			Set<String> predicatesInCondition = condition.getPredicatesInAtoms();
			for (Condition closingCondition : conditionsWithSameObject) {	
				String predicateInClosingCondition = closingCondition.getLastPredicate();
				if (predicatesInCondition.contains(predicateInClosingCondition))
					continue;

				boolean skipClosingCondition = false; 
				for (String predicateInCondition : predicatesInCondition) {
					if (KBUtils.areInversePairs(predicateInCondition, predicateInClosingCondition)) {
						skipClosingCondition = true;
					}
						
				}
				
				predicatesInCondition.add(predicateInClosingCondition);
				List<String> allPredicates = new ArrayList<>(predicatesInCondition);
				allPredicates.add(predicateInClosingCondition);
				Collections.sort(allPredicates);
				Triple<String, String, String> seenTriple = new MutableTriple<>(allPredicates.get(0), 
						allPredicates.get(1), allPredicates.get(2));
				
				if (seenClosingTriples.contains(seenTriple)) 
					continue;
				
				if (skipClosingCondition)
					continue;
				
				Condition closedConditionOfSize3 = condition.chainAtomSSOO(
						new Atom(referenceAtom.getSubject(), predicateInClosingCondition, "", 
								referenceAtom.getObjectHidden()), referenceAtom);
				extendedConditions.add(closedConditionOfSize3);
				seenClosingTriples.add(seenTriple);
			}				
		}
	}

	private void method2bP2a(List<Condition> closedConditionsOfSize2,
			Atom referenceAtom, Set<String> targetEntities, Set<Condition> output) {
		for (Condition closedCondition : closedConditionsOfSize2) {			
			
			Set<Atom> otherAtoms = closedCondition.getSetAtoms();
			Iterator<Atom> it = otherAtoms.iterator();
			Atom atom = it.next();
			Atom newReferenceAtom = null;
			if (atom.getPredicat().equals(referenceAtom.getPredicat())) {
				it.remove();
				newReferenceAtom = otherAtoms.iterator().next();
			} else {
				newReferenceAtom = atom;
			}
			
			List<Atom> candidateAtoms = (List<Atom>) getAtomsForEntity(newReferenceAtom.getObjectHidden());
			for (Atom candidateAtom : candidateAtoms) {		
				if (KBUtils.areInversePairs(referenceAtom.getPredicat(), candidateAtom.getPredicat())
						|| KBUtils.areInversePairs(newReferenceAtom.getPredicat(), candidateAtom.getPredicat())
						|| KBUtils.areInversePairs(newReferenceAtom.getPredicat(), referenceAtom.getPredicat()) ) {
					continue;
				}
				
				Condition result = method2a(candidateAtom, newReferenceAtom, closedCondition, targetEntities);
				if (result != null) {
					output.add(result);
				}
			}
		}
		
	}

	public List<Condition> method2a(List<Atom> candidateAtoms, Atom referenceAtom, 
			Condition referenceCondition, Set<String> targetEntities) {
		List<Condition> extendedConditions = new ArrayList<>();
		for (int i = 0; i < candidateAtoms.size(); i++) {
			Condition result = method2a(candidateAtoms.get(i), referenceAtom,
					referenceCondition, targetEntities);
			if (result != null) {
				extendedConditions.add(result);
			}				
		}
		
		return extendedConditions;
	}
	
	/**
	 * first extension of an atom so 2 atoms become one condition of size 2
	 * @param candidateAtom
	 * @param referenceAtom
	 * @param referenceCondition
	 * @param targetEntities
	 * @return
	 */
	public Condition method2a(Atom candidateAtom, Atom referenceAtom, 
			Condition referenceCondition, Set<String> targetEntities) {
		String extendedConditionObject = candidateAtom.getObject();
		String extendedConditionPredicate = candidateAtom.getPredicat();
		
		if (extendedConditionPredicate.startsWith("http://www.w3.org/2002/07/owl#differentFrom"))
			return null;
		
		if (extendedConditionObject.startsWith("https://"))
			return null;
		
		if (KBUtils.isBlankNode(extendedConditionObject))
			return null;
		
		if (extendedConditionPredicate.equals(GlobalConfig.typeRelation))
			return null;
		
		// Forbid the target entities in the description
		if (targetEntities.contains(extendedConditionObject))
			return null;
		
		// Added after eswc 2019 submission to reduce the number of subgraph expressions
		// =======================================================
		//if (!topEntities.contains(extendedConditionPredicate)) {
		//	return null;
		//}		
		// ========================================================

		if (!KBUtils.areInversePairs(extendedConditionPredicate, referenceAtom.getPredicat())) {
			return referenceCondition.chainAtomSO(candidateAtom, referenceAtom);
		}
		
		return null;
	}
	
	/**
	 * Change the list of condition to get the 2b extension
	 * ie : biggestCity(?x, ?y) capital(?x, ?y)
	 * @param closedConditionsOfSize2
	 * @param referenceAtom
	 * @param targetEntities
	 * @param output
	 */
	public List<Condition> method2b(List<Condition> conditionsWithSameObject, 
			Atom referenceAtom, Condition referenceCondition, Set<Pair<String, String>> seenClosingPairs) {
		List<Condition> extendedConditions = new ArrayList<>();
		if (conditionsWithSameObject != null) {
			for (Condition conditionWithSameObject : conditionsWithSameObject) {
				// Make sure the predicates are not the same
				if (conditionWithSameObject.getLastPredicate().equals(referenceAtom.getPredicat()))
					continue;
				
				Pair<String, String> predicatePair = null;
				if (conditionWithSameObject.getLastPredicate().compareTo(referenceAtom.getPredicat()) > 0) {
					predicatePair = new MutablePair<>(conditionWithSameObject.getLastPredicate(), referenceAtom.getPredicat());
				} else {
					predicatePair = new MutablePair<>(referenceAtom.getPredicat(), conditionWithSameObject.getLastPredicate());						
				}
				
				if (KBUtils.areInversePairs(predicatePair.getKey(), predicatePair.getValue()))
					continue;
				
				if (seenClosingPairs.contains(predicatePair))
					continue;
				
				Atom closingAtom = new Atom(referenceAtom.getSubject(), 
						conditionWithSameObject.getLastPredicate(),"", referenceAtom.getObject());			
				Condition closedCondition = referenceCondition.chainAtomSSOO(closingAtom, referenceAtom);
				extendedConditions.add(closedCondition);
				seenClosingPairs.add(predicatePair);
			}
		}
		
		return extendedConditions;
	}
	

	private Map<String, List<Condition>> indexConditionsByConstant(Set<Condition> conditionsOfSize1) {
		Map<String, List<Condition>> index = new HashMap<>();
		for (Condition c : conditionsOfSize1) {
			String obj = c.getLastObject();
			List<Condition> conds = index.get(obj);			
			if (conds == null) {
				conds = new ArrayList<>();
				index.put(obj, conds);
			}
			
			conds.add(c);
		}
		
		return index;
	}

	/**
	 * Test if the first element of the final list of Condition is redundancy Test
	 * if the last element of the final list of Condition is sufficient to describe
	 * the entities Test if a Condition add precision to the final list of Condition
	 * or not
	 * 
	 * @param conditions
	 * @param entities2Describe
	 * @return
	 */
	Stack<Condition> testRedundance(KB theKB, Stack<Condition> conditions, List<String> entities2Describe) {
		if (conditions.size() > 1) {
			if (theKB.selectDistinct(conditions.get(conditions.size() - 1))
					.equals(transformListToSet(entities2Describe))) {
				Stack<Condition> listReturn = new Stack<>();
				listReturn.add(conditions.get(conditions.size() - 1));
				return listReturn;
			}

			Set<String> entitiesForOtherCondition = new LinkedHashSet<>(theKB.selectDistinct(conditions.get(1)));
			for (int i = 2; i < conditions.size(); i++) {
				entitiesForOtherCondition.retainAll(theKB.selectDistinct(conditions.get(i)));
			}
			if (entitiesForOtherCondition.equals(transformListToSet(entities2Describe))) {
				conditions.remove(0);
			}
			Set<String> listLastCondition = theKB.selectDistinct(conditions.get(conditions.size() - 1));
			for (int i = conditions.size() - 1; i > 0; i--) {
				Set<String> listToTest = new LinkedHashSet<>(listLastCondition);
				for (int j = 0; j < conditions.size() - 1; j++) {
					if (j != i - 1) {
						listToTest.retainAll(theKB.selectDistinct(conditions.get(j)));
					}
				}
				if (listToTest.equals(transformListToSet(entities2Describe))) {
					conditions.remove(i - 1);
				}
			}
		}
		return conditions;
	}

	public String cleanForOutput2(List<Condition> listCond) {
		StringBuilder strBuilder = new StringBuilder();

		for (Condition c : listCond) {
			strBuilder.append(kb.cleanString(c.toString()) + " ");
		}

		return strBuilder.toString();
	}

	/**
	 * 
	 * @param collectionCondition
	 * @return
	 */
	public static double getComplexity(Collection<Condition> collectionCondition) {
		double complexity = 0;
		for (Condition cond : collectionCondition) {
			complexity += cond.getComplexity();
		}
		return complexity;
	}

	/**
	 * Deep research algorithm
	 * 
	 * @param PriorityQueue of Condition
	 * @param List of the entities we have to describe
	 * @result
	 * @see transformPriorityQueue
	 * @see convertMap2Condition
	 * @see makeExpression
	 */
	protected List<Condition> mine(List<String> entities2Describe, Set<Condition> setCond) {
		List<String> coveredObject = new ArrayList<>();
		long a = System.currentTimeMillis();
		PriorityQueue<Condition> priorityQueueElement = sortConditions(setCond);
		if (this.isEnableDebugging()) {
			long b = System.currentTimeMillis();
			System.out.print("Durée pour classer les éléments dans la queue de priorité : ");
			System.out.println(b - a);
			System.out.println(" en millisecondes");
		}
		a = System.currentTimeMillis();
		Stack<Condition> stack = new Stack<Condition>();
		Set<String> setEntities2Describe = new LinkedHashSet<String>(entities2Describe);
		while (!priorityQueueElement.isEmpty()
				&& compareCollectionEntities(coveredObject, setEntities2Describe) != SetRelationCase.EQUALITY) {
			stack.push(priorityQueueElement.poll());
			getCoveredEntities(this.kb, stack);
			SetRelationCase relationCase = compareCollectionEntities(setEntities2Describe, coveredObject);
			if (relationCase == SetRelationCase.SUPERSET || relationCase == SetRelationCase.NOCONTAINMENT) {
				stack.pop();
			} else if (relationCase == SetRelationCase.EQUALITY) {
				if (this.isEnableDebugging()) {
					long b = System.currentTimeMillis();
					System.out.print("Durée pour chercher dans la queue de priorité: ");
					System.out.println(b - a);
					System.out.println(" en millisecondes");
					System.out.println("Complexity of the solution : "
							+ getComplexity(testRedundance(this.kb, stack, entities2Describe)));
				}
				// Bingo
				return testRedundance(this.kb, stack, entities2Describe);
			}
		}
		return Collections.emptyList();
	}

	/**
	 * execute the deep research algorithm and doesn't stop if it finds a solution
	 * but continue to search if there is a best solution
	 * 
	 * @param entities2Describe
	 * @param setCond
	 * @param listSolution
	 * @return
	 * @throws InterruptedException 
	 */
	public List<Condition> mineExhaustively(List<String> entities2Describe, Set<Condition> setCond,
			List<List<Condition>> listSolution, List<Condition> rankedCondition, REMIRuntimeInfo... infos) throws InterruptedException {

		if (infos.length > 0) {
			infos[0].sizePriorityQueue = setCond.size();
		}
		
		if (this.scalabilityMode)
			return Collections.emptyList();
		
		List<String> coveredEntities = new ArrayList<>();
		long a = System.currentTimeMillis();		
		Condition[] masterQueue = sortConditionsInParallel(setCond);
		long b = System.currentTimeMillis();
		Solution sol = new Solution();

		if (infos.length > 0) {
			infos[0].sortingRuntimeMs = (b - a);
			int topK = Math.max(0, masterQueue.length - 10);
			for (int j = masterQueue.length - 1;  j >= topK; --j ) {
				infos[0].topConditions.add(masterQueue[j]);
			}
		}
		
		if (this.isEnableDebugging()) {
			for (Condition c : masterQueue) { 
				 System.out.println(c + " -- " + c.getComplexity());		
			}
			System.out.print("Durée pour classer les éléments dans la queue de priorité : ");
			System.out.println(b - a);
			System.out.println(" en millisecondes");
		}
		
		a = System.currentTimeMillis();
		Stack<Condition> stack = new Stack<Condition>();
		Stack<Integer> indexesStack = new Stack<Integer>();
		Set<String> setEntities2Describe = new LinkedHashSet<>(entities2Describe);
		int masterIndex = masterQueue.length - 1;
		// first loop of condition
		while (masterIndex >= 0) {
			int workIndex = masterIndex;
			coveredEntities.clear();
			stack.clear();
			indexesStack.clear();
			while (workIndex >= 0) {
				// Put the first condition of the sorted queue in the stack
				stack.push(masterQueue[workIndex]);
				indexesStack.push(workIndex);
				
				// Impose the max depth argument
				if (stack.size() > this.maxDepth) {
					stack.pop();
					indexesStack.pop();
					stack.pop();
					indexesStack.pop();
					--workIndex;
					continue;
				}
				
				//System.out.println("Stack: " + indexesStack);
				// Check that the last condition added is not redundant and modify the
				// entityCovered by the stack
				coveredEntities = getCoveredEntities(this.kb, stack);
				SetRelationCase relationCase = compareCollectionEntitiesExhaustively(coveredEntities,
						setEntities2Describe);
				if (relationCase == SetRelationCase.EQUALITY) {
					listSolution.add(transformStack2List(stack));
					if (sol.update(stack)) {
						if (this.enableDebugging)
							System.out.println(" Best solution until now: " + sol.getElements());
					}
					// If we find a solution of size 1 it must be the least complex
					// because it is a depth-first search: all upcoming solutions
					// must be more complex
					if (stack.size() == 1) {
						// It must be a solution. Force both loops to finish
						masterIndex = -1;
						workIndex = -1;
					} else {
						stack.pop();
						indexesStack.pop();
					}

					stack.pop();
					indexesStack.pop();
					//System.out.println("Stack after pruning: " + indexesStack);
					if (stack.isEmpty())
						break;

				} else {
					--workIndex;
				}
			}

			--masterIndex;

			// Check if the first loop have no solution. If it is the case there is no
			// possible solution to describe elements
			if (sol.isTrivialSolution()) {
				break;
			}

		}
		
		b = System.currentTimeMillis();
		if (infos.length > 0) {
			infos[0].miningRuntimeMs = (b - a);			
		}
		
		if (this.isEnableDebugging()) {
			System.out.print("Durée pour chercher dans la queue de priorité: ");
			System.out.println(b - a);
			System.out.println(" en millisecondes");
			System.out.println("Complexity of the last solution " + sol.getComplexity());
		}
		return sol.getElements();
	}

	/**
	 * Deep research for the multi-thread
	 * 
	 * @param entities2Describe
	 * @param setCond
	 * @return
	 * @throws InterruptedException
	 */
	public List<Condition> mineExhaustivelyInParallel(List<String> entities2Describe, 
			Set<Condition> setCond, REMIRuntimeInfo... infos)
			throws InterruptedException {
		if (infos.length > 0) {
			infos[0].sizePriorityQueue = setCond.size();
		}
		
		if (this.scalabilityMode)
			return Collections.emptyList();
		
		long a = System.currentTimeMillis();
		AtomicInteger cursor = new AtomicInteger(setCond.size() - 1);
		AtomicInteger finishedCursor = new AtomicInteger(Integer.MIN_VALUE);
		Condition[] masterQueue = sortConditionsInParallel(setCond);
		Solution sol = new Solution();
		
		long b = System.currentTimeMillis();
		if (infos.length > 0) {
			infos[0].sortingRuntimeMs = (b - a);
			int topK = 0;
			for (int j = masterQueue.length - 1;  j >= 0; --j) {
				if (masterQueue[j].getLastObject().startsWith("http://") &&
						!masterQueue[j].getLastPredicate().endsWith("-inv") &&
						!masterQueue[j].getLastPredicate().equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")) {
					infos[0].topConditions.add(masterQueue[j]);
					++topK;
				}
				
				if(topK > 10) {
					break;
				}
			}
		}
		
		if (this.isEnableDebugging()) {
			for (Condition c : masterQueue) { 
				 System.out.println(c + " -- " + c.getComplexity());		
			}
			System.out.print("Durée pour classer les éléments dans la queue de priorité : ");
			System.out.println(b - a);
			System.out.println(" en millisecondes");
		}

		ReadWriteLock rwl = new ReentrantReadWriteLock();
		ReadWriteLock cursorRWL = new ReentrantReadWriteLock();
		int nThreads = Math.min(this.nbProcessors, masterQueue.length);
		if (this.isEnableDebugging()) {
			System.out.println("Using " + nThreads + " threads");
		}
		
		Thread[] jobs = new Thread[nThreads];
		for (int i = 0; i < jobs.length; ++i) {
			jobs[i] = new Thread(new REMiningJob(masterQueue, entities2Describe, this, cursor, rwl, cursorRWL, finishedCursor, sol));
			jobs[i].start();
		}
		
		for (int i = 0; i < jobs.length; ++i) {
			jobs[i].join();
		}
		
		if (infos.length > 0) {
			long c = System.currentTimeMillis();
			infos[0].miningRuntimeMs = (c - b);
			infos[0].totalRuntimeMs = (c - a);
		}

		if (this.enableDebugging) {
			System.out.println("Complexity of the last solution " + sol.getComplexity());
		}
		return sol.getElements();
	}

	/**
	 * execute the breadth first search algorithm
	 * 
	 * @param entities2Describe
	 * @param setCond
	 * @return
	 * @throws InterruptedException 
	 */
	public List<Condition> mineExhaustivelyBreadth(List<String> entities2Describe, Set<Condition> setCond) throws InterruptedException {
		long a = System.currentTimeMillis();
		Condition[] masterQueue = sortConditionsInParallel(setCond);
		if (this.isEnableDebugging()) {
			long b = System.currentTimeMillis();
			System.out.print("Durée pour classer les éléments dans la queue de priorité : ");
			System.out.println(b - a);
			System.out.println(" en millisecondes");
		}
		
		Solution sol = new Solution();
		a = System.currentTimeMillis();
		int maxIndexGlobal = masterQueue.length - 1;
		int minIndexGlobal = 0;
		Queue<List<Integer>> queue = new ArrayDeque<>();
		for (int i = maxIndexGlobal; i >= 0; i--) {
			queue.add(Arrays.asList(i));
		}
		while (!queue.isEmpty()) {
			List<Integer> current = queue.poll();
			List<Condition> listCondition = fromIndexToCond(current, masterQueue);
			if (isASolution(listCondition, entities2Describe)) {
				sol.update(listCondition);
				System.out.println("solution until now : " + sol.getElements());
				minIndexGlobal = getLastIndex(current);
			} else {
				extendCurrentSolution(queue, current, minIndexGlobal, maxIndexGlobal);
			}
			maxIndexGlobal--;
		}

		return sol.getElements();
	}

	/**
	 * execute the first breadth search on the conditions of size 1
	 * 
	 * @param entities2Describe
	 * @param setCond
	 * @return
	 * @throws InterruptedException 
	 */
	protected List<Condition> mineFirstBreadthForParellel(List<String> entities2Describe, Set<Condition> setCond) throws InterruptedException {
		long a = System.currentTimeMillis();
		Condition[] masterQueue = sortConditionsInParallel(setCond);
		if (this.isEnableDebugging()) {
			long b = System.currentTimeMillis();
			System.out.print("Durée pour classer les éléments dans la queue de priorité : ");
			System.out.println(b - a);
			System.out.println(" en millisecondes");
		}
		Solution sol = new Solution();
		a = System.currentTimeMillis();
		int maxIndexGlobal = masterQueue.length - 1;
		Queue<List<Integer>> queue = new ArrayDeque<>();
		for (int i = maxIndexGlobal; i >= 0; i--) {
			queue.add(Arrays.asList(i));
		}
		for (int i = 0; i < maxIndexGlobal + 1; i++) {
			List<Integer> current = queue.poll();
			List<Condition> listCondition = fromIndexToCond(current, masterQueue);
			if (isASolution(listCondition, entities2Describe)) {
				return listCondition;
			}
		}

		return sol.getElements();
	}

	/**
	 * Execute the research without cutting the research tree to return all the
	 * different solution found
	 * 
	 * @param entities2Describe
	 * @param setCond
	 * @param listSolution
	 * @param time TODO
	 * @param rankedCondition TODO
	 * @return
	 * @throws InterruptedException 
	 */
	public List<Condition> mineExperimental(List<String> entities2Describe, Set<Condition> setCond,
			List<List<Condition>> listSolution, Map<Long, String> time, List<Condition> rankedCondition) throws InterruptedException {
		List<String> coveredEntities = new ArrayList<>();
		long a = System.currentTimeMillis();
		Condition[] masterQueue = sortConditionsInParallel(setCond);
		long b = System.currentTimeMillis();
		time.put(b-a, "\t");
		Solution sol = new Solution();
		/*for(int i = 0; i < masterQueue.length; i++) {
			rankedCondition.add(masterQueue[i]);
		}*/
		System.out.println("sorting conditions complete ");
		Stack<Condition> stack = new Stack<Condition>();
		Stack<Integer> indexesStack = new Stack<Integer>();
		Set<String> setEntities2Describe = new LinkedHashSet<>(entities2Describe);
		int masterIndex = masterQueue.length - 1;
		// first loop of condition
		while (masterIndex >= 0) {
			int workIndex = masterIndex;
			coveredEntities.clear();
			stack.clear();
			indexesStack.clear();
			while (workIndex >= 0) {
				// Put the first condition of the sorted queue in the stack
				stack.push(masterQueue[workIndex]);
				indexesStack.push(workIndex);
				// Check that the last condition added is not redundant and modify the
				// entityCovered by the stack
				coveredEntities = getCoveredEntities(this.kb, stack);
				SetRelationCase relationCase = compareCollectionEntitiesExhaustively(coveredEntities,
						setEntities2Describe);
				if (relationCase == SetRelationCase.EQUALITY) {
					System.out.println(stack);
					sol.update(stack);
					listSolution.add(transformStack2List(stack));
					
					stack.pop();
					indexesStack.pop();
					if (stack.isEmpty())
						break;
					else {
						stack.pop();
						indexesStack.pop();
					}
					
					if (stack.isEmpty())
						break;
				} else {
					--workIndex;
				}
			}

			--masterIndex;

			// Check if the first loop have no solution. If it is the case there is no
			// possible solution to describe elements
			if (sol.isTrivialSolution()) {
				break;
			}

		}
		a = System.currentTimeMillis();
		time.put(a - b, "\t");
		return sol.getElements();
	}

	/**
	 * Extend the list of Condition with the current list and add the elements in
	 * the queue from the min index to max index
	 * 
	 * @param queue
	 * @param current
	 * @param minIndexGlobal
	 * @param maxIndexGlobal
	 */
	private void extendCurrentSolution(Queue<List<Integer>> queue, List<Integer> current, int minIndexGlobal,
			int maxIndexGlobal) {
		for (int i = maxIndexGlobal - 1; i >= minIndexGlobal; i--) {
			List<Integer> child = new ArrayList<>(current);
			child.add(i);
			queue.add(child);
		}
	}

	/**
	 * return the index of the last element of the list of integer
	 * 
	 * @param current
	 * @return
	 */
	private int getLastIndex(List<Integer> current) {
		return current.get(current.size() - 1);
	}

	/**
	 * execute the select distinct and return if the list of Condition is a solution
	 * or no
	 * 
	 * @param listCondition
	 * @param entities2Describe
	 * @return
	 */
	private boolean isASolution(List<Condition> listCondition, List<String> entities2Describe) {
		Set<String> listEntitiesCovered = new LinkedHashSet<>(kb.selectDistinct(listCondition.get(0)));
		for (int i = 1; i < listCondition.size(); i++) {
			listEntitiesCovered.retainAll(kb.selectDistinct(listCondition.get(i)));
		}
		if (listEntitiesCovered.size() == entities2Describe.size()) {
			return true;
		}
		return false;
	}

	/**
	 * change the list of integer to a list of Condition with the array of Condition
	 * 
	 * @param current
	 * @param masterQueue
	 * @return
	 */
	private List<Condition> fromIndexToCond(List<Integer> current, Condition[] masterQueue) {
		List<Condition> listCond = new ArrayList<>();
		for (Integer i : current) {
			listCond.add(masterQueue[i]);
		}
		return listCond;
	}

	/**
	 * Final algorithm
	 * 
	 * @param List
	 *            of ByteString target
	 * @return List of the Condition common to all the entities
	 * @throws InterruptedException 
	 * @see mine(list<ByteString>, Set<Condition>)
	 */
	public List<Condition> mine(List<String> entities2Describe, REMIRuntimeInfo... infos) throws InterruptedException {
		long a = System.currentTimeMillis();
		Set<Condition> setCond = intersectEntities(entities2Describe);
		long b = System.currentTimeMillis();
		if (this.enableDebugging) {
			System.out.print("time of intersect function in milliseconds : ");
			System.out.println(b - a);
		}
		
		if (infos.length > 0) {
			infos[0].intersectRuntimeMs = (b - a);
		}
		

		List<Condition> listMine = null;
		List<List<Condition>> listOfOtherSolutions = new ArrayList<>();
		List<Condition> rankedCondition = new ArrayList<>();
		Map<Long, String> time = new LinkedHashMap<>();
		if (this.exhaustiveSearch) {
			if (this.nbProcessors <= 1) {				
				listMine = mineExhaustively(entities2Describe, setCond, listOfOtherSolutions, rankedCondition, infos);
				if (this.enableDebugging) {
					System.out.println("les solutions trouvés sont : " + listOfOtherSolutions);
				}
			} else {
				try {
					listMine = mineExhaustivelyInParallel(entities2Describe, setCond, infos);
				} catch (InterruptedException e) {
					throw e;
				}
			}
		} else {
			if (this.deepBreadthSearch) {
				listMine = mineExhaustivelyBreadth(entities2Describe, setCond);
			} else {
				if(this.experimentalSearch) {
					System.out.println("Les entités cibles sont : " + entities2Describe);
					System.out.println("Les conditions sont : " + setCond);
					listMine = mineExperimental(entities2Describe, setCond, listOfOtherSolutions, time, rankedCondition);
					System.out.println(time);
					System.out.println("liste de tous les éléments solutions : " + listOfOtherSolutions);
					System.out.println("liste des conditions triées : " + rankedCondition);

				}
				else listMine = mine(entities2Describe, setCond);
			}
		}

		b = System.currentTimeMillis();
		if (infos.length > 0) {
			infos[0].totalRuntimeMs = (b - a);
		}
		
		if (this.enableDebugging) {
			System.out.print("time of mine function in milliseconds : ");
			System.out.println(b - a);
			System.out.println();
			System.out.println();
			System.out.println();
		}
		if (listMine.isEmpty() && this.enableDebugging)
			System.out.println("pas de résultat");
		return listMine;
	}
	

	public KB getThreadSafeKB() {
		if (this.kb instanceof HDTKB) {
			try {
				return new HDTKB((HDTKB) this.kb);
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		} else if (this.kb instanceof InMemoryKB) {
			return this.kb;
		} else {
			return null;
		}
	}

	/**
	 * Parses the command line arguments and the returns an object that maps each
	 * argument to its value.
	 * 
	 * @param args
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static String parseArguments(String[] args) throws IOException, InterruptedException {
		HelpFormatter formatter = new HelpFormatter();
		String fileName = null;

		// create the command line parser
		CommandLineParser parser = new PosixParser();
		// create the Options
		Options options = new Options();
		CommandLine cli = null;

		OptionBuilder.withArgName("kb-bases");
		OptionBuilder.hasArg();
		OptionBuilder.isRequired();
		OptionBuilder.withDescription("Choose a knowledge bases, "
				+ "the value is considered as a valid knowledge bases of entities covered ");
		Option kbBasesOpt = OptionBuilder.create("kb");

		OptionBuilder.withArgName("enableExtension");
		OptionBuilder.withDescription("activate the extension of the Condition or not");
		Option enableExtensionOpt = OptionBuilder.create("ee");

		OptionBuilder.withArgName("enableExtensionFinal");
		OptionBuilder.withDescription("activate the extension of the Condition of size 3 or not");
		Option enableExtensionFinalOpt = OptionBuilder.create("eef");

		OptionBuilder.withArgName("enableDebugging");
		OptionBuilder.withDescription("activate the debug or not");
		Option enableDebuggingOpt = OptionBuilder.create("ed");

		OptionBuilder.withArgName("complexity-function");
		OptionBuilder.withDescription("cf");
		OptionBuilder.hasArg();
		Option complexityFunctionOpt = OptionBuilder.create("cf");

		OptionBuilder.withArgName("exhaustive");
		OptionBuilder.withDescription("It enables exhaustive search of solutions.");
		Option exhaustiveOpt = OptionBuilder.create("exh");
		
		OptionBuilder.withArgName("experimental");
		OptionBuilder.withDescription("It enables experimental search of solutions.");
		Option experimentalOpt = OptionBuilder.create("exp");

		OptionBuilder.withArgName("breadth first search");
		OptionBuilder.withDescription("It enables deep breath search of solutions.");
		Option breadthSearchOpt = OptionBuilder.create("bfs");

		OptionBuilder.withArgName("number processors");
		OptionBuilder.withDescription("It indicates the number of processors the computer will use.");
		OptionBuilder.hasArg();
		Option nbProcessorsOpt = OptionBuilder.create("nb");
		
		OptionBuilder.withArgName("scalability-measure");
		OptionBuilder.withDescription("It runs the system in scalability-measure mode: the system mines all subgraph expressions up to 3 atoms and 2 variables and reports the number of subgraph expressions");
		Option scalabilityOpt = OptionBuilder.create("cm");
		
		OptionBuilder.withArgName("page-rank");
		OptionBuilder.withDescription("Use page-rank as notion of prominence for entities");
		Option pageRankOpt = OptionBuilder.create("pr");

		options.addOption(kbBasesOpt);
		options.addOption(enableExtensionOpt);
		options.addOption(enableExtensionFinalOpt);
		options.addOption(enableDebuggingOpt);
		options.addOption(complexityFunctionOpt);
		options.addOption(exhaustiveOpt);
		options.addOption(experimentalOpt);
		options.addOption(nbProcessorsOpt);
		options.addOption(breadthSearchOpt);
		options.addOption(scalabilityOpt);
		options.addOption(pageRankOpt);

		try {
			cli = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println("Unexpected exception: " + e.getMessage());
			formatter.printHelp("CombinationsExploration [OPTIONS] <TSV FILES>", options);
			System.exit(1);
		}

		Boolean breadthSearch = false;
		String kbName = args[0];
		String storage = "disk";
		Boolean enableExtension = false;
		Boolean enableExtensionFinal = false;
		Boolean enableDebugging = false;
		boolean exhaustive = cli.hasOption("exh");
		boolean experimental = cli.hasOption("exp");
		int nbProcessors = 1;
		boolean scalabilityMode = cli.hasOption("cm");
		ComplexityFunction cf = ComplexityFunction.DEGREE_BASED;
		List<String> listOptionToPrint = new ArrayList<>();

		if (cli.hasOption("kb")) {
			try {
				kbName = cli.getOptionValue("kb");
			} catch (NumberFormatException e) {
				System.out.println("Unexpected exception: " + e.getMessage());
				formatter.printHelp("CombinationsExploration [OPTIONS] <TSV FILES>", options);
				System.exit(1);
			}
		}

		if (cli.hasOption("s")) {
			try {
				storage = cli.getOptionValue("s");
			} catch (NumberFormatException e) {
				System.out.println("Unexpected exception: " + e.getMessage());
				formatter.printHelp("CombinationsExploration [OPTIONS] <TSV FILES>", options);
				System.exit(1);
			}
		}
		listOptionToPrint.add("in " + storage);

		if (cli.hasOption("ee")) {
			listOptionToPrint.add("extension of size 2 activated");
			enableExtension = true;
		} else {
			if (cli.hasOption("eef")) {
				enableExtensionFinal = true;
				listOptionToPrint.add("extension of size 3 activated");
			} else {
				listOptionToPrint.add("extension deactivated");
			}
		}

		if (cli.hasOption("ed")) {
			listOptionToPrint.add("debugging activated");
			enableDebugging = true;
		} else {
			listOptionToPrint.add("debugging deactivated");
		}

		if (cli.hasOption("cf")) {
			cf = ComplexityFunction.valueOf(cli.getOptionValue("cf"));
			listOptionToPrint.add("complexity function = " + cf);
		}

		if (exhaustive) {
			listOptionToPrint.add("exhaustive function activated");
		} else {
			if (cli.hasOption("bfs")) {
				breadthSearch = true;
				listOptionToPrint.add("breadth first search activated");
			} else {
				listOptionToPrint.add("exhaustive function deactivated");
			}
		}

		if(experimental) {
			listOptionToPrint.add("experimental function activated");
		}		
		
		if (cli.hasOption("nb")) {
			if (cli.getOptionValue("nb").toLowerCase().equals("all")) {
				nbProcessors = Runtime.getRuntime().availableProcessors();
			} else {
				try {
					nbProcessors = Integer.parseInt(cli.getOptionValue("nb"));
				} catch (NumberFormatException e) {
					System.out.println("Unexpected exception: " + e.getMessage());
					formatter.printHelp("CombinationsExploration [OPTIONS] <TSV FILES>", options);
					System.exit(1);
				}
			}
		}
		

		System.out.println("Using at most " + nbProcessors + " thread(s)");

		if (kbName.equals("yago-test")) {
			fileName = "src/data/yago-test";
		} else if (kbName.equals("yago")) {
			fileName = "src/data/yago";
		} else if (kbName.equals("dbpedia")) {
			fileName = "src/data/dbpedia";
		} else if (kbName.equals("test")) {
			fileName = "src/data/test";
			GlobalConfig.typeRelation = "<rdf:type>";
		} else if (kbName.startsWith("wikidata")) {
			fileName = "src/data/wikidata";
			GlobalConfig.typeRelation = "http://wikidata.org/property/instance_of_P31";
		} else {
			System.err.println("No recognized knowledge base was provided.");
			System.exit(1);
		}

		boolean usePageRank = cli.hasOption("pr");
		GlobalConfig.setKbPath(fileName, kbName + ".hdt", usePageRank);		
		KB kb = new HDTKB(GlobalConfig.getKbPath());

		if (usePageRank) listOptionToPrint.add("using page rank as relevance metric");
		if (scalabilityMode) listOptionToPrint.add("scalability mode activated");


		listOptionToPrint.add(0, fileName);
		System.out.println(listOptionToPrint);
		System.out.print("les entités cibles sont : ");
		Pattern p = Pattern.compile(
				"(http(s)?://([a-zA-Z.0-9]+/)?([-_0-9a-zA-Z]+/)*)*([-_'.\\p{javaLowerCase}\\p{javaUpperCase}\\\\\"()@]+([\\p{Punct}0-9a-zA-Z]*)?)");
		List<String> listStr = new ArrayList<>();
		for (String arg : cli.getArgList()) {
			Matcher m = p.matcher(arg);
			while (m.find()) {
				listStr.add(m.group(5));
			}
		}

		System.out.println(listStr);
		REMiner reMiner = new REMiner(kb, enableDebugging, enableExtension, enableExtensionFinal, exhaustive, cf,
				nbProcessors, breadthSearch, experimental);
		reMiner.setScalabilityMode(scalabilityMode);
		List<String> listEntities = cli.getArgList();
		List<Condition> listCond = reMiner.mine(listEntities);
		return reMiner.cleanForOutput2(listCond);

	}

	public static void main(String[] args) throws IOException, InterruptedException {
		System.out.println(parseArguments(args));
	}

}

package fr.inria.res.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author lgalarra
 *
 */
public class KBUtils {
	

	/**
	 * It returns true if there exists at least one binding for the 
	 * variable positions in the given list of atoms. This method assumes
	 * that the list of atoms defines a path query.
	 * @param kb
	 * @param atoms. It is a sequence of atoms of the form 
	 * [ [?x0 r0 V0] ^ [?x1 r1 ?x0] ^ [?x2 r2 ?x1] ... 
	 * @return
	 */
	public static boolean existsBinding(KB kb, List<String[]> atoms) {
		String[] firstAtom = atoms.get(0);
		Set<String> subjectValues = kb.selectDistinctS(firstAtom);
		List<String[]> subQuery = new ArrayList<>(atoms.subList(1, atoms.size()));
		if (!subQuery.isEmpty()) {
			for (String subject : subjectValues) {
				subQuery.get(0)[2] = subject;
				if (existsBinding(kb, subQuery)) {
					return true;
				}
			}
			
			return false;
			
		} else {
			return !subjectValues.isEmpty();
		}
	}
	
	/**
	 * It returns true if the provided predicates have an inverse relationship, e.g., p1 p1-inv
	 * 
	 * @param predicate1
	 * @param predicate2
	 * @return
	 */
	public static boolean areInversePairs(String predicate1, String predicate2) {
		return predicate1.equals(predicate2 + "-inv") ||
				predicate2.equals(predicate1 + "-inv");
	}
	
	/**
	 * It returns true if the given IRI corresponds to a blank node.
	 * @param constant
	 */
	public static boolean isBlankNode(String constant) {
		Pattern p = Pattern.compile("__[0-9]+$");
		Matcher m = p.matcher(constant);
		return m.find();			
	}
	
	public static void main(String args[]) {
		System.out.println(isBlankNode("db:Ray_Miller_(journalist)__1"));
	}

}

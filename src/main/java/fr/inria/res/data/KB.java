package fr.inria.res.data;

import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inria.res.Condition;

public abstract class KB {
	
	/**
	 * It takes an atom of the form [subject*, predicate*, ""] and returns
	 * the list of all different subjects with the given predicate and object 
	 * (argument* means that the argument can be either a bound or unbound value).
	 * @param atom
	 * @return
	 */
	public abstract Set<String> selectDistinctO(String[] atom);
	
	
	/**
	 * It takes an atom of the form ["", predicate*, object*] and returns
	 * the list of all different subjects with the given predicate and object 
	 * (argument* means that the argument can be either a bound or unbound value).
	 * @param atom
	 * @return
	 */
	public abstract Set<String> selectDistinctS(String[] atom);
	
	
	/**
	 * It takes an atom of the form [subject, "", ""] and returns the different 
	 * predicates and objects as a map. The predicates are the keys in the map whereas
	 * the list of objects co-occuring with the predicate are the values. The subject
	 * MUST be bound.
	 * @param atom
	 * @return
	 */
	public abstract Map<String, Set<String>> selectDistinctPO(String[] atom);
	// The count version of the previous method
	public abstract double countDistinctPO(String[] atom);
	
	
	public abstract double countDistinctP();

	
	/**
	 * It returns a set of the entities that match the given condition.
	 * @param condition
	 * @return
	 */
	public abstract Set<String> selectDistinct(Condition condition);
	
	public abstract long countDistinct(String variable, List<String[]> atoms);
	
	public abstract long countDistinct(String variable1, String variable2, List<String[]> atoms);
	
	/**
	 * It returns true if there exists at least one binding for the 
	 * variable positions in the given list of atoms.
	 * @param kb
	 * @param atoms. It is a sequence of atoms of the form 
	 * [ [?x0 r0 V0] ^ [?x1 r1 ?x0] ^ [?x2 r2 ?x1] ... 
	 * @return
	 */
	public abstract boolean existsBinding(List<String[]> atoms);
	
	/**
	 * It cleans a string for legible output.
	 * @return
	 */
	public abstract String cleanString(String str);


	public abstract long size();

}

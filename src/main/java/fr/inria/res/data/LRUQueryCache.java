package fr.inria.res.data;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.PriorityQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class LRUQueryCache<V> {
	private ReadWriteLock rwlock;	
	private Lock wlock;
	private Lock rlock;

	private HashMap<String, V> dictValue;
	private HashMap<String, Long> dictTimestamp;
	private PriorityQueue<Pair<String, Long>> timeQueue;
	private int maxSize;

	public LRUQueryCache(int maxSize){
		this.rwlock = new ReentrantReadWriteLock();
		this.wlock = this.rwlock.writeLock();
		this.rlock = this.rwlock.readLock();
        this.maxSize = maxSize;
        this.dictValue = new LinkedHashMap<>();
        this.dictTimestamp = new LinkedHashMap<>();
        this.timeQueue = new PriorityQueue<>(new Comparator<Pair<String, Long>>() {

			@Override
			public int compare(Pair<String, Long> o1, Pair<String, Long> o2) {
				int comp = Long.compare(o1.getRight(), o2.getRight());
				if (comp == 0) {
					return o1.getLeft().compareTo(o2.getLeft());
				} else {
					return comp;
				}
			}

		});
    }

	public V get(String query) {
		rlock.lock();
		V value = dictValue.get(query); 
		if (value != null) {
			Long timeStamp = dictTimestamp.get(query);
			rlock.unlock();
			
			// Update the priority queue
			wlock.lock();
			timeQueue.remove(new MutablePair<>(query, timeStamp));
			long nv = System.currentTimeMillis();
			timeQueue.add(new MutablePair<>(query, nv));
			dictTimestamp.put(query, nv);
			wlock.unlock();
		} else {
			rlock.unlock();
		}

		return value;
	}
	
	public boolean contains(String query) {
		rlock.lock();
		boolean b =  dictValue.containsKey(query);
		rlock.unlock();
		return b;
	}
	

	public V put(String query, V value) {
		wlock.lock();
					
		if (timeQueue.size() >= maxSize) {
			// Remove the least-recently-used item
			Pair<String, Long> lru = timeQueue.poll();			
			dictValue.remove(lru.getLeft());
			dictTimestamp.remove(lru.getLeft());
		}
		
		Long timeStamp = System.currentTimeMillis();		
		if (dictValue.containsKey(query)) {
			timeQueue.remove(new MutablePair<>(query, dictTimestamp.get(query)));
		}
		
		timeQueue.add(new MutablePair<>(query, timeStamp));
		dictTimestamp.put(query, timeStamp);
		V v = dictValue.put(query, value);
		wlock.unlock();
		return v;		
	}
}

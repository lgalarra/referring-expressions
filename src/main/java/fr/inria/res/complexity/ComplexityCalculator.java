package fr.inria.res.complexity;

import java.util.Comparator;

import fr.inria.res.Condition;

/**
 * It defines a family of classes that can calculate the complexity
 * of conditions for the description of entities.
 * 
 * @author lgalarra
 *
 */
public abstract class ComplexityCalculator implements Comparator<Condition>{
	
	
	/**
	 * Get the number of bits used to represent the given description.
	 * @param c Condition to be encoded.
	 * @return
	 */
	public abstract double getComplexity(Condition c);
	
	public abstract double getComplexity(String constant);

}

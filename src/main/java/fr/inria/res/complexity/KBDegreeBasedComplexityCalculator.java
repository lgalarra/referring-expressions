package fr.inria.res.complexity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import fr.inria.res.Atom;
import fr.inria.res.Condition;
import fr.inria.res.data.KB;

public class KBDegreeBasedComplexityCalculator extends KBEntropyBasedComplexityCalculator {
	
	protected double defaultPredicateSlope;
	
	protected double defaultPredicateIntercept;
	
	protected double defaultConstantsGivenPredicateSlope;
	
	protected double defaultConstantsGivenPredicateIntercept;
	
	protected double defaultConstantsSlope;
	
	protected double defaultConstantsIntercept;
	
	Map<String, Pair<Double, Double>> coefficients;
	
	public static int thresholdForExactAdjustmentCalculation = 100000;

	public KBDegreeBasedComplexityCalculator(KB kb) {
		super(kb);
		this.defaultPredicateSlope = -1.0;
		this.defaultPredicateIntercept = 64.0;
		this.defaultConstantsGivenPredicateSlope = -1.0;
		this.defaultConstantsGivenPredicateIntercept = 64.0;
		this.defaultConstantsSlope = -1.0;
		this.defaultConstantsIntercept = 64.0;
	}
	
	public KBDegreeBasedComplexityCalculator(KB kb, double defaultPredicateSlope,
			double defaultPredicateIntercept, Map<String, Pair<Double, Double>> coefficients) {
		this(kb);
		this.defaultPredicateSlope = defaultPredicateSlope;
		this.defaultPredicateIntercept = defaultPredicateIntercept;
		this.coefficients = coefficients;
	}
	
	public KBDegreeBasedComplexityCalculator(KB kb, double defaultPredicateSlope,
			double defaultPredicateIntercept, double defaultConstantsSlope,
			double defaultConstantsIntercept,
			Map<String, Pair<Double, Double>> coefficients) {
		this(kb);
		this.defaultConstantsSlope = defaultConstantsSlope;
		this.defaultConstantsIntercept = defaultConstantsIntercept;
		this.defaultPredicateSlope = defaultPredicateSlope;
		this.defaultPredicateIntercept = defaultPredicateIntercept;
		this.coefficients = coefficients;
	}
	
	public KBDegreeBasedComplexityCalculator(KB kb, double slopePredicate, 
			double interceptPredicate, double slopeConstantsGivenPredicate, 
			double interceptConstantsGivenPredicate) {
		super(kb);
		this.defaultPredicateSlope = slopePredicate;
		this.defaultPredicateIntercept = interceptPredicate;
		this.defaultConstantsGivenPredicateSlope = slopeConstantsGivenPredicate;
		this.defaultConstantsGivenPredicateIntercept = interceptConstantsGivenPredicate;
	}
	
	public KBDegreeBasedComplexityCalculator(KB kb, double slopePredicate, 
			double interceptPredicate, double slopeConstants, double interceptConstants,
			double slopeConstantsGivenPredicate, double interceptConstantsGivenPredicate) {
		super(kb);
		this.defaultPredicateSlope = slopePredicate;
		this.defaultPredicateIntercept = interceptPredicate;
		this.defaultConstantsGivenPredicateSlope = slopeConstantsGivenPredicate;
		this.defaultConstantsSlope = slopeConstants;
		this.defaultConstantsIntercept = interceptConstants;
		this.defaultConstantsGivenPredicateIntercept = interceptConstantsGivenPredicate;
	}

	
	@Override
	public double getComplexity(Condition c) {
		if (c.getComplexity() > -1.0)
			return c.getComplexity();
		
		Map<Atom, List<Atom>> atom2Ancestors = c.ancestorWalk();
		double cumulativeComplexity = 0.0;

		for (Atom atom : atom2Ancestors.keySet()) {
			List<Atom> ancestors = atom2Ancestors.get(atom);
			List<String> otherPredicates = getPredicates(ancestors);
			
			double degreeRelation = getCardinalityOfPredicate(atom.getPredicat());
			double complexityRelation =  (this.defaultPredicateSlope * Math.log(degreeRelation) / Math.log(2)) 
					+ this.defaultPredicateIntercept;
			
			//System.out.println("Complexity of predicate " + atom.getPredicat() + " " + complexityRelation);
			
			double kr = 0.5;
			if (!otherPredicates.isEmpty()
					&& !containsNonSelectivePredicates(otherPredicates)) {
				kr = interceptAdjustmentForPredicates(otherPredicates);
			} else {
				kr = Math.pow(kr, otherPredicates.size());
			}
			
						
			double adjustment = Math.log(kr) / Math.log(2);
			complexityRelation += adjustment;
			
			// Proposed adjustment for next time
			//double adjustment = -Math.log(kr) / Math.log(2);
			//complexityRelation -= Math.sqrt(adjustment);
			//===============================================
			
			// Just be sure it is not a negative number
			complexityRelation = Math.max(1.0, complexityRelation);
			
			double degreeConstant = 1.0;			
			double slopeConstants = this.defaultConstantsGivenPredicateSlope;
			double interceptConstants = this.defaultConstantsGivenPredicateIntercept;
			double complexityObject = 0.0;
			
			
			if (atom.getObject() != null && !atom.getObject().equals("")) {
				adjustment = 0.0;
				Pair<Double, Double> coeffs = coefficients.get(atom.getPredicat());
				if (coeffs != null) {
					slopeConstants = coeffs.getKey();
					interceptConstants = coeffs.getValue();
				}
				
				degreeConstant = getCardinalityOfConstant(atom.getPredicat(), atom.getObject()) + 1;	
				complexityObject = slopeConstants * Math.log(degreeConstant) / Math.log(2) + interceptConstants;
				//System.out.println("Complexity of object " + atom.getObject()+ " " + complexityObject);
				
				// We have to adjust the intercept to account
				// for the size of the ranking
				if (!otherPredicates.isEmpty()) {
					adjustment = Math.log(interceptAdjustmentForConstants(atom.getPredicat(), 
							otherPredicates)) / Math.log(2);
				}
				
				complexityObject += adjustment;
				
				// Proposed adjustment for next time
				//adjustment = -adjustment;
				//complexityObject -= Math.sqrt(adjustment);
				//======================================
				
				//complexityObject = Math.max(1.0, complexityObject);
			}
			
			
			
			cumulativeComplexity += (complexityRelation + complexityObject);
		}
		
		c.setComplexity(cumulativeComplexity);
		return cumulativeComplexity;
	}



	private double getCardinalityOfPredicate(String predicat) {
		List<String[]> atoms = new ArrayList<>();		
		String[] firstAtom = new String[] {"?s", predicat, "?o"};
		atoms.add(firstAtom);
		return kb.countDistinct("?s", "?o", atoms);
	}

	private boolean containsNonSelectivePredicates(List<String> otherPredicates) {
		for (String predicate : otherPredicates) {
			List<String[]> atoms = new ArrayList<>();
			atoms.add(new String[] {"?s", predicate, "?o"});
			if (kb.countDistinct("?s", "?o", atoms) >= thresholdForExactAdjustmentCalculation) {
				return true;
			}
		} 
		
		return false;
	}

	private double interceptAdjustmentForPredicates(List<String> otherPredicates) {
		List<String[]> atoms = getQueryForConditionalRankingSize(otherPredicates);
		long numberOfEntitiesNewRanking = kb.countDistinct("?p", atoms);
		return (double) (numberOfEntitiesNewRanking + 1) / (kb.countDistinctP() + 1);
	}
	
	

	private List<String[]> getQueryForConditionalRankingSize(List<String> otherPredicates) {
		List<String[]> atoms = new ArrayList<>();
		int variable = 0;
		atoms.add(new String[] {"?x" + variable, "?p", "?y"});
		
		for (String predicate : otherPredicates) {
			atoms.add(new String[] {"?x" + (variable + 1), predicate, "?x" + variable});
		}
		
		return atoms;
	}

	private double interceptAdjustmentForConstants(String predicate, List<String> otherPredicates) {
		List<String[]> atoms = getQueryForConditionalRankingOfConstantsSize(predicate, otherPredicates);
		long numberOfEntitiesNewRanking = kb.countDistinct("?o", atoms);
		List<String[]> atomsDenom = new ArrayList<>(2);
		atomsDenom.add(new String[] {"?s", predicate, "?o"});
		double denominator = kb.countDistinct("?o", atomsDenom);
		return (double) (numberOfEntitiesNewRanking + 1) / (denominator + 1);
	}

	private List<String[]> getQueryForConditionalRankingOfConstantsSize(String predicate, 
			List<String> otherPredicates) {
		List<String[]> atoms = new ArrayList<>();
		int variable = 0;
		atoms.add(new String[] {"?x" + variable, predicate, "?o"});
		
		for (String otherPredicate : otherPredicates) {
			atoms.add(new String[] {"?x" + (variable + 1), otherPredicate, "?x" + variable});
		}
		
		return atoms;
	}

	public double getComplexity(String constant) {
		double degree = kb.countDistinctPO(new String[]{constant, "", ""}) + 1;
		return this.defaultConstantsSlope * Math.log(degree) / Math.log(2) + this.defaultConstantsIntercept;
	}

}

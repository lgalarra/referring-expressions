package fr.inria.res.complexity;

public enum ComplexityFunction {
	STANDARD, SHORTEST, ENTROPY_BASED, DEGREE_BASED, DEGREE_BASED_EXACT;
}

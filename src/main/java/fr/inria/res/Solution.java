package fr.inria.res;

import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Collections;

public class Solution {
	
	private List<Condition> elements;
	
	private List<Integer> indexes;
	
	private double complexity;
	
	
	public Solution() {
		resetSolution();
	}
	
	public boolean update(Collection<Condition> elements) {
		double complexity = 0.0;
		for (Condition c : elements) {
			complexity += c.getComplexity();
		}
			
		if (complexity < getComplexity()) {
			this.elements = new ArrayList<>(elements);
			this.complexity = complexity;
			return true;
		}
		
		return false;
	}
	
	/**
	 * A set of solution indexes of equal complexity dominates a second set of indexes 
	 * if the first one is towards the left and above the second one in the search space.
	 */
	private boolean dominates(Collection<Integer> solution1, Collection<Integer> solution2) {
		if (solution1.size() < solution2.size())
			return true;
		
		// If the first set is of larger size, look at the indexes
		int maxCol1 = Collections.max(solution1);
		int maxCol2 = Collections.max(solution2);
		if (maxCol1 > maxCol2) {
			return true;
		} else if (maxCol1 == maxCol2) {
			// Look at the most complex condition
			int minCol1 = Collections.min(solution1);
			int minCol2 = Collections.min(solution2);
			return minCol1 > minCol2;
		} else {
			return false;
		}
	}

	public boolean update(Collection<Condition> elements, 
			Collection<Integer> elementIndexes) {
		double complexity = 0.0;
		for (Condition c : elements) {
			complexity += c.getComplexity();
		}
			
		if (complexity < getComplexity()
				|| (complexity == getComplexity() && dominates(getElementIndexes(), elementIndexes))
				) {
			this.elements = new ArrayList<>(elements);
			this.indexes = new ArrayList<>(elementIndexes);
			this.complexity = complexity;
			return true;
		}			
		
		return false;
	}
	
	public boolean isTrivialSolution() {
		return elements.isEmpty();
	}
	
	public double getComplexity() {
		return this.complexity;
	}
	
	public List<Condition> getElements() {
		return this.elements;
	}
	
	public List<Integer> getElementIndexes() {
		return this.indexes;
	}
	
	public void resetSolution() {
		this.elements = Collections.emptyList();
		this.indexes = Collections.emptyList();
		this.complexity = Double.MAX_VALUE;
	}
	
	@Override
	public String toString() {
		return this.elements.toString() + ", complexity: " + this.complexity;
	}

	public int getSize() {
		return this.elements.size();
	}
}

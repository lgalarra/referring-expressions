package fr.inria.res.experimental;

import java.util.ArrayList;
import java.util.List;

import fr.inria.res.Condition;

public class REMIRuntimeInfo {
	public long intersectRuntimeMs = 0l;
	
	public long totalRuntimeMs = 0l;
	
	public long sortingRuntimeMs = 0l;
	
	public long miningRuntimeMs = 0l;
	
	public int sizePriorityQueue = 0;
	
	public List<Condition> topConditions = new ArrayList<>();

}

package fr.inria.res.experimental;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import fr.inria.res.Condition;
import fr.inria.res.GlobalConfig;
import fr.inria.res.REMiner;
import fr.inria.res.complexity.ComplexityFunction;
import fr.inria.res.data.HDTKB;
import fr.inria.res.data.KB;

public class REMinerMeasureRuntime {
	
	public static void printHeader(boolean withRanking) {
		if (withRanking)
			System.out.println("Id\tTimestamp\tSequential\tConfiguration\tNumber of entities\tEntities\tSolution\t"
					+ "There was a solution\tIntersection time\tSorting time\tMining time\t"
					+ "Total time\tSize priority queue\tTimeout\tTop subgraphs");
		else
			System.out.println("Id\tTimestamp\tSequential\tConfiguration\tNumber of entities\tEntities\tSolution\t"
				+ "There was a solution\tIntersection time\tSorting time\tMining time\t"
				+ "Total time\tSize priority queue\tTimeout");
		
	}
	
	public static void main(String[] args) throws IOException {
		GlobalConfig.setKbPath(args[0], args[1]);
		KB kb = new HDTKB(GlobalConfig.getKbPath());
		GlobalConfig.setConcurrency(Runtime.getRuntime().availableProcessors());
		String mode = "";

		REMiner remi2 = new REMiner(kb, false, true, true, true, ComplexityFunction.DEGREE_BASED);		
		REMiner remi2P = new REMiner(kb, false, true, true, true, ComplexityFunction.DEGREE_BASED);
		REMiner remi2B = new REMiner(kb, false, false, false, true, ComplexityFunction.DEGREE_BASED);		
		REMiner remi2BP = new REMiner(kb, false, false, false, true, ComplexityFunction.DEGREE_BASED);	
		
		REMiner remi2Ext2 = new REMiner(kb, false, true, false, true, ComplexityFunction.DEGREE_BASED);
		REMiner remi2Ext2P = new REMiner(kb, false, true, false, true, ComplexityFunction.DEGREE_BASED);

		remi2P.setNumberOfProcessors(Runtime.getRuntime().availableProcessors());
		remi2B.setNumberOfProcessors(Runtime.getRuntime().availableProcessors());
		remi2Ext2P.setNumberOfProcessors(Runtime.getRuntime().availableProcessors());
		
		REMiner remi2PMT = new REMiner(kb, false, true, true, true, ComplexityFunction.DEGREE_BASED);
		remi2PMT.setScalabilityMode(true);
		
		if (args.length > 4) {
			mode = args[4];
		}
		System.err.println("Mode = " + mode);
		List<String> lines = Files.readAllLines(Paths.get(args[2]), Charset.forName("utf-8"));		

		int id;
		ExecutorService executor = Executors.newCachedThreadPool();

		if (mode.equals("sm")) {
			printHeader(false);
			id = 1;
			for (String line : lines) {
				final REMIRuntimeInfo infoP = new REMIRuntimeInfo();
	
				List<String> entities = Arrays.asList(line.split(" "));
				
				//ExecutorService ex = Executors.
				Future<List<Condition>> future = executor.submit(new Callable<List<Condition>>() {
	
				     public List<Condition> call() throws Exception {
				    	 return remi2PMT.mine(entities, infoP);
				     }
				 });
				
				List<Condition> cP = null;
				boolean timeoutP = false;
				Date timeStamp = new Date();

				try {
					cP = future.get(args.length > 3 ? Integer.parseInt(args[3]) : 120, TimeUnit.MINUTES);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				} catch (TimeoutException e) {
					timeoutP = true;
				} finally {
					future.cancel(true);
				}
				
				System.out.println(id + "\t" + (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(timeStamp)) + "\tParallel\tExt. size 3\t" + entities.size() + "\t" + entities + "\t" + cP + "\t" +
						 + (cP == null || cP.isEmpty() ? 0 : 1) + "\t" + infoP.intersectRuntimeMs + "\t" + infoP.sortingRuntimeMs + "\t" + 
								infoP.miningRuntimeMs + "\t" + infoP.totalRuntimeMs + "\t" + infoP.sizePriorityQueue + "\t"
								+ timeoutP);
				++id;
			}
			
		}
		
		if (mode.equals("") || mode.contains("premi-ext-pr-3a")) {
			GlobalConfig.setKbPath(args[0], args[1], true);
			REMiner remi2PPR = new REMiner(kb, false, true, true, true, ComplexityFunction.DEGREE_BASED);
			printHeader(true);
			id = 1;
			for (String line : lines) {
				final REMIRuntimeInfo infoP = new REMIRuntimeInfo();
	
				List<String> entities = Arrays.asList(line.split(" "));
				
				//ExecutorService ex = Executors.
				Future<List<Condition>> future = executor.submit(new Callable<List<Condition>>() {
	
				     public List<Condition> call() throws Exception {
				    	 return remi2PPR.mine(entities, infoP);
				     }
				 });
				
				List<Condition> cP = null;
				boolean timeoutP = false;
				Date timeStamp = new Date();

				try {
					cP = future.get(args.length > 3 ? Integer.parseInt(args[3]) : 120, TimeUnit.MINUTES);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				} catch (TimeoutException e) {
					timeoutP = true;
				} finally {
					future.cancel(true);
				}
				
				System.out.println(id + "\t" + (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(timeStamp)) + "\tParallel\tExt. size 3 PR\t" + entities.size() + "\t" + entities + "\t" + cP + "\t" +
						 + (cP == null || cP.isEmpty() ? 0 : 1) + "\t" + infoP.intersectRuntimeMs + "\t" + infoP.sortingRuntimeMs + "\t" + 
								infoP.miningRuntimeMs + "\t" + infoP.totalRuntimeMs + "\t" + infoP.sizePriorityQueue + "\t"
								+ timeoutP + "\t" + infoP.topConditions);
				++id;
			}
		}
		
		if (mode.equals("") || mode.contains("premi-ext-3a")) {
			printHeader(true);
			id = 1;
			for (String line : lines) {
				final REMIRuntimeInfo infoP = new REMIRuntimeInfo();
	
				List<String> entities = Arrays.asList(line.split(" "));
				
				//ExecutorService ex = Executors.
				Future<List<Condition>> future = executor.submit(new Callable<List<Condition>>() {
	
				     public List<Condition> call() throws Exception {
				    	 return remi2P.mine(entities, infoP);
				     }
				 });
				
				List<Condition> cP = null;
				boolean timeoutP = false;
				Date timeStamp = new Date();

				try {
					cP = future.get(args.length > 3 ? Integer.parseInt(args[3]) : 120, TimeUnit.MINUTES);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				} catch (TimeoutException e) {
					timeoutP = true;
				} finally {
					future.cancel(true);
				}
				
				System.out.println(id + "\t" + (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(timeStamp)) + "\tParallel\tExt. size 3\t" + entities.size() + "\t" + entities + "\t" + cP + "\t" +
						 + (cP == null || cP.isEmpty() ? 0 : 1) + "\t" + infoP.intersectRuntimeMs + "\t" + infoP.sortingRuntimeMs + "\t" + 
								infoP.miningRuntimeMs + "\t" + infoP.totalRuntimeMs + "\t" + infoP.sizePriorityQueue + "\t"
								+ timeoutP + "\t" + infoP.topConditions);
				++id;
			}
		}
		
		if (mode.equals("") || mode.contains("premi-ext-2a")) {
			printHeader(false);			
			id = 1;
			for (String line : lines) {
				final REMIRuntimeInfo infoP = new REMIRuntimeInfo();
	
				List<String> entities = Arrays.asList(line.split(" "));
				
				//ExecutorService ex = Executors.
				Future<List<Condition>> future = executor.submit(new Callable<List<Condition>>() {
	
				     public List<Condition> call() throws Exception {
				    	 return remi2Ext2P.mine(entities, infoP);
				     }
				 });
				
				List<Condition> cP = null;
				boolean timeoutP = false;
				Date timeStamp = new Date();

				try {
					cP = future.get(args.length > 3 ? Integer.parseInt(args[3]) : 120, TimeUnit.MINUTES);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				} catch (TimeoutException e) {
					timeoutP = true;
				} finally {
					future.cancel(true);
				}
				
				System.out.println(id + "\t" + (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(timeStamp)) + "\tParallel\tExt. size 2\t" + entities.size() + "\t" + entities + "\t" + cP + "\t" +
						 + (cP == null || cP.isEmpty() ? 0 : 1) + "\t" + infoP.intersectRuntimeMs + "\t" + infoP.sortingRuntimeMs + "\t" + 
								infoP.miningRuntimeMs + "\t" + infoP.totalRuntimeMs + "\t" + infoP.sizePriorityQueue + "\t"
								+ timeoutP);
				++id;
			}
		}
		
		if (mode.equals("") || mode.contains("sremi-ext-3a")) {
			printHeader(false);			
			id = 1;
			executor = Executors.newCachedThreadPool();
			for (String line : lines) {
				final REMIRuntimeInfo infoS = new REMIRuntimeInfo();
				List<String> entities = Arrays.asList(line.split(" "));
				
				Future<List<Condition>> future = executor.submit(new Callable<List<Condition>>() {
	
				     public List<Condition> call() throws Exception {
				    	 return remi2.mine(entities, infoS);
				     }
				 });
				
				boolean timeoutS = false;
				List<Condition> cS = null;
				Date timeStamp = new Date();
				
				try {
					cS = future.get(args.length > 3 ? Integer.parseInt(args[3]) : 120, TimeUnit.MINUTES);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				} catch (TimeoutException e) {
					timeoutS = true;
				} finally {
					future.cancel(true);
				}
				
				System.out.println(id + "\t" + (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(timeStamp)) + "\tSequential\tExt. size 3\t" + entities.size() + "\t" + entities + "\t" + cS + "\t" +
						 + (cS == null || cS.isEmpty() ? 0 : 1) + "\t" + infoS.intersectRuntimeMs + "\t" + infoS.sortingRuntimeMs + "\t" + 
								infoS.miningRuntimeMs + "\t" + infoS.totalRuntimeMs + "\t" + infoS.sizePriorityQueue + "\t" +
								 timeoutS);
				
				++id;
			}
		}
		
		if (mode.equals("") || mode.contains("sremi-noext")) {
			printHeader(false);			
			id = 1;		
			for (String line : lines) {
				final REMIRuntimeInfo infoBP = new REMIRuntimeInfo();
	
				List<String> entities = Arrays.asList(line.split(" "));
				Future<List<Condition>> future = executor.submit(new Callable<List<Condition>>() {
	
					public List<Condition> call() throws Exception {
				    	return remi2BP.mine(entities, infoBP);
					}
				});
				
				boolean timeoutBP = false;
				List<Condition> cBP = null;
				Date timeStamp = new Date();
				try {
					cBP = future.get(args.length > 3 ? Integer.parseInt(args[3]) : 480, TimeUnit.MINUTES);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				} catch (TimeoutException e) {
					timeoutBP = true;
				} finally {
					future.cancel(true);
				}
				
				System.out.println(id + "\t" + (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(timeStamp)) + "\tSequential\tNo ext.\t" + entities.size() + "\t" + entities + "\t" + cBP + "\t" +
						 + (cBP == null || cBP.isEmpty() ? 0 : 1) + "\t" + infoBP.intersectRuntimeMs + "\t" + infoBP.sortingRuntimeMs + "\t" + 
								infoBP.miningRuntimeMs + "\t" + infoBP.totalRuntimeMs + "\t" + infoBP.sizePriorityQueue + "\t" 
								+ timeoutBP);
				++id;
			}
		}
		
		
		if (mode.equals("") || mode.contains("premi-noext")) {
			printHeader(true);			
			id = 1;
			for (String line : lines) {
				final REMIRuntimeInfo infoB = new REMIRuntimeInfo();
	
				List<String> entities = Arrays.asList(line.split(" "));
				Future<List<Condition>> future = executor.submit(new Callable<List<Condition>>() {
	
					public List<Condition> call() throws Exception {
				    	return remi2B.mine(entities, infoB);
					}
				});
				
				boolean timeoutB = false;
				List<Condition> cB = null;
				Date timeStamp = new Date();
				try {
					cB = future.get(args.length > 3 ? Integer.parseInt(args[3]) : 480, TimeUnit.MINUTES);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				} catch (TimeoutException e) {
					timeoutB = true;
				} finally {
					future.cancel(true);
				}
				
				System.out.println(id + "\t" + (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(timeStamp)) + "\tParallel\tNo ext.\t" + entities.size() + "\t" + entities + "\t" + cB + "\t" +
						 + (cB == null || cB.isEmpty() ? 0 : 1) + "\t" + infoB.intersectRuntimeMs + "\t" + infoB.sortingRuntimeMs + "\t" + 
								infoB.miningRuntimeMs + "\t" + infoB.totalRuntimeMs + "\t" + infoB.sizePriorityQueue + "\t" 
								 + timeoutB + "\t" + infoB.topConditions);
				++id;
			}
		}		
		
		if (mode.equals("") || mode.contains("premi-pr-noext")) {
			printHeader(true);			
			GlobalConfig.setKbPath(args[0], args[1], true);
			id = 1;
			for (String line : lines) {
				final REMIRuntimeInfo infoB = new REMIRuntimeInfo();
	
				List<String> entities = Arrays.asList(line.split(" "));
				Future<List<Condition>> future = executor.submit(new Callable<List<Condition>>() {
	
					public List<Condition> call() throws Exception {
				    	return remi2B.mine(entities, infoB);
					}
				});
				
				boolean timeoutB = false;
				List<Condition> cB = null;
				Date timeStamp = new Date();
				try {
					cB = future.get(args.length > 3 ? Integer.parseInt(args[3]) : 480, TimeUnit.MINUTES);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				} catch (TimeoutException e) {
					timeoutB = true;
				} finally {
					future.cancel(true);
				}
				
				System.out.println(id + "\t" + (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(timeStamp)) + "\tParallel\tNo ext. PR\t" + entities.size() + "\t" + entities + "\t" + cB + "\t" +
						 + (cB == null || cB.isEmpty() ? 0 : 1) + "\t" + infoB.intersectRuntimeMs + "\t" + infoB.sortingRuntimeMs + "\t" + 
								infoB.miningRuntimeMs + "\t" + infoB.totalRuntimeMs + "\t" + infoB.sizePriorityQueue + "\t" 
								 + timeoutB + "\t" + infoB.topConditions);
				++id;
			}
		}
		
		
		if (mode.equals("") || mode.contains("sremi-ext-2a")) {
			printHeader(false);
			id = 1;
			for (String line : lines) {
				final REMIRuntimeInfo infoP = new REMIRuntimeInfo();
	
				List<String> entities = Arrays.asList(line.split(" "));
				
				//ExecutorService ex = Executors.
				Future<List<Condition>> future = executor.submit(new Callable<List<Condition>>() {
	
				     public List<Condition> call() throws Exception {
				    	 return remi2Ext2.mine(entities, infoP);
				     }
				 });
				
				List<Condition> cP = null;
				boolean timeoutP = false;
				Date timeStamp = new Date();

				try {
					cP = future.get(args.length > 3 ? Integer.parseInt(args[3]) : 120, TimeUnit.MINUTES);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				} catch (TimeoutException e) {
					timeoutP = true;
				} finally {
					future.cancel(true);
				}
				
				System.out.println(id + "\t" + (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(timeStamp)) + "\tSequential\tExt. size 2\t" + entities.size() + "\t" + entities + "\t" + cP + "\t" +
						 + (cP == null || cP.isEmpty() ? 0 : 1) + "\t" + infoP.intersectRuntimeMs + "\t" + infoP.sortingRuntimeMs + "\t" + 
								infoP.miningRuntimeMs + "\t" + infoP.totalRuntimeMs + "\t" + infoP.sizePriorityQueue + "\t"
								+ timeoutP);
				++id;
			}
		}
		
		executor.shutdownNow();
	}

}

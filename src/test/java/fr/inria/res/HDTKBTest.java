package fr.inria.res;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.junit.jupiter.api.Test;

import fr.inria.res.data.HDTKB;
import fr.inria.res.data.InMemoryKB;
import fr.inria.res.data.KB;
import javatools.datatypes.ByteString;

class HDTKBTest {

	@Test
	void test() {
		KB kb = null;
		try {
			kb = new HDTKB("src/data/dbpedia/dbpedia.hdt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Map<String, Set<String>> answers = kb.selectDistinctPO(new String[] 
				{"http://dbpedia.org/resource/Barack_Obama", 
				"", ""});
		
		assertFalse(answers.isEmpty());
		System.out.println(answers);
	}
	
	@Test
	void testJenaIntegration() {
		KB kb = null;
		try {
			kb = new HDTKB("src/data/dbpedia/dbpedia.hdt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		}
		
		Model model = ((HDTKB)kb).getDataSource();
		String queryString = "SELECT * WHERE { <http://dbpedia.org/resource/Barack_Obama> ?p ?o . }";
	    Query query = QueryFactory.create(queryString) ;
	    try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
	        ResultSet results = qexec.execSelect() ;
	        for ( ; results.hasNext() ; ) {
	        	QuerySolution soln = results.nextSolution() ;
	        	System.out.println(soln.get("p") + " -- " + soln.get("o")) ;
	        }
	    }
	}
	
	@Test
	void testSimpleCondition1() {
		KB kb = null;
		try {
			kb = new HDTKB("src/data/test/test.hdt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		}
	    
		Condition condition = Condition.makeCondition("http://example.org/graduatedFrom", "http://example.org/INSA");
		Set<String> results = kb.selectDistinct(condition);
		System.out.println(results);
		assertTrue(results.contains("http://example.org/Mael"));
		assertEquals(1, results.size());
	}
	
	@Test
	public void testPathCondition() {
		KB kb = null;
		try {
			kb = new HDTKB("src/data/test/test.hdt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		}
		
		Condition condition = Condition.makeCondition("http://example.org/worksAt", "http://example.org/IRISA");
		Atom newAtom = new Atom("http://example.org/IRISA", "http://example.org/headquarteredIn", "http://example.org/Rennes", null);
		Condition pathCondition = condition.chainAtomSO(newAtom, condition.getSetAtoms().iterator().next());
		Set<String> results = kb.selectDistinct(pathCondition);
		for (ByteString[] atom : InMemoryKB.condition2Query(pathCondition, "?x0")) {
			System.out.println(Arrays.toString(atom));
		}
		System.out.println("Query for testPathCondition");		
		assertTrue(results.contains("http://example.org/Mael"));
		assertTrue(results.contains("http://example.org/Luis"));		
		assertTrue(results.contains("http://example.org/Julien"));
		assertTrue(results.contains("http://example.org/Colin"));
		assertTrue(results.contains("http://example.org/Kevin"));		
		assertEquals(5, results.size());	
	}

	@Test
	public void selectDistinctForPathTest() {
		KB kb = null;
		try {
			kb = new HDTKB("src/data/test/test.hdt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		}
		
		Condition condition = Condition.makeCondition("http://example.org/worksAt", "http://example.org/Energiency");
		Atom newAtom1 = new Atom("http://example.org/Energiency", "http://example.org/type", "http://example.org/Company", null);
		Atom newAtom2 = new Atom("http://example.org/Energiency", "http://example.org/type", "http://example.org/Startup", null);
		Condition pathCondition = condition.addAtoms(Arrays.asList(newAtom1, newAtom2), 
				condition.getSetAtoms().iterator().next());

		for (ByteString[] atom : InMemoryKB.condition2Query(pathCondition, "?x0")) {
			System.out.println(Arrays.toString(atom));
		}
		Set<String> results = kb.selectDistinct(pathCondition);
		assertTrue(results.contains("http://example.org/Mael"));
		assertEquals(1, results.size());
		
	}
	
	@Test
	public void selectDistinctForStarTest() {
		KB kb = null;
		try {
			kb = new HDTKB("src/data/test/test.hdt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		}
		
		Condition condition = Condition.makeCondition("http://example.org/worksAt", "http://example.org/Energiency");
		Atom newAtom1 = new Atom("http://example.org/Energiency", "http://example.org/type", "http://example.org/Company", null);
		Atom newAtom2 = new Atom("http://example.org/Energiency", "http://example.org/type", "http://example.org/Startup", null);
		Condition pathCondition = condition.addAtom(newAtom1, newAtom2, 
				condition.getSetAtoms().iterator().next());
		System.out.println("Query for selectDistinctForStarTest");
		for (ByteString[] atom : InMemoryKB.condition2Query(pathCondition, "?x0")) {
			System.out.println(Arrays.toString(atom));
		}
		Set<String> results = kb.selectDistinct(pathCondition);
		assertTrue(results.contains("http://example.org/Mael"));
		assertEquals(1, results.size());
	}

}

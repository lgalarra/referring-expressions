package fr.inria.res;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.junit.jupiter.api.Test;

import fr.inria.res.data.InMemoryKB;

/**
 * Unit test for simple App.
 */
public class RETest {
	
	
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
	@Test
    void RETestIntersection() {
    	InMemoryKB kb = new InMemoryKB();
    	kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<France>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<GhostIntern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Julien>", "<livesIn>", "<Rennes>");
    	kb.add("<Luis>", "<livesIn>", "<Rennes>");
    	kb.add("<Gregory>", "<livesIn>", "<Rennes>");
    	kb.add("<Yann>", "<livesIn>", "<Rennes>");

    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Travel>");
    	kb.add("<Gregory>", "<likes>", "<Read>");
    	kb.add("<Gregory>", "<likes>", "<Cooking>");
    	kb.add("<Yann>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	REMiner reMiner = new REMiner(kb);
    	Set<Condition> intersection = reMiner.intersectEntities(Arrays.asList("<Julien>", "<Gregory>", "<Yann>"));
    	
    	assertTrue(intersection.contains(Condition.makeCondition("<citizenOf>", "<France>")));
    	assertFalse(intersection.contains(Condition.makeCondition("<citizenOf>", "<Ecuador>")));
    	assertTrue(intersection.contains(Condition.makeCondition("<livesIn>", "<Rennes>")));
    	assertFalse(intersection.contains(Condition.makeCondition("<profession>", "<Intern>")));    	
    	assertTrue(intersection.contains(Condition.makeCondition("<likes>", "<Cooking>")));
    }
	
	
	@Test
    void RETestCompareListEntities() {
		String cond1 = "<Julien>";
		String cond2 = "<Luis>";
		String cond3 = "<Elisa>";
		String cond4 = "<Yann>";
		String cond5 = "<Gregory>";
		String cond6 = "<Colin>";
		
		InMemoryKB kb = new InMemoryKB();
		REMiner re = new REMiner(kb);
		
		Set<String> setEntities1 = new LinkedHashSet<>();
		Set<String> setEntities2 = new LinkedHashSet<>();
		
		
		setEntities1.add(cond1);
		setEntities1.add(cond2);
		setEntities2.add(cond1);
		setEntities2.add(cond2);
		assert(SetRelationCase.EQUALITY == re.compareCollectionEntities(setEntities1, setEntities2));
		setEntities1.add(cond3);
		assert(setEntities1.containsAll(setEntities2) == true);
		assert(re.compareCollectionEntities(setEntities1, setEntities2) == SetRelationCase.SUPERSET);
		setEntities1.add(cond4);
		assert(re.compareCollectionEntities(setEntities1, setEntities2) == SetRelationCase.SUPERSET);
		setEntities2.add(cond3);
		setEntities2.add(cond4);
		assert(re.compareCollectionEntities(setEntities1, setEntities2) == SetRelationCase.EQUALITY);
		setEntities2.add(cond5);
		setEntities2.add(cond6);
		assert(re.compareCollectionEntities(setEntities1, setEntities2) == SetRelationCase.SUBSET);
	}

	
	/**
	 * return the map of the Condition for the entities "Julien, Gregory and Yann"
	 */
	@Test
	void RETestResultOfKb() {
		InMemoryKB kb = new InMemoryKB();
		REMiner reMiner = new REMiner(kb);
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<France>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<GhostIntern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Julien>", "<livesIn>", "<Rennes>");
    	kb.add("<Luis>", "<livesIn>", "<Rennes>");
    	kb.add("<Gregory>", "<livesIn>", "<Rennes>");
    	kb.add("<Yann>", "<livesIn>", "<Rennes>");

    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Travel>");
    	kb.add("<Gregory>", "<likes>", "<Read>");
    	kb.add("<Gregory>", "<likes>", "<Cooking>");
    	kb.add("<Yann>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	kb.add("<Julien>", "<workPlace>", "<Inria>");
    	
    	kb.add("<Luis>", "<workWith>", "<Gregory>");
    	
    	List<String> entities2Describe = Arrays.asList("<Julien>", "<Gregory>", "<Yann>");
    	Set<Condition> intersection = reMiner.intersectEntities(entities2Describe);
    	
    	Set<String> object = new LinkedHashSet<>();
    	Set<String> object1 = new LinkedHashSet<>();
    	Set<String> object2 = new LinkedHashSet<>();
    	Set<String> object3 = new LinkedHashSet<>();
    	Set<String> object4 = new LinkedHashSet<>();
    	object.add("<Cooking>");
    	object1.add("<Rennes>");
    	object2.add("<France>");
    	object3.add("<Inria>");
    	object4.add("<Travel>");
    	
    	
    	Map<String, Set<String>> map = reMiner.resultOfKb(kb, intersection, entities2Describe);
    	assertTrue(map.keySet().contains("<citizenOf>"));
    	assertTrue(map.keySet().contains("<profession>"));
    	assertTrue(map.keySet().contains("<livesIn>"));
    	assertTrue(map.keySet().contains("<likes>"));
    	assertTrue(map.keySet().contains("<workPlace>"));
    	assertFalse(map.keySet().contains("<workWith>"));    	
    	assertTrue(map.values().contains(object));
    	assertTrue(map.values().contains(object1));
    	assertTrue(map.values().contains(object2));
    	assertTrue(map.values().contains(object3));
    	assertFalse(map.values().contains(object4));

	}
	
	
	/**
	 * Return the entities who "likes Cooking" and "livesIn Rennes"
	 */
	@Test
    void RETestMakeExpression() {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<France>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<GhostIntern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Julien>", "<livesIn>", "<Rennes>");
    	kb.add("<Luis>", "<livesIn>", "<Rennes>");
    	kb.add("<Gregory>", "<livesIn>", "<Rennes>");
    	kb.add("<Yann>", "<livesIn>", "<Rennes>");

    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Travel>");
    	kb.add("<Gregory>", "<likes>", "<Read>");
    	kb.add("<Gregory>", "<likes>", "<Cooking>");
    	kb.add("<Yann>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	REMiner reMiner = new REMiner(kb);
    	
    	List<String> entities2Describe = new ArrayList<>();
    	entities2Describe.add("<Julien>");
    	entities2Describe.add("<Luis>");
    	entities2Describe.add("<Yann>");
    	List<String> entitiesCoveredUntilNow = new ArrayList<>(entities2Describe);
    	entitiesCoveredUntilNow.add("<Gregory>");
    	
    	Stack<Condition> stack = new Stack<>();
    	Condition cond = new Condition("<livesIn>", "<Rennes>");
    	Condition cond2 = new Condition("<likes>", "<Cooking>");
    	stack.add(cond);
    	stack.add(cond2);
		

    	
		List<String> coveredObject = reMiner.getCoveredEntities(kb, stack);

		
		assertTrue(coveredObject.contains("<Julien>"));
		assertTrue(coveredObject.contains("<Yann>"));
		assertTrue(coveredObject.contains("<Gregory>"));
		assertFalse(coveredObject.contains("<Luis>"));
		
	}
	
	
	/**
	 * return the different entities who "livesIn Rennes"
	 */
	@Test
	void RETestMakeQueryForCondition() {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<France>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<GhostIntern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Julien>", "<livesIn>", "<Rennes>");
    	kb.add("<Luis>", "<livesIn>", "<Rennes>");
    	kb.add("<Gregory>", "<livesIn>", "<Rennes>");
    	kb.add("<Yann>", "<livesIn>", "<Rennes>");

    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Travel>");
    	kb.add("<Gregory>", "<likes>", "<Read>");
    	kb.add("<Gregory>", "<likes>", "<Cooking>");
    	kb.add("<Yann>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	REMiner reMiner = new REMiner(kb);
    	
    	List<String> entities2Describe = new ArrayList<>();
    	entities2Describe.add("<Julien>");
    	entities2Describe.add("<Luis>");
    	entities2Describe.add("<Yann>");
    	
    	Condition cond = new Condition("<livesIn>", "<Rennes>");
    	Set<String> makeQueryForCondition = reMiner.getEntitiesForSimpleCondition(kb, cond);
    	
    	assertTrue(makeQueryForCondition.containsAll(entities2Describe));
    	assertTrue(makeQueryForCondition.contains("<Gregory>"));
    	
	}
	
	/**
	 * return the different entities who "likes Cooking"
	 */
	@Test
	void RETestMakeQueryForCondition2() {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<France>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<GhostIntern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Julien>", "<livesIn>", "<Rennes>");
    	kb.add("<Luis>", "<livesIn>", "<Rennes>");
    	kb.add("<Gregory>", "<livesIn>", "<Rennes>");
    	kb.add("<Yann>", "<livesIn>", "<Rennes>");

    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Travel>");
    	kb.add("<Gregory>", "<likes>", "<Read>");
    	kb.add("<Gregory>", "<likes>", "<Cooking>");
    	kb.add("<Yann>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	REMiner reMiner = new REMiner(kb);
    	
    	Condition cond2 = new Condition("<likes>", "<Cooking>");
    	Set<String> makeQueryForCondition2 = reMiner.getEntitiesForSimpleCondition(kb, cond2);
    	
    	assertFalse(makeQueryForCondition2.contains("<Luis>"));
    	assertTrue(makeQueryForCondition2.contains("<Julien>"));
    	assertTrue(makeQueryForCondition2.contains("<Yann>"));
    	
	}
	
	
	/**
	 * Julien, Luis and Yann "livesIn Rennes" is the only common point 
	 */
	@Test
	void RETestMine() {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<France>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<GhostIntern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Julien>", "<livesIn>", "<Rennes>");
    	kb.add("<Luis>", "<livesIn>", "<Rennes>");
    	kb.add("<Gregory>", "<livesIn>", "<Rennes>");
    	kb.add("<Yann>", "<livesIn>", "<Rennes>");

    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Travel>");
    	kb.add("<Gregory>", "<likes>", "<Read>");
    	kb.add("<Gregory>", "<likes>", "<Cooking>");
    	kb.add("<Yann>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	REMiner reMiner = new REMiner(kb);
    	
    	List<String> entities2Describe = new ArrayList<>();
    	entities2Describe.add("<Julien>");
    	entities2Describe.add("<Luis>");
    	entities2Describe.add("<Yann>");
    	entities2Describe.add("<Gregory>");
    	
    	Condition cond = new Condition("<livesIn>", "<Rennes>");
    	Condition cond2 = new Condition("<likes>", "<Cooking>");
    	Condition cond3 = new Condition("<likes>", "<Read>");
    	Condition cond4 = new Condition("<profession>", "<Intern>");
    	Set<Condition> setCond = new LinkedHashSet<>();
    	setCond.add(cond);
    	setCond.add(cond2);
    	setCond.add(cond3);
    	setCond.add(cond4);
    	
    	
    	List<Condition> listCond = reMiner.mine(entities2Describe, setCond);
    	
    	assertTrue(listCond.contains(cond));
    	assertFalse(listCond.contains(cond2));
    	assertFalse(listCond.contains(cond3));
    	assertFalse(listCond.contains(cond4));
    	
	}
	
	
	/**
	 * Julien, Luis and Yann are the only to "likes Cooking"
	 */
	@Test
	void RETestMine2() {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<France>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<GhostIntern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Julien>", "<livesIn>", "<Rennes>");
    	kb.add("<Luis>", "<livesIn>", "<Rennes>");
    	kb.add("<Gregory>", "<livesIn>", "<Rennes>");
    	kb.add("<Yann>", "<livesIn>", "<Rennes>");

    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Travel>");
    	kb.add("<Luis>", "<likes>", "<Cooking>");
    	kb.add("<Gregory>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	REMiner reMiner = new REMiner(kb);
    	
    	List<String> entities2Describe = new ArrayList<>();
    	entities2Describe.add("<Julien>");
    	entities2Describe.add("<Luis>");
    	entities2Describe.add("<Yann>");
    	
    	Condition cond = new Condition("<livesIn>", "<Rennes>");
    	Condition cond2 = new Condition("<likes>", "<Cooking>");
    	Condition cond3 = new Condition("<profession>", "<Intern>");
    	Set<Condition> setCond = new LinkedHashSet<>();
    	setCond.add(cond);
    	setCond.add(cond2);
    	setCond.add(cond3);
    	
    	
    	List<Condition> listCond = reMiner.mine(entities2Describe, setCond);
    	
    	assertFalse(listCond.contains(cond));
    	assertTrue(listCond.contains(cond2));
    	assertFalse(listCond.contains(cond3));
    	
	}
	
	
	/**
	 * Julien must be "citizenOf France" and "profession Intern" because Gregory and Yann are each one of this Condition
	 * @throws InterruptedException 
	 */
	@Test
	void REComplexTestResultOfKbExpression() throws InterruptedException{
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<Intern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	List<String> entities2Describe = new ArrayList<>();
    	entities2Describe.add("<Julien>");
    	
    	REMiner reMiner = new REMiner(kb);
    	List<Condition> re = reMiner.mine(entities2Describe);
    	
    	assertTrue(re.size() == 2);
    	assertTrue(re.contains(new Condition("<citizenOf>", "<France>")));
    	assertTrue(re.contains(new Condition("<profession>", "<Intern>")));    	
	}
	
	
	/**
	 * Julien must be "citizenOf France" and "profession Intern" because Gregory and Yann are each one of this Condition
	 * @throws InterruptedException 
	 */
	@Test
	void REComplexTestResultOfKbExpression2() throws InterruptedException {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<Intern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Cooking>");
    	kb.add("<Gregory>", "<likes>", "<Cooking>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	List<String> entities2Describe = new ArrayList<>();
    	entities2Describe.add("<Julien>");
    	
    	REMiner reMiner = new REMiner(kb);
    	List<Condition> re = reMiner.mine(entities2Describe);
		
    	
    	assertTrue(re.size() == 2);
    	assertTrue(re.contains(new Condition("<citizenOf>", "<France>")));
    	assertTrue(re.contains(new Condition("<profession>", "<Intern>"))); 
    	assertFalse(re.contains(new Condition("<likes>", "<Cooking>")));
    	assertFalse(re.contains(new Condition("<citizenOf>", "<Ecuador>")));
	}
	
	
	/**
	 * Julien and Luis "likes Cooking" but Yann et Gregory too
	 * @throws InterruptedException 
	 */
	@Test
	void REComplexTestResultOfKbExpression3() throws InterruptedException {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<Intern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Cooking>");
    	kb.add("<Gregory>", "<likes>", "<Cooking>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	List<String> entities2Describe = new ArrayList<>();
    	entities2Describe.add("<Julien>");
    	entities2Describe.add("<Luis>");
    	
    	REMiner reMiner = new REMiner(kb);
    	List<Condition> re = reMiner.mine(entities2Describe);
    	
    	assertTrue(re.size() == 0);
    	assertFalse(re.contains(new Condition("<citizenOf>", "<France>")));
    	assertFalse(re.contains(new Condition("<profession>", "<Intern>"))); 
    	assertFalse(re.contains(new Condition("<likes>", "<Cooking>")));
    	assertFalse(re.contains(new Condition("<citizenOf>", "<Ecuador>")));
	}
	
	

	/**
	 * There is no solution because there is no common point to only these entities
	 * @throws InterruptedException 
	 */
	@Test
	void RETestAlgo() throws InterruptedException{
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<France>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<GhostIntern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Julien>", "<livesIn>", "<Rennes>");
    	kb.add("<Luis>", "<livesIn>", "<Rennes>");
    	kb.add("<Gregory>", "<livesIn>", "<Rennes>");
    	kb.add("<Yann>", "<livesIn>", "<Rennes>");

    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Travel>");
    	kb.add("<Luis>", "<likes>", "<Cooking>");
    	kb.add("<Gregory>", "<likes>", "<Read>");
    	kb.add("<Gregory>", "<likes>", "<Cooking>");
    	kb.add("<Yann>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	REMiner reMiner = new REMiner(kb);
    	
    	List<String> entities2Describe = new ArrayList<>();
    	entities2Describe.add("<Julien>");
    	entities2Describe.add("<Luis>");
    	entities2Describe.add("<Yann>");
    	
    	Condition cond = new Condition("<livesIn>", "<Rennes>");
    	Condition cond2 = new Condition("<likes>", "<Cooking>");
    	Condition cond3 = new Condition("<likes>", "<Read>");
    	Condition cond4 = new Condition("<profession>", "<Intern>");
 
    	
    	List<Condition> listCond = reMiner.mine(entities2Describe);
    	
    	assertFalse(listCond.contains(cond));
    	assertFalse(listCond.contains(cond2));
    	assertFalse(listCond.contains(cond3));
    	assertFalse(listCond.contains(cond4));
    	assertTrue(listCond.isEmpty());
    	
	}
	
	
	/**
	 * Condition "LivesIn Rennes" is solution because of the PriorityQueue sorted and it is the only common point
	 * @throws InterruptedException 
	 */
	@Test
	void RETestAlgo2() throws InterruptedException{
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<France>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<GhostIntern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Julien>", "<livesIn>", "<Rennes>");
    	kb.add("<Luis>", "<livesIn>", "<Rennes>");
    	kb.add("<Gregory>", "<livesIn>", "<Rennes>");
    	kb.add("<Yann>", "<livesIn>", "<Rennes>");

    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Travel>");
    	kb.add("<Luis>", "<likes>", "<Cooking>");
    	kb.add("<Gregory>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	REMiner reMiner = new REMiner(kb);
    	
    	List<String> entities2Describe = new ArrayList<>();
    	entities2Describe.add("<Julien>");
    	entities2Describe.add("<Luis>");
    	entities2Describe.add("<Yann>");
    	entities2Describe.add("<Gregory>");
    	
    	
    	Condition cond = new Condition("<livesIn>", "<Rennes>");
    	Condition cond2 = new Condition("<likes>", "<Cooking>");
    	Condition cond3 = new Condition("<likes>", "<Read>");
    	Condition cond4 = new Condition("<profession>", "<Intern>");
 
    	
    	List<Condition> listCond = reMiner.mine(entities2Describe);
    	
    	assertTrue(listCond.contains(cond));
    	assertFalse(listCond.contains(cond2));
    	assertFalse(listCond.contains(cond3));
    	assertFalse(listCond.contains(cond4));
    	
	}
	
	
	/**
	 * No Condition common to all the entities
	 * @throws InterruptedException 
	 */
	@Test
	void RETestAlgo3() throws InterruptedException{
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<France>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<GhostIntern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Luis>", "<livesIn>", "<Rennes>");
    	kb.add("<Gregory>", "<livesIn>", "<Rennes>");
    	kb.add("<Yann>", "<livesIn>", "<Rennes>");

    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Travel>");
    	kb.add("<Luis>", "<likes>", "<Cooking>");
    	kb.add("<Gregory>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	REMiner reMiner = new REMiner(kb);
    	
    	List<String> entities2Describe = new ArrayList<>();
    	entities2Describe.add("<Julien>");
    	entities2Describe.add("<Luis>");
    	entities2Describe.add("<Yann>");
    	entities2Describe.add("<Gregory>");
    	
    	
    	Condition cond = new Condition("<livesIn>", "<Rennes>");
    	Condition cond2 = new Condition("<likes>", "<Cooking>");
    	Condition cond3 = new Condition("<likes>", "<Read>");
    	Condition cond4 = new Condition("<profession>", "<Intern>");
 
    	
    	List<Condition> listCond = reMiner.mine(entities2Describe);
    	
    	assertFalse(listCond.contains(cond));
    	assertFalse(listCond.contains(cond2));
    	assertFalse(listCond.contains(cond3));
    	assertFalse(listCond.contains(cond4));
    	assertTrue(listCond.isEmpty());
	}
	
	
	/**
	 * Julien is the only entity, to difference him from the other 
	 * we must describe him as "profession Intern"
	 * @throws InterruptedException 
	 */
	@Test
	void RETestAlgo4() throws InterruptedException{
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<France>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<GhostIntern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Luis>", "<livesIn>", "<Rennes>");
    	kb.add("<Gregory>", "<livesIn>", "<Rennes>");
    	kb.add("<Yann>", "<livesIn>", "<Rennes>");

    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Travel>");
    	kb.add("<Luis>", "<likes>", "<Cooking>");
    	kb.add("<Gregory>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	REMiner reMiner = new REMiner(kb);
    	
    	List<String> entities2Describe = new ArrayList<>();
    	entities2Describe.add("<Julien>");    	
    	
    	Condition cond = new Condition("<citizenOf>", "<France>");
    	Condition cond2 = new Condition("<likes>", "<Cooking>");
    	Condition cond3 = new Condition("<likes>", "<Read>");
    	Condition cond4 = new Condition("<profession>", "<Intern>");
 
    	
    	List<Condition> listCond = reMiner.mine(entities2Describe);
    	
    	assertFalse(listCond.contains(cond));
    	assertFalse(listCond.contains(cond2));
    	assertFalse(listCond.contains(cond3));
    	assertTrue(listCond.contains(cond4));
    	assertFalse(listCond.isEmpty());
	}
	
	
	@Test
	void RETestRefine(){
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<France>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<GhostIntern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Luis>", "<livesIn>", "<Rennes>");
    	kb.add("<Gregory>", "<livesIn>", "<Rennes>");
    	kb.add("<Yann>", "<livesIn>", "<Rennes>");

    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Travel>");
    	kb.add("<Luis>", "<likes>", "<Cooking>");
    	kb.add("<Gregory>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	kb.add("<France>", "<hasCapital>", "<Paris>");
    	kb.add("<France>", "<likes>", "<Wine>");
    	kb.add("<France>", "<citizenAre>", "<French>");
    	
    	
    	REMiner reMiner = new REMiner(kb);
		Condition cond = new Condition("<livesIn>", "<France>");
		Condition cond1 = new Condition("<likes>", "<Wine>");
		Condition cond2 = new Condition("<citizenAre>", "<French>");
		
		Object firstObject = cond.getLastObject();
		assertTrue(firstObject.equals("<France>"));
		
		List<Condition> list = reMiner.refine(cond);
		assertTrue(list.size() == 3);
		assertTrue(list.contains(cond1));
		assertTrue(list.contains(cond2));
		
		
	}
	
	@Test
	void RETestExtendListOfConditions(){
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
    	kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
    	kb.add("<Gregory>", "<citizenOf>", "<France>");
    	kb.add("<Yann>", "<citizenOf>", "<France>");
    	
    	kb.add("<Julien>", "<profession>", "<Intern>");
    	kb.add("<Luis>", "<profession>", "<Researcher>");
    	kb.add("<Gregory>", "<profession>", "<GhostIntern>");
    	kb.add("<Yann>", "<profession>", "<PhDStudent>");
    	
    	kb.add("<Luis>", "<livesIn>", "<Rennes>");
    	kb.add("<Gregory>", "<livesIn>", "<Rennes>");
    	kb.add("<Yann>", "<livesIn>", "<Rennes>");

    	kb.add("<Julien>", "<likes>", "<Cooking>");
    	kb.add("<Luis>", "<likes>", "<Travel>");
    	kb.add("<Luis>", "<likes>", "<Cooking>");
    	kb.add("<Gregory>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Read>");
    	kb.add("<Yann>", "<likes>", "<Cooking>");
    	
    	kb.add("<France>", "<hasCapital>", "<Paris>");
    	kb.add("<France>", "<likes>", "<Wine>");
    	kb.add("<France>", "<citizenAre>", "<French>");
    	
    	
    	REMiner reMiner = new REMiner(kb);
		Condition cond = new Condition("<livesIn>", "<France>");
		Condition cond1 = new Condition("<likes>", "<Wine>");
		Condition cond2 = new Condition("<citizenAre>", "<French>");
		Condition cond3 = new Condition("<hasCapital>", "<Paris>");
		Condition cond4 = new Condition("<citizenOf>", "<France>");
		Condition cond5 = new Condition("<likes>", "<Cooking>");
		
		List<Condition> originalList = new ArrayList<>();
		originalList.add(cond);
		originalList.add(cond1);
		originalList.add(cond2);
		
		/*List<Condition> list = reMiner.extendListOfConditions(originalList);
		assertTrue(list.size() == 3);
		assertTrue(list.contains(cond1));
		assertTrue(list.contains(cond2));
		assertFalse(list.contains(cond3));
		assertFalse(list.contains(cond4));
		assertFalse(list.contains(cond5));
		assertFalse(list.contains(cond));;
*/	}
	
	
	
	/**
	 * Test avec citizenOf en dernier élément de par sa complexité et livesIn et likes Cooking qui renvoient les mêmes éléments
	 * CitizenOf seul renvoie la liste des entités à décrire, donc on devrait ne pas récupérer les autres.
	 * @throws InterruptedException 
	 */
	@Test
	void RETestBug() throws InterruptedException {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
		kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
		kb.add("<Gregory>", "<citizenOf>", "<France>");
		kb.add("<Yann>", "<citizenOf>", "<France>");
		
		kb.add("<Julien>", "<profession>", "<Intern>");
		kb.add("<Luis>", "<profession>", "<Researcher>");
		kb.add("<Gregory>", "<profession>", "<GhostIntern>");
		kb.add("<Yann>", "<profession>", "<PhDStudent>");
		
		kb.add("<Julien>", "<livesIn>", "<Rennes>");
		kb.add("<Luis>", "<livesIn>", "<Rennes>");
		kb.add("<Gregory>", "<livesIn>", "<Rennes>");
		kb.add("<Yann>", "<livesIn>", "<Rennes>");
	
		kb.add("<Julien>", "<likes>", "<Cooking>");
		kb.add("<Luis>", "<likes>", "<Travel>");
		kb.add("<Luis>", "<likes>", "<Cooking>");
		kb.add("<Gregory>", "<likes>", "<Cooking>");
		kb.add("<Yann>", "<likes>", "<Read>");
		kb.add("<Yann>", "<likes>", "<Cooking>");
		
		kb.add("<France>", "<hasCapital>", "<Paris>");
		kb.add("<France>", "<likes>", "<Wine>");
		kb.add("<France>", "<citizenAre>", "<French>");
		
		
		List<String> entities2Describe = new ArrayList<>();
		entities2Describe.add("<Gregory>");
		entities2Describe.add("<Julien>");
		entities2Describe.add("<Yann>");
		Condition cond = new Condition("<citizenOf>", "<France>");
		Condition cond2 = new Condition("<livesIn>", "<Rennes>");
		Condition cond3 = new Condition("<likes>", "<Cooking>");
		REMiner reMiner = new REMiner(kb);
		List<Condition> list = reMiner.mine(entities2Describe);
		
		assertTrue(list.contains(cond));
		assertFalse(list.contains(cond2));
		assertFalse(list.contains(cond3));
		
	}
	
	
	/**
	 * makes the extend of the 2 Conditions "citizenOf Ecuador" and "livesIn Rennes"
	 */
	@Test
	public void RETestFinalExtendListOfCond() {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
		kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
		kb.add("<Gregory>", "<citizenOf>", "<Spain>");
		kb.add("<Yann>", "<citizenOf>", "<Italy>");
		
		kb.add("<Julien>", "<profession>", "<Intern>");
		kb.add("<Luis>", "<profession>", "<Researcher>");
		kb.add("<Gregory>", "<profession>", "<GhostIntern>");
		kb.add("<Yann>", "<profession>", "<Researcher>");
		
		kb.add("<Julien>", "<livesIn>", "<Rennes>");
		kb.add("<Luis>", "<livesIn>", "<Rennes>");
		kb.add("<Gregory>", "<livesIn>", "<Rennes>");
		kb.add("<Yann>", "<livesIn>", "<Rennes>");
		
		kb.add("<France>", "<hasCapital>", "<Paris>");
		kb.add("<France>", "<likes>", "<Wine>");
		kb.add("<France>", "<citizenAre>", "<French>");
		
		kb.add("<Ecuador>", "<hasCapital>", "<Quito>");
		kb.add("<Ecuador>", "<speaks>", "<Spannish>");
		kb.add("<Ecuador>", "<speaks>", "<Kichwa>");
		kb.add("<Ecuador>", "<borderW>", "<Peru>");
		
		kb.add("<Spannish>", "<origin>", "<Romance>");
		kb.add("<France>", "<learned>", "<School>");
		kb.add("<Rennes>", "<produces>", "<Cider>");
		kb.add("<Rennes>", "<speaks>", "<Breton>");
		kb.add("<Cider>", "<basedOn>", "<Apple>");
		
		Set<Condition> setCond = new LinkedHashSet<>();
		Condition cond = new Condition("<citizenOf>", "<Ecuador>");
		Condition cond2 = new Condition("<livesIn>", "<Rennes>");
		setCond.add(cond);	
		setCond.add(cond2);
		
		List<String> entities2Describe = new ArrayList<>();
		entities2Describe.add("<Luis>");
	
		REMiner reMiner = new REMiner(kb);
		Set<Condition> listCondFinalExten = reMiner.finalExtendListOfConditions(setCond, 
				new HashSet<>(entities2Describe));
		
		assertTrue(listCondFinalExten.contains(cond));
		assertTrue(listCondFinalExten.contains(cond2));
	}
	
	
	@Test
	void RETestBreadthFirstSearchForParallel() throws InterruptedException{
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
		kb.add("<Luis>", "<citizenOf>", "<France>");
		kb.add("<Gregory>", "<citizenOf>", "<France>");
		kb.add("<Yann>", "<citizenOf>", "<France>");
		
		kb.add("<Julien>", "<profession>", "<Researcher>");
		kb.add("<Veronique>", "<profession>", "<Researcher>");
		kb.add("<Alexandre>", "<profession>", "<Researcher>");
		kb.add("<Laurence>", "<profession>", "<Researcher>");
		
		kb.add("<Julien>", "<livesIn>", "<Rennes>");
		kb.add("<Luis>", "<livesIn>", "<Rennes>");
		kb.add("<Gregory>", "<livesIn>", "<Rennes>");
		kb.add("<Yann>", "<livesIn>", "<Rennes>");
		
		kb.add("<Julien>", "<goesTo>", "<Sherbrooke>");
		
		Set<Condition> setCond = new LinkedHashSet<>();
		Condition cond = new Condition("<citizenOf>", "<France>");
		Condition cond2 = new Condition("<livesIn>", "<Rennes>");
		Condition cond3 = new Condition("<profession>", "<Researcher>");
		Condition cond4 = new Condition("<goesTo>", "<Sherbrooke>");
		setCond.add(cond);	
		setCond.add(cond2);
		setCond.add(cond3);
		setCond.add(cond4);
		
		List<String> entities2Describe = new ArrayList<>();
		entities2Describe.add("<Julien>");
	
		REMiner reMiner = new REMiner(kb);
		List<Condition> listCondBreadth = reMiner.mineFirstBreadthForParellel(entities2Describe, setCond);
		
		assertTrue(listCondBreadth.size() == 1);
		assertTrue(listCondBreadth.contains(cond4));
	}
	
	
	@Test
	void RETestEquals(){
		Condition cond = new Condition("<livesIn>", "<Rennes>");
		Condition cond1 = new Condition("<livesIn>", "<Rennes>");
		assertTrue(cond.equals(cond1));
	}
	
	
	@Test
	void RETestEquals2(){
		Condition cond = new Condition("<livesIn>", "<Rennes>");
		Condition cond1 = new Condition("<livesIn>", "<Paris>");
		assertFalse(cond.equals(cond1));
	}
	
	
	/**
	 * test of equals on 2 equals condition with the same atom and condition
	 */
	@Test
	void RETestEquals3(){
		Condition cond = new Condition("<livesIn>", "<Rennes>");
		Condition cond1 = new Condition("<livesIn>", "<Rennes>");
		Atom atom = new Atom("<Julien>", "<livesIn>", "<Rennes>", null);
		List<Condition> listCond = new ArrayList<>();
		assertTrue(cond.equals(cond1));
		cond.atoms().put(atom, listCond);
		cond1.atoms().put(atom, listCond);
		assertTrue(cond.equals(cond1));
		assertTrue(cond.atoms().equals(cond1.atoms()));
		assertTrue(cond.atoms().keySet().equals(cond1.atoms().keySet()));
		//assertTrue(cond.atoms().values().equals(cond1.atoms().values()));
	}
	
	
	/**
	 * makes the equals on 2 different atoms because of the objectHidden
	 */
	@Test
	void RETestEquals4(){
		Condition cond = new Condition();
		Condition condTest = new Condition();
		Condition cond1 = new Condition("<HeadQ>", "<Company>");
		Atom atom = new Atom("", "<livesIn>", "", "<Rennes>");
		Atom atom1 = new Atom("", "<livesIn>", "", "<Nantes>");
		List<Condition> listCond = new ArrayList<>();
		listCond.add(cond1);

		cond.atoms().put(atom, listCond);
		condTest.atoms().put(atom1, listCond);
		
		assertTrue(cond.equals(condTest));
		assertTrue(cond.atoms().equals(condTest.atoms()));
		assertTrue(cond.atoms().keySet().equals(condTest.atoms().keySet()));
		//assertTrue(cond.atoms().values().equals(condTest.atoms().values()));
	}
	
	/**
	 * makes the equals on 2 atoms with the same objectHidden
	 */
	@Test
	void RETestEquals5(){
		Condition cond = new Condition();
		Condition condTest = new Condition();
		Condition cond1 = new Condition("<HeadQ>", "<Company>");
		Atom atom = new Atom("", "<livesIn>", "", "<Rennes>");
		Atom atom1 = new Atom("", "<livesIn>", "", "<Rennes>");
		List<Condition> listCond = new ArrayList<>();
		listCond.add(cond1);

		cond.atoms().put(atom, listCond);
		condTest.atoms().put(atom1, listCond);
		
		assertTrue(cond.equals(condTest));
		assertTrue(cond.atoms().equals(condTest.atoms()));
		assertTrue(cond.atoms().keySet().equals(condTest.atoms().keySet()));
		//assertTrue(cond.atoms().values().equals(condTest.atoms().values()));
	}
	
	
	@Test
	void RETestGetLastObject(){
		Condition cond = new Condition("<livesIn>", "<Rennes>");
		assertTrue(cond.getLastObject().equals("<Rennes>"));
	}
	
	
	@Test
	void RETestGetLastPredicate(){
		Condition cond = new Condition("<livesIn>", "<Rennes>");
		assertTrue(cond.getLastPredicate().equals("<livesIn>"));
	}
	
	/**
	 * Test of the function addAtom on a single atom
	 */
	@Test
	void RETestAddAtom(){
		Condition cond = new Condition();
		Condition cond1 = new Condition("<livesIn>", "<Rennes>");
		Condition cond2 = new Condition("<likes>", "<Cooking>");
		Condition cond3 = new Condition("<isCitizenOf>", "<France>");
		List<Condition> listCond = new ArrayList<>();
		listCond.add(cond1);
		listCond.add(cond2);
		listCond.add(cond3);
		

		Atom oldAtom = new Atom("", "<livesIn>", "<Rennes>", null);
		Atom addAtom = new Atom("<Rennes>", "<Capital>", "<Bretagne>", null);
		Atom newAtom = new Atom("", "<livesIn>", "", "<Rennes>");
		
		Condition condTest = new Condition("<Capital>", "<Bretagne>");
				
		cond.atoms().put(oldAtom, listCond);
		cond = cond.chainAtomSO(addAtom, oldAtom);
		
		assertTrue(cond.atoms().containsKey(newAtom));
		assertFalse(cond.atoms().keySet().contains(oldAtom));
		assertTrue(cond.atoms().get(newAtom).contains(condTest));
		// Does the new Atom get the Condition of the oldAtom as Values ? 
		//assertTrue(cond.atoms().get(newAtom).contains(cond3));

	}
	
	/**
	 * Test of the function addAtom on a single atom
	 */
	@Test
	void RETestAddAtom2(){
		Condition cond = new Condition();
		Condition cond1 = new Condition("<livesIn>", "<Rennes>");
		Condition cond2 = new Condition("<likes>", "<Cooking>");
		Condition cond3 = new Condition("<isCitizenOf>", "<France>");
		List<Condition> listCond = new ArrayList<>();
		listCond.add(cond1);
		listCond.add(cond2);
		listCond.add(cond3);
		

		Atom oldAtom = new Atom("", "<livesIn>", "<Rennes>", null);
		Atom addAtom = new Atom("<Rennes>", "<Capital>", "<Bretagne>", null);
		Atom newAtom = new Atom("", "<livesIn>", "", "<Rennes>");
		
		Condition condTest = new Condition("<Capital>", "<Bretagne>");
		List<Condition> listTest = new ArrayList<>();
		listTest.addAll(listCond);
		listTest.add(condTest);
		
				
		cond.atoms().put(oldAtom, listCond);
		cond = cond.chainAtomSO(addAtom, oldAtom);
		
		assertTrue(cond.atoms().containsKey(newAtom));
		assertFalse(cond.atoms().keySet().contains(oldAtom));
		assertTrue(cond.atoms().get(newAtom).contains(condTest));
		assertTrue(cond.atoms().values().contains(listTest));
		assertFalse(cond.atoms().keySet().contains(addAtom));
	}
	
	
	/**
	 * Test of the function addAtom on 2 atoms
	 */
	@Test
	void RETestAdd2Atom(){
		Condition cond = new Condition();
		Condition cond1 = new Condition("<livesIn>", "<Rennes>");
		Condition cond2 = new Condition("<likes>", "<Cooking>");
		Condition cond3 = new Condition("<isCitizenOf>", "<France>");
		List<Condition> listCond = new ArrayList<>();
		listCond.add(cond1);
		listCond.add(cond2);
		listCond.add(cond3);
		

		Atom oldAtom = new Atom("", "<livesIn>", "<Rennes>", null);
		Atom addAtom = new Atom("<Rennes>", "<Capital>", "<Bretagne>", null);
		Atom addAtom2 = new Atom("<Rennes>", "<produces>", "<Cider>", null);
		Atom newAtom = new Atom("", "<livesIn>", "", "<Rennes>");
		
		Atom atomTest = new Atom("", "<Capital>", "<Bretagne>", null);
		Atom atomTest2 = new Atom("", "<produces>", "<Cider>", null);
		List<Condition> listTest = new ArrayList<>();
		listTest.add(new Condition());
		listTest.get(0).atoms().put(atomTest, new ArrayList<>());
		listTest.get(0).atoms().put(atomTest2, new ArrayList<>());
						
		cond.atoms().put(oldAtom, new ArrayList<>());
		cond = cond.addAtom(addAtom, addAtom2, oldAtom);
	
		assertTrue(cond.atoms().containsKey(newAtom));
		assertFalse(cond.atoms().containsKey(oldAtom));
		assertTrue(cond.atoms().containsValue(listTest));
	}
	
	
	/**
	 * Case of add a list of Atom, transform the initial Atom (oldAtom) to a new (atomTest) and his son's become condTest and condTest2
	 */
	@Test
	void RETestAddListAtom(){
		Condition cond = new Condition();

		Atom oldAtom = new Atom("", "<livesIn>", "<Rennes>", null);
		Atom addAtom = new Atom("<Rennes>", "<Capital>", "<Bretagne>", null);
		Atom newAtom = new Atom("<Rennes>", "<Produces>", "<Cider>", null);
		Atom atomTest = new Atom("", "<livesIn>", "", "<Rennes>");
		
		Condition condTest = new Condition("<Capital>", "<Bretagne>");
		Condition condTest2 = new Condition("<Produces>", "<Cider>");
		List<Condition> listTest = new ArrayList<>();
		listTest.add(condTest);
		
		List<Atom> listAtom = new ArrayList<>();
		listAtom.add(addAtom);
		listAtom.add(newAtom);
				
		cond.atoms().put(oldAtom, new ArrayList<>());
		cond = cond.addAtoms(listAtom, oldAtom);
		
		assertTrue(cond.atoms().containsKey(atomTest));
		assertFalse(cond.atoms().keySet().contains(oldAtom));
		assertTrue(cond.atoms().get(atomTest).contains(condTest));
		assertTrue(cond.atoms().get(atomTest).contains(condTest2));
		//assertTrue(cond.atoms().values().contains(condTest));
		assertFalse(cond.atoms().keySet().contains(addAtom));
	}
	
	
	/**
	 * Case of add a list of Atoms but with an Atom's subject different from the Atom object 
	 */
	@Test
	void RETestAddListAtom2(){
		Condition cond = new Condition();

		Atom referenceAtom = new Atom("", "<livesIn>", "<Rennes>", null);
		Atom addAtom = new Atom("<Rennes>", "<Capital>", "<Bretagne>", null);
		Atom newAtom = new Atom("<Nantes>", "<Produces>", "<Cider>", null);
		
		List<Atom> listAtom = new ArrayList<>();
		listAtom.add(addAtom);
		listAtom.add(newAtom);
				
		cond.atoms().put(referenceAtom, new ArrayList<>());
		cond = cond.addAtoms(listAtom, referenceAtom);
		
		assertTrue(cond.atoms().isEmpty());
		
	}
	
	/**
	 * Case of add a list of Atoms twice times
	 * the results is the Condition of the second addListAtom based upon the oldAtomTest
	 */
	@Test
	void RETestAddListAtom3(){


		Atom referenceAtom = new Atom("", "<livesIn>", "<Rennes>", null);
		Atom referenceAtomTest = new Atom("", "<Capital>", "<Bretagne>", null);
		Atom addAtom = new Atom("<Rennes>", "<Capital>", "<Bretagne>", null);
		Atom newAtom = new Atom("<Rennes>", "<Produces>", "<Cider>", null);
		Atom sonNewAtom = new Atom("<Bretagne>", "<locatedIn>", "<France>", null);
		Atom sonNewAtom2 = new Atom("<Bretagne>", "<speaks>", "<Breton>", null);
		Atom atomTest = new Atom("", "<Capital>", "", "<Bretagne>");
		Atom atomTest2 = new Atom("", "<livesIn>", "", "<Rennes>");
		
		Condition condTest = new Condition("<Capital>", "<Bretagne>");
		Condition condTest2 = new Condition("<Produces>", "<Cider>");
		Condition condTest3 = new Condition("<locatedIn>", "<France>");
		Condition condTest4 = new Condition("<speaks>", "<Breton>");
		List<Condition> listCond = new ArrayList<>();
		listCond.add(condTest3);
		listCond.add(condTest4);
		List<Condition> listCond2 = new ArrayList<>();
		listCond2.add(condTest);
		listCond2.add(condTest2);
		
		List<Atom> listAtom = new ArrayList<>();
		listAtom.add(addAtom);
		listAtom.add(newAtom);
		
		List<Atom> listSonAtom = new ArrayList<>();
		listSonAtom.add(sonNewAtom);
		listSonAtom.add(sonNewAtom2);
				
		Condition cond = new Condition();
		cond.atoms().put(referenceAtom, new ArrayList<>());
		cond = cond.addAtoms(listAtom, referenceAtom);
		cond = cond.addAtoms(listSonAtom, referenceAtomTest);
		
		assertTrue(cond.atoms().containsKey(atomTest2));
		assertFalse(cond.atoms().keySet().contains(referenceAtomTest));
		assertTrue(cond.atoms().get(atomTest2).get(0).atoms().containsKey(atomTest));
		assertTrue(cond.atoms().get(atomTest2).get(0).atoms().values().contains(listCond));
		// False because condTest has been transform to ["" "Capital" ""] ["" "locatedIn" "France", "" "Speaks" "Breton"]
		assertFalse(cond.atoms().get(atomTest2).get(0).atoms().values().contains(listCond2));
	}
	
	/**
	 * function search return the oldAtom and the List of Condition link to it
	 */
	@Test
	void RETestSearch(){
		Condition cond = new Condition();
		Condition cond1 = new Condition("<livesIn>", "<Rennes>");
		Condition cond2 = new Condition("<likes>", "<Cooking>");
		Condition cond3 = new Condition("<isCitizenOf>", "<France>");
		List<Condition> listCond = new ArrayList<>();
		List<Condition> listCond1 = new ArrayList<>();
		listCond.add(cond1);
		listCond1.add(cond1);
		listCond.add(cond2);
		listCond.add(cond3);
		

		Atom oldAtom = new Atom("<Julien>", "<livesIn>", "<Rennes>", null);
		
		Condition condTest = new Condition("<Capital>", "<Bretagne>");
		List<Condition> listTest = new ArrayList<>();
		listTest.add(condTest);
				
		cond.atoms().put(oldAtom, listCond);

		Condition condSearch = cond.search(oldAtom);
		assertTrue(condSearch.atoms().containsKey(oldAtom));
		assertTrue(condSearch.atoms().containsValue(listCond));
		assertFalse(condSearch.atoms().containsValue(listCond1));
	}
	
	
	/**
	 * Test the function search after an addAtom
	 */
	
	@Test
	void RETestSearch2(){
		Condition cond = new Condition();
		Condition cond1 = new Condition("<produces>", "<Cider>");
		Condition cond2 = new Condition("<dep>", "<35000>");
		Condition cond3 = new Condition("<cityOf>", "<France>");
		List<Condition> listCond = new ArrayList<>();
		listCond.add(cond1);
		listCond.add(cond2);
		listCond.add(cond3);
		

		Atom referenceAtom = new Atom("<Julien>", "<livesIn>", "<Rennes>", null);
		Atom addAtom = new Atom("<Rennes>", "<Capital>", "<Bretagne>", null);
		Atom newAtom = new Atom("", "<livesIn>", "", "<Rennes>");
		
		Condition condTest = new Condition("<Capital>", "<Bretagne>");
		List<Condition> listTest = new ArrayList<>();
		listTest.addAll(listCond);
		listTest.add(condTest);
				
		cond.atoms().put(referenceAtom, listCond);
		cond = cond.chainAtomSO(addAtom, referenceAtom);
		Condition condSearch = cond.search(newAtom);
		assertTrue(condSearch.atoms().containsKey(newAtom));
		assertTrue(condSearch.atoms().containsValue(listTest));
		assertFalse(condSearch.atoms().containsValue(listCond));
	}
	
	
	
	
	@Test
	void RETestMergeCondition(){


		Condition cond1 = new Condition("<livesIn>", "<Rennes>");
		Condition cond2 = new Condition("<likes>", "<Cooking>");
		Condition cond3 = new Condition("<isCitizenOf>", "<France>");
		Condition cond4 = new Condition("<UnRecognized>", "<False>");
		Condition cond5 = new Condition("<Capital>", "<Pristina>");
		Condition cond6 = new Condition("<work>", "<Well>");
		Condition cond7 = new Condition("<Capital>", "<Bretagne>");
		
		List<Condition> listCond = new ArrayList<>();
		listCond.add(cond1);
		listCond.add(cond2);
		listCond.add(cond3);
		
		List<Condition> listCond4TestMerge = new ArrayList<>();
		listCond4TestMerge.add(cond4);
		
		List<Condition> listCond456TestMerge = new ArrayList<>();
		listCond456TestMerge.add(cond5);
		listCond456TestMerge.add(cond4);
		listCond456TestMerge.add(cond6);
		

		Atom oldAtom = new Atom("", "<livesIn>", "<Rennes>", null);
		Atom addAtom = new Atom("<Rennes>", "<Capital>", "<Bretagne>", null);
		Atom oldAtomTestMerge = new Atom("", "<livesIn>", "<Kosovo>", null);
		Atom addAtomTestMerge = new Atom("<Kosovo>", "<Capital>", "<Pristina>", null);
		Atom addAtomTestMerge2 = new Atom("<Kosovo>", "<UnRecognized>", "<False>", null);
		Atom addAtomTestMerge3 = new Atom("<Kosovo>", "<work>", "<Well>", null);
		Atom atomTestMerge = new Atom("", "<livesIn>", "", "<Kosovo>");
		
		Condition cond = new Condition();
		cond.atoms().put(oldAtom, new ArrayList<>());
		//cond becomes the association of "" "livesIn" "Rennes" and "Rennes" "Capital" "Bretagne"
		cond = cond.chainAtomSO(addAtom, oldAtom);
		//condMerge becomes the merge of cond and listCond
		Condition condMerge = cond.mergeCondition(listCond);
		
		List<Atom> listAddAtomTestMerge = new ArrayList<>();
		listAddAtomTestMerge.add(addAtomTestMerge);
		listAddAtomTestMerge.add(addAtomTestMerge2);
		listAddAtomTestMerge.add(addAtomTestMerge3);
		
		Condition condTestMerge = new Condition();
		condTestMerge.atoms().put(oldAtomTestMerge, new ArrayList<>());
		//condTestMerge becomes the association of "" "livesIn" "Kosovo" and addAtomTestMerge, addAtomTestMerge2, addAtomTestMerge3
		condTestMerge = condTestMerge.addAtoms(listAddAtomTestMerge, oldAtomTestMerge);
		List<Condition> listCondTestMerge = new ArrayList<>();
		listCondTestMerge.add(condTestMerge);
		
		//condTestMerge is the merge of condMerge(merge of cond and listCond) and listCondTestMerge compose of(condTestMerge)
		condTestMerge = condMerge.mergeCondition(listCondTestMerge);
		
		assertFalse(condTestMerge.atoms().keySet().contains(oldAtomTestMerge));
		assertTrue(condTestMerge.atoms().keySet().containsAll(condMerge.atoms().keySet()));
		assertFalse(condTestMerge.atoms().values().contains(listCond4TestMerge));
		assertFalse(condTestMerge.atoms().values().contains(listCond456TestMerge));
		assertTrue(condTestMerge.atoms().keySet().contains(atomTestMerge));
		assertTrue(condTestMerge.atoms().get(atomTestMerge).get(0).equals(cond7));
		
	}
	
	
	@Test
	void RETestMergeCondition2(){
		Condition cond1 = new Condition("<livesIn>", "<Rennes>");
		Condition cond2 = new Condition("<likes>", "<Cooking>");
		Condition cond3 = new Condition("<isCitizenOf>", "<France>");
		Condition cond4 = new Condition("<UnRecognized>", "<False>");
		Condition cond5 = new Condition("<Capital>", "<Pristina>");
		Condition cond6 = new Condition("<work>", "<Well>");
		
		List<Condition> listCond = new ArrayList<>();
		listCond.add(cond1);
		listCond.add(cond2);
		listCond.add(cond3);
		
		List<Condition> listCond4TestMerge = new ArrayList<>();
		listCond4TestMerge.add(cond4);
		
		List<Condition> listCond456TestMerge = new ArrayList<>();
		listCond456TestMerge.add(cond5);
		listCond456TestMerge.add(cond4);
		listCond456TestMerge.add(cond6);
		

		Atom oldAtom = new Atom("", "<livesIn>", "<Rennes>", null);
		Atom addAtom = new Atom("<Rennes>", "<Capital>", "<Bretagne>", null);
		Atom oldAtomTestMerge = new Atom("", "<livesIn>", "<Kosovo>", null);
		Atom addAtomTestMerge = new Atom("<Kosovo>", "<Capital>", "<Pristina>", null);
		Atom addAtomTestMerge2 = new Atom("<Kosovo>", "<UnRecognized>", "<False>", null);
		Atom addAtomTestMerge3 = new Atom("<Kosovo>", "<work>", "<Well>", null);
		Atom atomTestMerge = new Atom("", "<livesIn>", "<Rennes>", null);
		
		Condition cond = new Condition();
		cond.atoms().put(oldAtom, new ArrayList<>());
		//cond becomes the association of "" "livesIn" "Rennes" and "Rennes" "Capital" "Bretagne"
		cond = cond.chainAtomSO(addAtom, oldAtom);
		//condMerge becomes the merge of cond and listCond
		Condition condMerge = cond.mergeCondition(listCond);
		
		List<Atom> listAddAtomTestMerge = new ArrayList<>();
		listAddAtomTestMerge.add(addAtomTestMerge);
		listAddAtomTestMerge.add(addAtomTestMerge2);
		listAddAtomTestMerge.add(addAtomTestMerge3);
		
		Condition condTestMerge = new Condition();
		condTestMerge.atoms().put(oldAtomTestMerge, new ArrayList<>());
		//condTestMerge becomes the association of "" "livesIn" "Kosovo" and addAtomTestMerge, addAtomTestMerge2, addAtomTestMerge3
		condTestMerge = condTestMerge.addAtoms(listAddAtomTestMerge, oldAtomTestMerge);
		List<Condition> listCondTestMerge = new ArrayList<>();
		listCondTestMerge.add(condTestMerge);

		
		//condTestMerge is the merge of condMerge(merge of cond and listCond) and listCondTestMerge compose of(condTestMerge)
		condTestMerge = condMerge.mergeCondition(listCondTestMerge);
		
		assertFalse(condTestMerge.atoms().keySet().contains(oldAtomTestMerge));
		assertTrue(condTestMerge.atoms().keySet().containsAll(condMerge.atoms().keySet()));
		assertFalse(condTestMerge.atoms().values().contains(listCond4TestMerge));
		assertFalse(condTestMerge.atoms().values().contains(listCond456TestMerge));
		assertTrue(condTestMerge.atoms().keySet().contains(atomTestMerge));
	}
	
	
	/**
	 * problems because the algorithm didn't search in all the condition but only the "main Condition" intersect of all entities
	 * it will not search if there are common sons
	 * Julien and Luis are "citizenOf a country which speaks a langage of Romane origin" 
	 * @throws InterruptedException 
	 */
	@Test
	public void RETestComplexFunction() throws InterruptedException {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
		kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
		kb.add("<Gregory>", "<citizenOf>", "<Spain>");
		kb.add("<Yann>", "<citizenOf>", "<Italy>");
		
		kb.add("<Julien>", "<profession>", "<Intern>");
		kb.add("<Luis>", "<profession>", "<Researcher>");
		kb.add("<Gregory>", "<profession>", "<GhostIntern>");
		kb.add("<Yann>", "<profession>", "<Researcher>");
		
		kb.add("<Julien>", "<livesIn>", "<Rennes>");
		kb.add("<Luis>", "<livesIn>", "<Barcelona>");
		//kb.add("<Gregory>", "<livesIn>", "<Rennes>");
		//kb.add("<Yann>", "<livesIn>", "<Rennes>");
		
		kb.add("<France>", "<hasCapital>", "<Paris>");
		kb.add("<France>", "<likes>", "<Wine>");
		kb.add("<France>", "<citizenAre>", "<French>");
		
		kb.add("<Ecuador>", "<hasCapital>", "<Quito>");
		kb.add("<Ecuador>", "<speaks>", "<Spannish>");
		kb.add("<Ecuador>", "<speaks>", "<Kichwa>");
		kb.add("<Ecuador>", "<borderW>", "<Peru>");
		
		kb.add("<Spannish>", "<origin>", "<Romance>");
		kb.add("<France>", "<speaks>", "<Breton>");
		kb.add("<Rennes>", "<produces>", "<Cider>");
		kb.add("<Rennes>", "<speaks>", "<Breton>");
		kb.add("<Cider>", "<basedOn>", "<Apple>");
		kb.add("<Breton>", "<origin>", "<Romance>");
		
		List<String> entities2Describe = new ArrayList<>();
		entities2Describe.add("<Luis>");
		entities2Describe.add("<Julien>");
	
		REMiner reMiner = new REMiner(kb);
		List<Condition> listCondFinal = reMiner.mine(entities2Describe);
		
		/*problem because we don't initialize the enableExtensionFinal to true so it didn't search inside the predicate of size 3
		* In this case I must change the test to assertTrue instead of assertFalse
		*/
		assertTrue(listCondFinal.isEmpty());
	}
	
	
	/**
	 * problems because the algorithm didn't search in all the condition but only the "main Condition" intersect of all entities
	 * it will not search if there are common sons
	 * Julien and Luis are "citizenOf a country which likes Wine"
	 * @throws InterruptedException 
	 */
	@Test
	public void RETestComplexFunction2() throws InterruptedException {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
		kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
		
		kb.add("<France>", "<likes>", "<Wine>");
		kb.add("<Ecuador>", "<likes>", "<Wine>");
		
		
		List<String> entities2Describe = new ArrayList<>();
		entities2Describe.add("<Luis>");
		entities2Describe.add("<Julien>");
	
		Condition cond = new Condition("<citizenOf>", "<France>");
		Atom atom = new Atom("", "<citizenOf>", "<France>", null);
		Atom atomAdd = new Atom("<France>", "<likes>", "<Wine>", null);
		cond = cond.chainAtomSO(atomAdd, atom);
		
		Condition cond2 = new Condition("<citizenOf>", "<Ecuador>");
		Atom atom2 = new Atom("", "<citizenOf>", "<Ecuador>", null);
		Atom atomAdd2 = new Atom("<Ecuador>", "<likes>", "<Wine>", null);
		cond2 = cond2.chainAtomSO(atomAdd2, atom2);
		
		REMiner reMiner = new REMiner(kb);
		List<Condition> listCondFinal = reMiner.mine(entities2Describe);
		
		assertFalse(listCondFinal.isEmpty());
		assertTrue(listCondFinal.contains(cond));
		assertTrue(listCondFinal.contains(cond2));
	}
	
	/**
	 * problems because the algorithm didn't search in all the condition but only the "main Condition" intersect of all entities
	 * it will not search if there are common sons
	 * Julien and Luis are "citizenOf a country which likes Wine"
	 * @throws InterruptedException 
	 */
	@Test
	public void RETestComplexFunction3() throws InterruptedException {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<likes>", "<ComputerScience>");
		kb.add("<Luis>", "<likes>", "<ComputerScience>");
		kb.add("<Gregory>", "<likes>", "<ComputerScience>");
		kb.add("<Laurence>", "<likes>", "<ComputerScience>");
		kb.add("<Veronique>", "<likes>", "<ComputerScience>");
		kb.add("<Alexandre>", "<likes>", "<ComputerScience>");
		kb.add("<Mael>", "<likes>", "<ComputerScience>");
		
		kb.add("<Julien>", "<citizenOf>", "<France>");
		kb.add("<Luis>", "<citizenOf>", "<France>");
		kb.add("<Gregory>", "<citizenOf>", "<France>");
		kb.add("<Laurence>", "<citizenOf>", "<France>");
		kb.add("<Veronique>", "<citizenOf>", "<France>");
		kb.add("<Alexandre>", "<citizenOf>", "<France>");
		
		kb.add("<Julien>", "<livesIn>", "<Rennes>");
		kb.add("<Luis>", "<livesIn>", "<Rennes>");
		kb.add("<Gregory>", "<livesIn>", "<Rennes>");
		kb.add("<Laurence>", "<livesIn>", "<Rennes>");
		kb.add("<Veronique>", "<livesIn>", "<Rennes>");
		
		kb.add("<Julien>", "<likes>", "<Cooking>");
		kb.add("<Luis>", "<likes>", "<Cooking>");
		kb.add("<Gregory>", "<likes>", "<Cooking>");

		kb.add("<Julien>", "<likes>", "<Running>");
		kb.add("<Laurence>", "<likes>", "<Running>");
		
		List<String> entities2Describe = new ArrayList<>();
		entities2Describe.add("<Julien>");
		
		Condition cond = new Condition("<citizenOf>", "<France>");		
		Condition cond2 = new Condition("<citizenOf>", "<Ecuador>");
		Condition condTrue = new Condition("<likes>", "<Cooking>");
		Condition condTrue2 = new Condition("<likes>", "<Running>");
		
		REMiner reMiner = new REMiner(kb);
		List<Condition> listCondFinal = reMiner.mine(entities2Describe);
		
		assertFalse(listCondFinal.isEmpty());
		assertFalse(listCondFinal.contains(cond));
		assertFalse(listCondFinal.contains(cond2));
		assertTrue(listCondFinal.contains(condTrue));
		assertTrue(listCondFinal.contains(condTrue2));
	}
	
	/**
	 * problems because the algorithm didn't search in all the condition but only the "main Condition" intersect of all entities
	 * it will not search if there are common sons
	 * Julien and Luis are "citizenOf a country which likes Wine"
	 * @throws InterruptedException 
	 */
	@Test
	public void RETestExhaustiveMine() throws InterruptedException {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<likes>", "<ComputerScience>");
		kb.add("<Luis>", "<likes>", "<ComputerScience>");
		kb.add("<Gregory>", "<likes>", "<ComputerScience>");
		kb.add("<Laurence>", "<likes>", "<ComputerScience>");
		kb.add("<Veronique>", "<likes>", "<ComputerScience>");
		kb.add("<Alexandre>", "<likes>", "<ComputerScience>");
		kb.add("<Mael>", "<likes>", "<ComputerScience>");
		
		kb.add("<Julien>", "<citizenOf>", "<France>");
		kb.add("<Luis>", "<citizenOf>", "<France>");
		kb.add("<Gregory>", "<citizenOf>", "<France>");
		kb.add("<Laurence>", "<citizenOf>", "<France>");
		kb.add("<Veronique>", "<citizenOf>", "<France>");
		kb.add("<Alexandre>", "<citizenOf>", "<France>");
		
		kb.add("<Julien>", "<livesIn>", "<Rennes>");
		kb.add("<Luis>", "<livesIn>", "<Rennes>");
		kb.add("<Gregory>", "<livesIn>", "<Rennes>");
		kb.add("<Laurence>", "<livesIn>", "<Rennes>");
		kb.add("<Veronique>", "<livesIn>", "<Rennes>");
		
		kb.add("<Julien>", "<enjoy>", "<Cooking>");
		kb.add("<Luis>", "<enjoy>", "<Cooking>");
		kb.add("<Gregory>", "<enjoy>", "<Cooking>");

		kb.add("<Julien>", "<appreciate>", "<Running>");
		kb.add("<Laurence>", "<appreciate>", "<Running>");
		
		kb.add("<Julien>", "<goes>", "<ToCanada>");
		
		List<String> entities2Describe = new ArrayList<>();
		entities2Describe.add("<Julien>");
		
		Condition cond = new Condition("<likes>", "<ComputerScience>");		
		Condition cond2 = new Condition("<citizenOf>", "<France>");
		Condition cond3 = new Condition("<livesIn>", "<Rennes>");
		Condition cond4 = new Condition("<enjoy>", "<Cooking>");
		Condition cond5 = new Condition("<appreciate>", "<Running>");
		Condition condTrue = new Condition("<goes>", "<ToCanada>");
		
		Set<Condition> setCond = new LinkedHashSet<>();
		setCond.add(cond);
		setCond.add(cond2);
    	setCond.add(cond3);
    	setCond.add(cond4);
    	setCond.add(cond5);
    	setCond.add(condTrue);
		
		REMiner reMiner = new REMiner(kb);
		List<Condition> listCondFinal = reMiner.mineExhaustively(entities2Describe, setCond, 
				new ArrayList<List<Condition>>(), new ArrayList<Condition>());
		
		assertFalse(listCondFinal.isEmpty());
		assertFalse(listCondFinal.contains(cond));
		assertFalse(listCondFinal.contains(cond2));
		assertTrue(listCondFinal.contains(condTrue));
		assertFalse(listCondFinal.contains(cond3));
		assertFalse(listCondFinal.contains(cond4));
	}
	
	/**
	 * problems because the algorithm didn't search in all the condition but only the "main Condition" intersect of all entities
	 * it will not search if there are common sons
	 * Julien and Luis are "citizenOf a country which likes Wine"
	 * @throws InterruptedException 
	 */
	@Test
	public void RETestExhaustiveMine2() throws InterruptedException {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<livesIn>", "<Rennes>");
		kb.add("<Veronique>", "<livesIn>", "<Rennes>");
		kb.add("<Alexandre>", "<livesIn>", "<Rennes>");
		kb.add("<Laurence>", "<livesIn>", "<Rennes>");
		
		kb.add("<Julien>", "<enjoy>", "<Cooking>");
		kb.add("<Luis>", "<enjoy>", "<Cooking>");
		kb.add("<Gregory>", "<enjoy>", "<Cooking>");

		kb.add("<Julien>", "<appreciate>", "<Running>");
		kb.add("<Laurence>", "<appreciate>", "<Running>");
		
		kb.add("<Julien>", "<goes>", "<ToCanada>");
		
		List<String> entities2Describe = new ArrayList<>();
		entities2Describe.add("<Julien>");
		
		Condition cond3 = new Condition("<livesIn>", "<Rennes>");
		Condition cond4 = new Condition("<enjoy>", "<Cooking>");
		Condition cond5 = new Condition("<appreciate>", "<Running>");
		Condition condTrue = new Condition("<goes>", "<ToCanada>");
		
		Set<Condition> setCond = new LinkedHashSet<>();
    	setCond.add(cond3);
    	setCond.add(cond4);
    	setCond.add(cond5);
    	setCond.add(condTrue);
		
		REMiner reMiner = new REMiner(kb);
		List<Condition> listCondFinal = reMiner.mineExhaustively(entities2Describe, setCond,
				new ArrayList<List<Condition>>(), new ArrayList<Condition>());
		System.out.println(listCondFinal);
		assertFalse(listCondFinal.isEmpty());
		assertTrue(listCondFinal.contains(condTrue));
		assertFalse(listCondFinal.contains(cond3));
		assertFalse(listCondFinal.contains(cond4));
		assertFalse(listCondFinal.contains(cond5));
	}
	
	@Test
	public void RETestExhaustiveMine3() throws InterruptedException {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<likes>", "<ComputerScience>");
		kb.add("<Luis>", "<likes>", "<ComputerScience>");
		kb.add("<Gregory>", "<likes>", "<ComputerScience>");
		kb.add("<Laurence>", "<likes>", "<ComputerScience>");
		kb.add("<Veronique>", "<likes>", "<ComputerScience>");
		kb.add("<Alexandre>", "<likes>", "<ComputerScience>");
		kb.add("<Mael>", "<likes>", "<ComputerScience>");
		
		kb.add("<Julien>", "<citizenOf>", "<France>");
		kb.add("<Luis>", "<citizenOf>", "<France>");
		kb.add("<Gregory>", "<citizenOf>", "<France>");
		kb.add("<Laurence>", "<citizenOf>", "<France>");
		kb.add("<Veronique>", "<citizenOf>", "<France>");
		kb.add("<Alexandre>", "<citizenOf>", "<France>");
		
		kb.add("<Julien>", "<livesIn>", "<Rennes>");
		kb.add("<Luis>", "<livesIn>", "<Rennes>");
		kb.add("<Gregory>", "<livesIn>", "<Rennes>");
		kb.add("<Laurence>", "<livesIn>", "<Rennes>");
		kb.add("<Veronique>", "<livesIn>", "<Rennes>");
		
		kb.add("<Julien>", "<enjoy>", "<Cooking>");
		kb.add("<Luis>", "<enjoy>", "<Cooking>");
		kb.add("<Gregory>", "<enjoy>", "<Cooking>");

		kb.add("<Julien>", "<appreciate>", "<Running>");
		kb.add("<Laurence>", "<appreciate>", "<Running>");
		
		kb.add("<Julien>", "<goes>", "<ToCanada>");
		
		List<String> entities2Describe = new ArrayList<>();
		entities2Describe.add("<Julien>");
		
		Condition cond = new Condition("<likes>", "<ComputerScience>");		
		Condition cond2 = new Condition("<citizenOf>", "<France>");
		Condition cond3 = new Condition("<livesIn>", "<Rennes>");
		Condition cond4 = new Condition("<enjoy>", "<Cooking>");
		Condition cond5 = new Condition("<appreciate>", "<Running>");
		Condition condTrue = new Condition("<goes>", "<ToCanada>");
		
		Set<Condition> setCond = new LinkedHashSet<>();
		setCond.add(cond);
		setCond.add(cond2);
    	setCond.add(cond3);
    	setCond.add(cond4);
    	setCond.add(cond5);
    	setCond.add(condTrue);
		
		REMiner reMiner = new REMiner(kb);
		List<Condition> listCondFinalExha = reMiner.mineExhaustively(entities2Describe, setCond, 
				new ArrayList<List<Condition>>(), new ArrayList<Condition>());
		List<Condition> listCondFinal = reMiner.mine(entities2Describe, setCond);
		
		assertFalse(listCondFinal.isEmpty());
		assertFalse(listCondFinal.contains(cond));
		assertFalse(listCondFinal.contains(cond2));
		assertFalse(listCondFinal.contains(condTrue));
		assertFalse(listCondFinal.contains(cond3));
		assertTrue(listCondFinal.contains(cond4));
		assertTrue(listCondFinal.contains(cond5));
		assertFalse(listCondFinalExha.contains(cond));
		assertFalse(listCondFinalExha.contains(cond2));
		assertFalse(listCondFinalExha.contains(cond3));
		assertFalse(listCondFinalExha.contains(cond4));
		assertFalse(listCondFinalExha.contains(cond5));
		assertTrue(listCondFinalExha.contains(condTrue));
	}
}

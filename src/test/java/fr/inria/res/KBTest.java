package fr.inria.res;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.Arrays;
import java.util.List;

import fr.inria.res.data.InMemoryKB;
import javatools.datatypes.ByteString;
import javatools.datatypes.IntHashMap;

public class KBTest {
	

	public static void main(String[] args) throws IOException {
		InMemoryKB kb = new InMemoryKB();
		kb.load(new File("src/data/yago-test/yago-test.tsv"));
		System.out.println(kb.getRelations());
		Set<ByteString> results = kb.selectDistinct(ByteString.of("?y"), 
				InMemoryKB.triples(InMemoryKB.triple("?x", "<diedIn>", "?y"), InMemoryKB.triple("?y", "<isLocatedIn>", "<France>"), InMemoryKB.triple("?x", "<hasChild>", "?z")));
		System.out.println(results);
		if(kb.contains(InMemoryKB.triple(ByteString.of("<Barack_Obama>"), "<isMarriedTo>", "<Michelle_Obama>"))) {
			System.out.println("Ju");
		}
		else {
			System.out.println("bis");
		}
		Map<ByteString, IntHashMap<ByteString>> res = kb.selectDistinct(ByteString.of("?x"), ByteString.of("?y"),
				InMemoryKB.triples(InMemoryKB.triple("?x", "<diedIn>", "?y"), InMemoryKB.triple("?y", "<isLocatedIn>", "<France>"), InMemoryKB.triple("?x", "<hasChild>", "?z")));
		System.out.println(res);
		if(res.containsKey(ByteString.of("<Maria_Christina_of_the_Two_Sicilies>"))) {
			System.out.println("test");
		}
		else {
			System.out.println("essai");
		}
		System.out.println(res.keySet());
		System.out.println(res.values());
		
		
		/*
		 * Fonction qui permet de récupérer dans la base de données toutes les informations sur les individus
		 * @param List<ByteString>
		 * @return Map<ByteString, IntHashMap<ByteString>>
		 */
		//ByteString.of("<Todd_Haynes>"), 
		List<ByteString> queue = Arrays.asList(ByteString.of("<Barack_Obama>"), ByteString.of("<Michelle_Obama>"), ByteString.of("<Maria_Christina_of_the_Two_Sicilies>"));
		Queue<ByteString> priorityQ = new PriorityQueue<ByteString>((a,b) -> a.length() - b.length());
		System.out.println("faux");
		priorityQ.offer(ByteString.of("<hasChild>"));
		priorityQ.offer(ByteString.of("<diedIn>"));
		priorityQ.offer(ByteString.of("<isMarriedTo>"));
		System.out.println(priorityQ);
		
		//for(int i = 0; i < queue.size(); i++) {
			//System.out.print(queue.get(i) + " ");
			//System.out.print(priorityQ.peek() + " ");
			Map<ByteString,IntHashMap<ByteString>> map = kb.selectDistinct(ByteString.of("?y"), ByteString.of("?x"),
					InMemoryKB.triples(InMemoryKB.triple(queue.get(0), "?x", "?y")));
			System.out.println(map);
			System.out.println("...............................");
			System.out.println(map.keySet());
			System.out.println("...............................");
			System.out.println(map.entrySet());
			System.out.println("...............................");
			System.out.println(map.values());
			System.out.println("...............................");
			
		//}
	
	}
}

package fr.inria.data;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Set;

import org.junit.Test;

import fr.inria.res.Atom;
import fr.inria.res.Condition;
import fr.inria.res.data.InMemoryKB;
import fr.inria.res.data.KB;
import javatools.datatypes.ByteString;

public class InMemoryKBTest {
	
	static KB kb;
	
	static {
		kb = new InMemoryKB();
		InMemoryKB ikb =(InMemoryKB)kb;
		try {
			ikb.load(new File("src/data/test/kb.rdf"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSimpleCondition1() {
		Condition condition = Condition.makeCondition("<graduatedFrom>", "<INSA>");
		Set<String> results = kb.selectDistinct(condition);
		assertTrue(results.contains("<Mael>"));
		assertEquals(1, results.size());
	}
	
	@Test
	public void testPathCondition() {
		Condition condition = Condition.makeCondition("<worksAt>", "<IRISA>");
		Atom newAtom = new Atom("<IRISA>", "<headquarteredIn>", "<Rennes>", null);
		Condition pathCondition = condition.chainAtomSO(newAtom, condition.getSetAtoms().iterator().next());
		Set<String> results = kb.selectDistinct(pathCondition);
		for (ByteString[] atom : InMemoryKB.condition2Query(pathCondition, "?x0")) {
			System.out.println(Arrays.toString(atom));
		}
		System.out.println("Query for testPathCondition");		
		assertTrue(results.contains("<Mael>"));
		assertTrue(results.contains("<Luis>"));		
		assertTrue(results.contains("<Julien>"));
		assertTrue(results.contains("<Colin>"));
		assertTrue(results.contains("<Kevin>"));		
		assertEquals(5, results.size());	
	}

	@Test
	public void selectDistinctForPathTest() {
		Condition condition = Condition.makeCondition("<worksAt>", "<Energiency>");
		Atom newAtom1 = new Atom("<Energiency>", "<rdf:type>", "<Company>", null);
		Atom newAtom2 = new Atom("<Energiency>", "<rdf:type>", "<Startup>", null);
		Condition pathCondition = condition.addAtoms(Arrays.asList(newAtom1, newAtom2), 
				condition.getSetAtoms().iterator().next());

		for (ByteString[] atom : InMemoryKB.condition2Query(pathCondition, "?x0")) {
			System.out.println(Arrays.toString(atom));
		}
		Set<String> results = kb.selectDistinct(pathCondition);
		assertTrue(results.contains("<Mael>"));
		assertEquals(1, results.size());
		
	}
	
	@Test
	public void selectDistinctForStarTest() {
		Condition condition = Condition.makeCondition("<worksAt>", "<Energiency>");
		Atom newAtom1 = new Atom("<Energiency>", "<rdf:type>", "<Company>", null);
		Atom newAtom2 = new Atom("<Energiency>", "<rdf:type>", "<Startup>", null);
		Condition pathCondition = condition.addAtom(newAtom1, newAtom2, 
				condition.getSetAtoms().iterator().next());
		System.out.println("Query for selectDistinctForStarTest");
		for (ByteString[] atom : InMemoryKB.condition2Query(pathCondition, "?x0")) {
			System.out.println(Arrays.toString(atom));
		}
		Set<String> results = kb.selectDistinct(pathCondition);
		assertTrue(results.contains("<Mael>"));
		assertEquals(1, results.size());
	}
}

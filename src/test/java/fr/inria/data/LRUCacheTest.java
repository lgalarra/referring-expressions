package fr.inria.data;

import fr.inria.res.data.LRUQueryCache;

public class LRUCacheTest {

	public static void main(String[] args) {
		LRUQueryCache<Long> cache = new LRUQueryCache<Long>(3);
		cache.put("q1", 5l);
		cache.put("q2", 5l);
		cache.put("q3", 5l);
		cache.put("q4", 5l);
		System.out.println(cache);
		cache.get("q2");
		cache.put("q5", 4l);
		System.out.println(cache);		
		cache.get("q2");	
		System.out.println(cache);			
	}

}

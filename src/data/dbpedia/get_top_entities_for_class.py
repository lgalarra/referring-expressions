#!/usr/bin/python3

import csv
import sys
import operator
import re
import random
from add_inverse_facts import getEntitiesWithImportance


if __name__ == '__main__' :
    if len(sys.argv) < 3 :
        print('./get_top_entities_for_class.py <ratio> <class1> [class2] .... [classN]', file=sys.stderr)
        print('<ratio> : real number in the interval (0, 1]', file=sys.stderr)
        print('The program was not provided enough arguments')        
        sys.exit(1)
    
    try : 
        ratio = float(sys.argv[1])
    except ValueError:
        print('Provide a real number in the interval (0, 1]', file=sys.stderr)         
        sys.exit(1)
    
    
    classNames = [x.strip() for x in sys.argv[2:]]
    print ('Classes: ', ", ".join(classNames))
    
    ## Get the relevance of entities by number of facts
    entitiesDict, entitiesInClassesDict = getEntitiesWithImportance('../wikidata/wikidata-simple-statements.compressed.clean.ids-labeled.plus-interesting-literals.nt', classNames)
    sortedEntities = [x[0] for x in sorted(entitiesInClassesDict.items(), key=operator.itemgetter(1), reverse=True)]
    top = int(len(sortedEntities) * ratio)
    topEntities = set(sortedEntities[:top + 1])                
            
    with open('topentities-for-classes-' + str(ratio).replace('.', '_') + "-" + "-".join([re.sub("[^a-zA-Z]+", '', cn) for cn in classNames]), 'w') as ftop :
        for topEntity in topEntities :
            ftop.write(topEntity.replace('<', '').replace('>', ''))
            ftop.write('\t' + str(entitiesDict[topEntity]))
            ftop.write('\n') 
#!/usr/bin/python3

import re
import sys

with open('pagerank_en_2016-04.ttl') as f :
    for line in f :
        line = line.strip('\n')
        parts = line.split('<http://purl.org/voc/vrank#hasRank>')
        if len(parts) < 2 :
            continue
            
        subject = parts[0].strip('\t')
        match = re.search('"([0-9]+\\.[0-9]+)"', parts[1])
        value = '0.0'
        if match is not None :
            value = match.group(1)
        else :
            print('No match in' + parts[1], file=sys.stderr)
            continue
        print(subject + '\t' + value)
    
    
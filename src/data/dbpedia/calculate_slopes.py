#!/usr/bin/python3

import csv
import sys
import operator
import re
import random
import math
import pandas as pd
from sklearn import linear_model
import os

def solve(filename, absoluteRanking=False, message='predicates'):
    predicate = message
    records = []
    with open(filename, 'r') as fin :
        lineN = 0
        for line in fin :
            if lineN == 0 and not absoluteRanking :                
                predicate = line.rstrip('\n')
                lineN += 1 
                continue               
            elif lineN == 0 and absoluteRanking :
                predicate = message
                lineN += 1 

            if line.startswith('"') :
                continue

                    
            parts = line.rstrip('\n').split(' ')
            if len(parts) >= 2 :
                #print(lineN, float(parts[1]) + 1.0)
                records.append((math.log(lineN, 2), math.log(float(parts[1]) + 1.0, 2)))
                
            lineN += 1   
     
    labels = ['log_ranking', 'log_degree']               
    df = pd.DataFrame.from_records(records, columns=labels) 
    if len(df) == 0 :
        return None, None
    
    y = df['log_ranking']
    del df['log_ranking']
    #print(df)
    reg = linear_model.LinearRegression()
    reg.fit(df, y)
    score = reg.score(df, y)
    return predicate, (reg.coef_[0], reg.intercept_, score)
       


if __name__ == '__main__' :
    root = "ranking_relations_pagerank/"
    predicate2Coefficients = {}
    
    for r,d,f in os.walk(root):
        for file in f:
            filename = os.path.join(root,file)
            print(filename)        
            if (os.path.basename(filename).startswith('ranking_')) :                
                predicate, coefficients = solve(filename)
                if predicate is not None and coefficients is not None :
                    print(predicate, coefficients)
                    predicate2Coefficients[predicate] = coefficients
    
    p, cp = solve('cached_relation_queries', absoluteRanking=True, message='predicates')
    p1, cp1 = solve('topentities-ranking_pagerank', absoluteRanking=True, message='constants')       
    with open('ranking_relations_pagerank/coefficients', 'w') as ftop :
        ftop.write('Predicate\tSlope\tIntercept\tR^2\n')
        ftop.write(p + '\t')
        ftop.write(str(cp[0]) + '\t')
        ftop.write(str(cp[1]) + '\t' + str(cp[2]))
        ftop.write('\n')
        
        ftop.write(p1 + '\t')
        ftop.write(str(cp1[0]) + '\t')
        ftop.write(str(cp1[1]) + '\t' + str(cp1[2]))
        ftop.write('\n')
        
        for predicate in predicate2Coefficients :
            ftop.write(predicate + '\t')
            ftop.write(str(predicate2Coefficients[predicate][0]) + '\t')
            ftop.write(str(predicate2Coefficients[predicate][1]) + '\t' + str(predicate2Coefficients[predicate][2]))
            ftop.write('\n') 
            
    
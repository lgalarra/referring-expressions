
This folder contains the evaluation datasets/details for type generation in RDF datatype properties using DBpedia 3.9 and DBpedia 2015-04 datasets.
This contains two evaluations. First evaluation is about type generation using Dbpedia spotlight as the baseline. Evaluation data for this is in "evaluation1" folder.

Files named "final.txt" in each folder for evaluation1 (baseline and faces-e) have manual evaluation results for the type generation. In each row, the following format holds.
<# of correct types identified, # of total types generated>:-:<property>:-:<value>:-:<set of identified types>
note that 'set of identified types' do not have the namespace details.



The second evaluation is about faceted entity summary generation using FACES-E (extention of FACES) and a gold standard is created using 17 human evaluators. 
There are total of 80 entities used in this evaluation and details are in "evaluation2' folder. There are 20 entities selected from Dbpedia 3.9 and the rest of 60 
entities selected from DBpedia 2015-04. Each entity received at least 4 human summaries of length 5 and 10 (450 x 2 summaries) and they are used for the evaluation 
of FACES-E. List of entities for each entity sample is in the respective folders inside "evaluation2" folder.

The ideal summaries are in .xls file format (excel files). The excel files have selections made by 
human judges as the ideal summaries. There are two tabs in each excel file: (1) a tab called 'Triples' that has features and the summary 
selection and (2) a tab called 'Extra-Information'. The 'Triples' tab is the one to process to extract the ideal summaries.
It list <ID> <Property> <Value> <5 triple summary> <10 triple summary>. Judges have marked with a 'x' for their preference of 
the feature in 5 triple summary (for 5 times) and 10 triple summary (for 10 times).

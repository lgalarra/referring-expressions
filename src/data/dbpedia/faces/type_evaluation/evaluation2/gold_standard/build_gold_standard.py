#!/usr/bin/python3

import glob
import os
import numpy
import re
import sys
import csv
import scipy.spatial


def normalizedHamming(s1, s2) :
    scipy.spatial.distance.hamming(s1, s2) / scipy.spatial.distance.jaccard(s1, s2)

def getPredicates(topSubgraphs) :
    predicatesInRanking = set()
    indexes = []
    
    for match in re.finditer(r">\(\?x[0-1]", topSubgraphs) :
        indexes.append(match.start(0))        
            
    for index in indexes :
        ix = index
        c  = topSubgraphs[ix]            
        while c != '<' :
            ix -= 1
            c = topSubgraphs[ix]
        
        predicate = topSubgraphs[ix:index+1]            
        if predicate.endswith('-inv>') :
            predicate = predicate.replace('-inv>', '>')
            
        predicatesInRanking.add(predicate)    
            
    return predicatesInRanking

def getObjects(topSubgraphs) :
    objectsInRanking = set()
    indexes = []
    
    for match in re.finditer(r">\(\?x[0-1],\s", topSubgraphs) :
        indexes.append(match.end(0))
    
    
    for index in indexes :
        c  = topSubgraphs[index]            

        if c == '?' :
            continue
            
        ix = index + 1;
            
        while c != '>' and c != '"' :
            ix += 1
            c = topSubgraphs[ix]
            
        objValue = topSubgraphs[index:ix+1]                        
        objectsInRanking.add(objValue)    
            
    return objectsInRanking

if len(sys.argv) >= 3 : 
    param = sys.argv[2]
else :
    param = 'top10'

paramConf = '*'
if len(sys.argv) >= 4 :
    paramConf = sys.argv[3]
    

if paramConf == '*' :
    confs = ['**/*/*.csv', 'dbpedia3.9/1/*.csv', 'dbpedia3.9/1/*.csv', 
        'dbpedia3.9/3/*.csv', 'dbpedia3.9/4/*.csv',
        'dbpedia3.9/5/*.csv', 'dbpedia2015/1/*.csv', 'dbpedia2015/2/*.csv',
        'dbpedia2015/3/*.csv', 'dbpedia2015/4/*.csv',
        'dbpedia2015/5/*.csv', 'dbpedia2015/6/*.csv', 'dbpedia2015/7/*.csv' ]
elif paramConf == '3.9' :
    confs = ['dbpedia3.9/*/*.csv', 'dbpedia3.9/1/*.csv', 'dbpedia3.9/1/*.csv', 'dbpedia3.9/3/*.csv', 'dbpedia3.9/4/*.csv', 'dbpedia3.9/5/*.csv']
else :
    confs = ['dbpedia2015/*/*.csv', 'dbpedia2015/1/*.csv', 'dbpedia2015/2/*.csv', 'dbpedia2015/3/*.csv', 'dbpedia2015/4/*.csv', 'dbpedia2015/5/*.csv', 'dbpedia2015/6/*.csv', 'dbpedia2015/7/*.csv']

qualitiesF = []
qualitiesPR = []
qualitiesSO = []
qualitiesPRSO = []

for conf in confs :
    entityPairDict = {}
    entityPredicateDict = {}
    entityObjectDict = {}

    remiOverlap = {}
    remiPROverlap = {}

    remiOverlapPrecision = {}
    remiPROverlapPrecision = {}

    remiSolutionPrecision = {}
    remiPRSolutionPrecision = {}

    remiObjSolutionPrecision = {}
    remiPRObjSolutionPrecision = {}

    remiObjSolutionOverlap = {}
    remiPRObjSolutionOverlap = {}

    remiObjRecall = {}
    remiPRObjRecall = {} 

    for filename in glob.iglob(conf, recursive=True):
         with open(filename, 'r') as f:
             entityName = os.path.splitext(os.path.basename(filename))[0]
             entityName = 'http://dbpedia.org/resource/' + entityName.replace('.xls', '')
             csvreader = csv.reader(f, delimiter=',', quotechar = '"')
             if entityName not in entityPairDict :
                 entityPairDict[entityName] = set()
                 entityPredicateDict[entityName] = set()
                 entityObjectDict[entityName] = set()
             
             for parts in csvreader :
                 if len(parts) <= 1 :
                     continue
                 
                 if parts[0] == 'ID' :
                     continue
                 
                 maxLength = 5
                 if param == 'top5' :
                     maxLength = 4

                 if len(parts) >= maxLength :
                    cross = parts[maxLength - 1].lower()
                    if cross == 'x' :
                        ## Take the subject and object
                        predicate = parts[1].rstrip('"').lstrip('"')
                        predicate = predicate.replace('http://dbpedia.org/property/', 'http://dbpedia.org/property/ontology/')
                        objs = set()
                        obj = parts[2].rstrip('"').lstrip('"').strip('*').strip(' ')            
                        if not obj.startswith('http') or obj.endswith('.jpg') or obj.endswith('.svg'):
                            objParts = obj.split(',')
                            for objPart in objParts :
                                objPart = objPart.strip(' ')
                                if len(objPart) == 0 :
                                    continue
                                    
                                if objPart[0].isupper() :
                                    objs.add('http://dbpedia.org/resource/' + objPart.replace(' ', '_'))
                                    
                        else :
                            objs.add(obj)

                        for o in objs :
                            entityPairDict[entityName].add((predicate, o))
                            entityObjectDict[entityName].add(o)
                        

                        entityPredicateDict[entityName].add(predicate)


    with open(sys.argv[1], 'r') as f :
        for line in f :
            if line.startswith('Id') :
                continue
            parts = line.rstrip('\n').split('\t')        
            entity = parts[5].rstrip(']').lstrip('[')
            
            nPreds = 0
            nPredsSolution = 0
            nObjs = 0
            nObjsSolution = 0
            nPairsSol = 0
            topSubgraphs = parts[len(parts) - 1]
            
            topSubgraphParts = topSubgraphs.split(') , ')
            nPairsInRanking = len(topSubgraphParts)
            if param == 'top5' :
                topSubgraphs = "), ".join(topSubgraphParts[0:min(5, len(topSubgraphParts))]) + ")]"
            
            solution = parts[6]
            if entity not in entityPredicateDict or entity not in entityPairDict or entity not in entityObjectDict:
                continue
                
            if len(entityPredicateDict[entity]) == 0 or len(entityPairDict[entity]) == 0 or len(entityObjectDict[entity]) == 0 :
                continue    
            
            predicatesInGoldStandard = entityPredicateDict[entity]
            
            for pred in predicatesInGoldStandard :
                if pred in topSubgraphs :
                    nPreds += 1
                if pred in solution :
                    nPredsSolution += 1
            
            predicatesInRanking = getPredicates(topSubgraphs)
            predicatesInSolution = getPredicates(solution)
            
            if len(predicatesInRanking) == 0 :
                print(conf, entity, 'has no pred ranking')
                continue            
            
            objectsInGoldStandard = entityObjectDict[entity]
            for obj in objectsInGoldStandard :
                if obj in topSubgraphs :
                    nObjs += 1
                if obj in solution :
                    nObjsSolution += 1
            
            objectsInSolution = getObjects(solution)
            objectsInRanking = getObjects(topSubgraphs)        
            
            if len(objectsInRanking) == 0 :
                print(conf, entity, 'has no obj ranking')
                continue
                            
            nPairs = 0
            pairsInGoldStandard = entityPairDict[entity]
            for pair in pairsInGoldStandard :
                atom = '<' + pair[0] + '>(?x0, <' + pair[1]
                atom1 = '<' + pair[0] + '>(?x1, <' + pair[1]
                if atom in topSubgraphs or atom1 in topSubgraphs:
                    nPairs = nPairs + 1
                if atom in solution or atom1 in solution :
                    nPairsSol = nPairsSol + 1

            pairsInSolution = solution.count('?x0')
                
            if 'PR' in parts[3] :                           
                remiOverlap[entity] = (nPreds / len(predicatesInGoldStandard), nPairs, nPairs / (5 if param == 'top5' else 10), nPairs / nPairsInRanking, nPairsSol / pairsInSolution)
                remiOverlapPrecision[entity] = nPreds / len(predicatesInRanking)
                
                if len(predicatesInSolution) > 0 :
                    remiPRSolutionPrecision[entity] = nPredsSolution / len(predicatesInSolution)
                if len(objectsInSolution) > 0 :
                    remiPRObjSolutionPrecision[entity] = nObjsSolution / len(objectsInSolution)
                if len(objectsInRanking) > 0 :
                    remiPRObjSolutionOverlap[entity] = (nObjs / len(objectsInRanking), nObjs)
                    
                remiPRObjRecall[entity] = nObjs / len(entityObjectDict[entity]) 
            else :
                remiPROverlap[entity] = (nPreds / len(predicatesInGoldStandard), nPairs, nPairs / (5 if param == 'top5' else 10), nPairs / nPairsInRanking, nPairsSol / pairsInSolution)
                remiPROverlapPrecision[entity] = nPreds / len(predicatesInRanking)         
                if len(predicatesInSolution) > 0 :
                    remiSolutionPrecision[entity] = nPredsSolution / len(predicatesInSolution) 
                if len(objectsInSolution) > 0 :            
                    remiObjSolutionPrecision[entity] = nObjsSolution / len(objectsInSolution)              
                if len(objectsInRanking) > 0:
                    remiObjSolutionOverlap[entity] = (nObjs / len(objectsInRanking), nObjs)
                
                remiObjRecall[entity] = nObjs / len(entityObjectDict)

    print('Metric, average, median, max')
    print('Frequency (recall for predicates): ', numpy.average([x[0] for x in remiOverlap.values()]), numpy.median([x[0] for x in remiOverlap.values()]), max([x[0] for x in remiOverlap.values()]))    
    print('PR (recall for predicates): ', numpy.average([x[0] for x in remiPROverlap.values()]), numpy.median([x[0] for x in remiPROverlap.values()]), max([x[0] for x in remiPROverlap.values()]))    
    print('Frequency (precision for predicates): ', numpy.average([x for x in remiOverlapPrecision.values()]), numpy.median([x for x in remiOverlapPrecision.values()]), max([x for x in remiOverlapPrecision.values()]))    
    print('PR (precision for predicates): ', numpy.average([x for x in remiPROverlapPrecision.values()]), numpy.median([x for x in remiPROverlapPrecision.values()]), max([x for x in remiPROverlapPrecision.values()]))    
    print('Frequency (precision for objects): ', numpy.average([x[0] for x in remiObjSolutionOverlap.values()]), numpy.median([x[0] for x in remiObjSolutionOverlap.values()]), max([x[0] for x in remiObjSolutionOverlap.values()]))    
    print('PR (precision for objects): ', numpy.average([x[0] for x in remiPRObjSolutionOverlap.values()]), numpy.median([x[0] for x in remiPRObjSolutionOverlap.values()]), max([x[0] for x in remiPRObjSolutionOverlap.values()]))    
    print('Frequency (precision for predicates in solution): ', numpy.average([x for x in remiSolutionPrecision.values()]), numpy.median([x for x in remiSolutionPrecision.values()]), max([x for x in remiSolutionPrecision.values()]))    
    print('PR (precision for predicates in solution): ', numpy.average([x for x in remiPRSolutionPrecision.values()]), numpy.median([x for x in remiPRSolutionPrecision.values()]), max([x for x in remiPRSolutionPrecision.values()]))    
    print('Frequency (precision for objects in solution): ', numpy.average([x for x in remiObjSolutionPrecision.values()]), numpy.median([x for x in remiObjSolutionPrecision.values()]), max([x for x in remiObjSolutionPrecision.values()]))    
    print('PR (precision for objects in solution): ', numpy.average([x for x in remiPRObjSolutionPrecision.values()]), numpy.median([x for x in remiPRObjSolutionPrecision.values()]), max([x for x in remiPRObjSolutionPrecision.values()]))    

    print('Frequency (recall for objects): ', numpy.average([x for x in remiObjRecall.values()]), numpy.median([x for x in remiObjRecall.values()]), max([x for x in remiObjRecall.values()]))    
    print('PR (recall for objects): ', numpy.average([x for x in remiPRObjRecall.values()]), numpy.median([x for x in remiPRObjRecall.values()]), max([x for x in remiPRObjRecall.values()]))    
    print('Frequence (quality for atoms): ', numpy.average([x[1] for x in remiOverlap.values()]), numpy.median([x[1] for x in remiOverlap.values()]), max([x[1] for x in remiOverlap.values()]))    
    print('PR (quality for atoms): ', numpy.average([x[1] for x in remiPROverlap.values()]), numpy.median([x[1] for x in remiPROverlap.values()]), max([x[1] for x in remiPROverlap.values()]))    

    print('Frequency (precision for atoms): ', numpy.average([x[3] for x in remiOverlap.values()]), numpy.median([x[3] for x in remiOverlap.values()]), max([x[3] for x in remiOverlap.values()]))    
    print('PR (precision for atoms): ', numpy.average([x[3] for x in remiPROverlap.values()]), numpy.median([x[3] for x in remiPROverlap.values()]), max([x[3] for x in remiPROverlap.values()]))    
    print('Frequency (recall for atoms): ', numpy.average([x[2] for x in remiOverlap.values()]), numpy.median([x[2] for x in remiOverlap.values()]), max([x[2] for x in remiOverlap.values()]))    
    print('PR (recall for atoms): ', numpy.average([x[2] for x in remiPROverlap.values()]), numpy.median([x[2] for x in remiPROverlap.values()]), max([x[2] for x in remiPROverlap.values()]))    

    print('Frequency (precision for atoms in solution): ', numpy.average([x[4] for x in remiOverlap.values()]), numpy.median([x[4] for x in remiOverlap.values()]), max([x[4] for x in remiOverlap.values()]))    
    print('PR (precision for atoms in solution): ', numpy.average([x[4] for x in remiPROverlap.values()]), numpy.median([x[4] for x in remiPROverlap.values()]), max([x[4] for x in remiPROverlap.values()]))    


    qualitiesF.append(numpy.average([x[1] for x in remiOverlap.values()]))
    qualitiesPR.append(numpy.average([x[1] for x in remiPROverlap.values()]))
    qualitiesSO.append(numpy.average([x[1] for x in remiObjSolutionOverlap.values()]))
    qualitiesPRSO.append(numpy.average([x[1] for x in remiPRObjSolutionOverlap.values()]))

print('quality scores Frequency')
print(qualitiesF)
print('Avg', numpy.average(qualitiesF[1:]), numpy.std(qualitiesF[1:]))
print('quality scores PR')
print(qualitiesPR)
print('Avg', numpy.average(qualitiesPR[1:]), numpy.std(qualitiesPR[1:]))

print('quality scores Frequency SO')
print(qualitiesSO)
print('Avg', numpy.average(qualitiesSO[1:]), numpy.std(qualitiesSO[1:]))
print('quality scores PR SO')
print(qualitiesPRSO)
print('Avg', numpy.average(qualitiesPRSO[1:]), numpy.std(qualitiesPRSO[1:]))

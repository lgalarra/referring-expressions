#!/bin/bash

COMMAND='echo'
find `pwd` -iname "*.xls" -printf "%h\n" | sort -u | while read i; do
    cd "$i" && ls -a1 *.xls | sort -u | while read j; do
		xls2csv -x "$j" > "$j.csv"
	done
done

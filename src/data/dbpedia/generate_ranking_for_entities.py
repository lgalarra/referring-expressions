#!/usr/bin/python3

import csv
import sys
import operator
import re
import random

from add_inverse_facts import parseLine
from generate_ranking_for_predicates import getRelationsAndEntitiesByDegree

if __name__ == '__main__' :
    ## Get the relevance of entities (number of facts)
    predicatesDict, predicates2EntitiesDict, entitiesDict = getRelationsAndEntitiesByDegree(sys.argv[1])      
    # Now we have to do a second pass on the data to invert the facts  
    sortedEntities = [x[0] for x in sorted(entitiesDict.items(), key=operator.itemgetter(1), reverse=True)]
                       
            
    with open('topentities-ranking', 'w') as ftop :
        for entity in sortedEntities :
            if entity is not None :
                ftop.write(entity.replace('<', '').replace('>', '') + " " + str(entitiesDict[entity]))
                ftop.write('\n')

#!/usr/bin/python3

import csv
import sys
import operator
import re
import random

def parseLine(line, lineNumber):
    parts = line.strip(' .\n').split('> <')
    if len(parts) == 3 :
        subject = parts[0] + '>'
        predicate = '<' + parts[1] + '>'
        object = '<' + parts[2]
    elif len(parts) == 2 :
        subject = parts[0] + '>'
        tail = parts[1]
        tailParts = tail.split('> "')
        if len(tailParts) == 2 :
            predicate = '<' + tailParts[0] + '>'
            object = '"' + tailParts[1]
        else :
            print('Error in line ', lineNumber, line, file=sys.stderr)
            return None, None, None
    else :      
        print('Error in line ', lineNumber, line, file=sys.stderr)
        return None, None, None 
    
    return subject, predicate, object


def getEntitiesWithImportance(fileName, classNames=None):
    ## Read the dbpedia file directly
    with open(fileName, 'r') as fdata :
        lineNumber = 0
        entitiesInClass = {}
        entitiesDict = {}
        for line in fdata :
            if line.startswith('#') :
                continue
            
            lineNumber += 1
            subject, predicate, object = parseLine(line, lineNumber)
            
            if classNames is not None :
                if predicate == '<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>' or predicate == '<http://wikidata.org/property/instance_of_P31>' :
                    if object in classNames :
                        entitiesInClass[subject] = 0
            
            if 'resource/List_of_' not in subject and re.search('__[0-9]+>$', subject) is None :
                if subject not in entitiesDict :
                    entitiesDict[subject] = 0
                
                entitiesDict[subject] += 1
    
    for entity in entitiesDict.keys() :
        if entity in entitiesInClass :
            entitiesInClass[entity] = entitiesDict[entity]
            
    return entitiesDict, entitiesInClass

if __name__ == '__main__' :
    print('First pass on the data to get the top entities', file=sys.stderr)
    ## Get the relevance of entities (number of facts)
    entitiesDict, entitiesInClass = getEntitiesWithImportance('/home/lgalarra/git/referring-expressions/src/data/wikidata/wikidata-simple-statements.compressed.clean.ids-labeled.plus-interesting-literals.nt', None)
    if len(sys.argv) > 1 and sys.argv[1] == 'only-top-entities' :       
        sortedEntities = [x[0] for x in sorted(entitiesDict.items(), key=operator.itemgetter(1), reverse=True)]
        top = int(len(sortedEntities) * 0.05) # Top 5%
        topEntities = set(sortedEntities[:top + 1])
    else :        
        # Now we have to do a second pass on the data to invert the facts  
        sortedEntities = [x[0] for x in sorted(entitiesDict.items(), key=operator.itemgetter(1), reverse=True)]
        top = int(len(sortedEntities) * 0.01) # Top 1%
        topEntities = set(sortedEntities[:top + 1])    
        
        print('Second pass on the data to get the inverted facts. The most frequent entity has', 
              entitiesDict[sortedEntities[0]], 'facts',  
              file=sys.stderr)
        ### FILL THE DESIRED DATASET HERE      
        with open('/home/lgalarra/git/referring-expressions/src/data/wikidata/wikidata-simple-statements.compressed.clean.ids-labeled.plus-interesting-literals.nt', 'r') as fdata :
            lineNumber = 0
            entitiesDict = {}
            for line in fdata :
                if line.startswith('#') :
                    continue
                 
                lineNumber += 1
                subject, predicate, object = parseLine(line, lineNumber)
                 
                ## We discard websites and wikidata entities
                if not object.startswith('<http://dbpedia.org/') and not object.startswith('<http://wikidata.org/') :
                    continue
                 
                if subject in topEntities and object.startswith('<') and predicate != '<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>' and predicate != '<http://wikidata.org/property/instance_of_P31>' :
                    print(object, predicate.replace('>', '-inv>'), subject, ' .')
                
            
    with open('topentities', 'w') as ftop :
        for topEntity in topEntities :
            ftop.write(topEntity.replace('<', '').replace('>', ''))
            ftop.write('\n') 

#!/usr/bin/python3

import re
import sys
import os


def rerank(filename, prdict):
    values = []
    idx = 0
    predicateName = ''
    with open(filename, 'r') as f:
        for line in f :
            line = line.rstrip('\n')
            if idx == 0 :
                predicateName = line
                idx += 1
                continue
                
            spaceIdx = line.rfind(' ')
            key = line[:spaceIdx]            
            if key in prdict :
                pr = prdict[key]
                values.append((key, pr))
            else :
                values.append((key, 0.0))    
    
    values = sorted(values, key=lambda x : x[1], reverse=True)
    outFileName = os.path.basename(filename)
    #print(len(values))

    with open(outFileName, 'w') as f:
        f.write(predicateName + '\n')
        for value in values :
            f.write(value[0] + ' ' + str(value[1]) + '\n')
    
                
        

prdict = {}
with open('pagerank.tsv') as f :    
    for line in f :
        parts = line.strip('\n').split('\t')
        key = parts[0].replace('>', '').replace('<', '')
        prdict[key] = float(parts[1])
        

root = "../ranking_relations/"
predicate2Coefficients = {}
    
for r,d,f in os.walk(root):
    for file in f:
        filename = os.path.join(root,file)
        #print(filename)
        if os.path.basename(filename).startswith('ranking_') :
            rerank(filename, prdict)